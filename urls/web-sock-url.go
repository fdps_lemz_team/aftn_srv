package urls

// url для web socker server
const (
	// URL для работы WS fmtp каналов
	ChannelURLPath = "/channels"
	// URL для работы с сервисом Fdps
	FdpsURLPath = "/fdps"
	// URL для работы с логгером
	LoggerURLPath = "/logger"
	// URL для web странички канала
	ChannelWebPath = "channel"
	// URL для web странички контроллера
	ChiefWebPath = "chief"	
)
