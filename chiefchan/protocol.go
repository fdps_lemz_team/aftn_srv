package chiefchan

import (
	"fdps/aftn/aftn-srv/channel/settings"
	"fdps/aftn/aftn-srv/channel/state"
)

// коды завершения приложения канала
const (
	InvalidParamCount = 1001 // приложению передано не верное кол-во аргументов
	InvalidNetPort    = 1002 // невалидное значение сетевого порта для связи с контроллером
	InvalidChannelID  = 1003 // невалидное значение идентификатора канала
	InvalidWebPort    = 1004 // невалидное значение порта web странички
)

// от контроллера (chief) могут быть получены сообщения:
//		- настройки канала
// 		- запрос остановки передачи сообщений
//		- запрос возобновления передачи сообщений
// контроллеру(chief) отправляется соообщение:
//		- запрос настроек канала
//		- сообщение для журнала
//		- сообщение о состоянии канала
//		- подтверждение остановки передачи сообщений
//		- подтверждение возобновления передачи сообщений

const (
	// RequestSettingsHdr заголовок сообщения запроса настроек канала
	RequestSettingsHdr = "RequestSettings"

	// SettingsHdr заголовок сообщения с настройками канала (либо ответ на запрос, либо модифицированные)
	SettingsHdr = "AnswerSettings"

	// ChannelHeartbeatHeader заголовок сообщения о состоянии канала
	ChannelHeartbeatHdr = "ChannelHeartbeat"

	// ChannelLogHdr заголовок сообщения для журнала
	ChannelLogHdr = "ChannelLog"

	// StopTransportRequestHdr заголовок сообщения запроса остановки передачи сообщений
	StopTransportRequestHdr = "StopTransportRequest"

	// StopTransportCompleteHdr заголовок сообщения подтверждения остановки передачи сообщений
	StopTransportCompleteHdr = "StopTransportComplete"

	// RestoreTransportRequestHdr заголовок сообщения запроса возобновления передачи сообщений
	RestoreTransportRequestHdr = "RestoreTransportRequest"

	// RestoreTransportCompleteHdr заголовок сообщения подтверждения возобновления передачи сообщений
	RestoreTransportCompleteHdr = "RestoreTransportComplete"

	// SendTestRequestHdr запрос на отправку тестового сообщения
	SendTestRequestHdr = "SendTestRequest"
)

// HeaderMsg описание заголовка сообщений, получаемых от контроллера(chief)
// канал <- контроллер (chief)
type HeaderMsg struct {
	Header string `json:"MessageType"`
}

// SettingsRequestMsg сообщение запроса настроек канала
// канал -> контроллер (chief)
type SettingsRequestMsg struct {
	HeaderMsg
	ChannelID int `json:"ChannelID"`
}

// CreateSettingsRequestMsg сформировать сообщение запроса настроек
func CreateSettingsRequestMsg(chID int) SettingsRequestMsg {
	return SettingsRequestMsg{HeaderMsg: HeaderMsg{Header: RequestSettingsHdr}, ChannelID: chID}
}

// SettingsMsg сообщение с настройками канала
// контроллер (chief) -> канал
type SettingsMsg struct {
	HeaderMsg
	chansetts.Settings
}

// CreateSettingsMsg сформировать ответ на запрос настроек
func CreateSettingsMsg(chSett chansetts.Settings) SettingsMsg {
	return SettingsMsg{HeaderMsg: HeaderMsg{Header: SettingsHdr}, Settings: chSett}
}

// ChannelHeartbeatMsg сообщение о состоянии канала
// канал -> контроллер (chief)
type ChannelHeartbeatMsg struct {
	HeaderMsg
	chanstate.State
}

// CreateChannelHeartbeatMsg сформировать сообщение о состоянии канала
func CreateChannelHeartbeatMsg(state chanstate.State) ChannelHeartbeatMsg {
	return ChannelHeartbeatMsg{HeaderMsg: HeaderMsg{Header: ChannelHeartbeatHdr}, State: state}
}

// ChannelLogMsg сообщение для журнала
// канал -> контроллер (chief)
//type ChannelLogMsg struct {
//	HeaderMsg
//	common.LogMessage // сообщение для канала
//}

// CreateChannelLogMsg сформировать сообщение для журнала
//func CreateChannelLogMsg(log common.LogMessage) ChannelLogMsg {
//	return ChannelLogMsg{HeaderMsg: HeaderMsg{Header: ChannelLogHeader}, LogMessage: log}
//}

// StopTransportRequestMsg - запрос остановки передачи сообщений (template 5).
// контроллер (chief) -> канал
type StopTransportRequestMsg struct {
	HeaderMsg
	ChannelID int `json:"ChannelID"`
}

// StopTransportCompleteMsg - ответ на запрос остановки передачи сообщений (template 6).
// канал -> контроллер (chief)
type StopTransportCompleteMsg struct {
	HeaderMsg
	ChannelID int `json:"ChannelID"`
}

// RestoreTransportRequestMsg - запрос восстановления передачи сообщений (template 7).
// контроллер (chief) -> канал
type RestoreTransportRequestMsg struct {
	HeaderMsg
	ChannelID int `json:"ChannelID"`
}

// RestoreTransportCompleteMsg - ответ на запрос восстановления передачи сообщений (template 8).
// канал -> контроллер (chief)
type RestoreTransportCompleteMsg struct {
	HeaderMsg
	ChannelID int `json:"ChannelID"`
}

// SendTestRequestMsg - апрос на отправку тестового сообщения (template 9).
//  контроллер (chief) -> канал
type SendTestRequestMsg struct {
	HeaderMsg
	ChannelID int `json:"ChannelID"`
}
