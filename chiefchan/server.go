package chiefchan

import (
	"context"
	"encoding/json"
	"fmt"
	"os/exec"
	"reflect"
	"strconv"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/network"
	"github.com/docker/docker/client"

	"fdps/aftn/aftn-srv/channel/state"
	"fdps/aftn/aftn-srv/channel/settings"
	"fdps/aftn/aftn-srv/chiefconfig"
	"fdps/aftn/aftn-srv/urls"
	"fdps/utils"
	"fdps/utils/logger"
	"fdps/utils/web_sock"
)

const (
	srvStateKey       = "AFTN. Состояние WS Server:"
	srvLastConnKey    = "AFTN. Последнее клиентское подключение:"
	srvLastDisconnKey = "AFTN. Последнее клиентское отключение:"
	srvLastErrKey     = "AFTN. Последняя ошибка:"
	srvClntListKey    = "AFTN. Список клиентов:"

	srvStateOkValue    = "Запущен."
	srvStateErrorValue = "Не запущен."

	timeFormat = "2006-01-02 15:04:05"
)

// сведения об исполняемом файле канала
type сhannelBin struct {
	filePath string        // путь к исполняемому файлу
	killChan chan struct{} // канал, исользуемый для завершения выполнения
}

// состояния каналов AFTN с отметкой времени
type сhannelStateTime struct {
	chanstate.State //состояние
	Time time.Time  // время получения сообщений о состоянии
}

// Server контроллер AFTN каналов
// Отвечает за сетевое взаимодействие с каналами,
// Запуск, остановку исполняемых файлов/docker контейнеров приложений каналов.
// Собирает состояния каналов и передает их по каналу ChannelStates
type Server struct {
	ChannelSettsChan chan chansetts.SettingsWithPort // канал для приема настроек каналов и порта для взаимодействия с каналами
	channelSetts     chansetts.SettingsWithPort      // текущие настройки каналов и орт для связи с каналами
	ChannelStates    chan []chanstate.State           // канал для передачи состояний каналов

	ChannelBinMap  map[int]сhannelBin // ключ - идентификатор канала
	killerChan     chan struct{}      // канал, по которому передается сигнал о завершение работы канала
	doneChan       chan struct{}
	wsServer       *web_sock.WebSockServer
	chStates       map[int]сhannelStateTime // ключ - ID канала
	workWithDocker bool
	stateTkr       *time.Ticker  // тикер отправки состояния каналов
	stateValidDur  time.Duration // интервал, после которого, если нет сведений о канале, сичтаем его нерабочим
}

// NewServer конструктор
func NewServer(done chan struct{}, withDocker bool) *Server {
	return &Server{
		ChannelSettsChan: make(chan chansetts.SettingsWithPort, 10),
		channelSetts: chansetts.SettingsWithPort{
			ChSettings: make([]chansetts.Settings, 0),
			ChPort:     0,
		},
		ChannelStates:  make(chan []chanstate.State, 10),
		killerChan:     make(chan struct{}),
		ChannelBinMap:  make(map[int]сhannelBin),
		doneChan:       done,
		wsServer:       web_sock.NewWebSockServer(done),
		chStates:       make(map[int]сhannelStateTime),
		workWithDocker: withDocker,
		stateTkr:       time.NewTicker(time.Second),
		stateValidDur:  time.Second * 3,
	}
}

// Work реализация работы
func (s *Server) Work() {
	go s.wsServer.Work(urls.ChannelURLPath)
	//cc.chiefChannelWS.SettChan <- chief_channel.ServerSettings{ChiefPort: cc.ChannelPort}

	logger.SetDebugParam(srvStateKey, srvStateErrorValue+"   "+time.Now().Format(timeFormat), logger.StateErrorColor)
	logger.SetDebugParam(srvLastConnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvLastDisconnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvLastErrKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvClntListKey, "-", logger.StateDefaultColor)

	for {
		select {
		// получены новые настройки каналов
		case newSetts := <-s.ChannelSettsChan:

			var needToStopIds, needToStartIds []int // идентификаторы каналов, которые необходимо остановить/запустить

			if s.channelSetts.ChPort != newSetts.ChPort {
				s.wsServer.SettingsChan <- web_sock.WebSockServerSettings{Port: newSetts.ChPort}

				// обнуляем состояния каналов
				for key := range s.chStates {
					delete(s.chStates, key)
				}

				//останавливаем и запускаем все каналы
				for _, val := range s.channelSetts.ChSettings {
					if val.IsWorking {
						needToStopIds = append(needToStopIds, val.ID)
					}
				}

				//останавливаем и запускаем все каналы
				for _, val := range newSetts.ChSettings {
					if val.IsWorking {
						needToStartIds = append(needToStartIds, val.ID)
					}
				}
			} else if !reflect.DeepEqual(s.channelSetts.ChSettings, newSetts.ChSettings) {
				// обнуляем состояния каналов
				for key := range s.chStates {
					delete(s.chStates, key)
				}

				var oldIt, newIt chansetts.Settings

				for _, newIt = range newSetts.ChSettings {
					newInOld := false
				NEWL:
					for _, oldIt = range s.channelSetts.ChSettings {
						if oldIt.ID == newIt.ID {
							newInOld = true
							break NEWL
						}
					}

					// есть в новых, есть в старых
					if newInOld {
						if oldIt != newIt {
							if oldIt.IsWorking {
								needToStopIds = append(needToStopIds, oldIt.ID)
							}
							if newIt.IsWorking {
								needToStartIds = append(needToStartIds, newIt.ID)
							}
						}
					} else { // есть в новых, нет в старых
						if newIt.IsWorking {
							needToStartIds = append(needToStartIds, newIt.ID)
						}
					}
				}

				for _, oldIt = range s.channelSetts.ChSettings {
					oldInNew := false
				OLDL:
					for _, newIt = range newSetts.ChSettings {
						if newIt.ID == oldIt.ID {
							oldInNew = true
							break OLDL
						}
					}
					// есть в старых, нет в новых
					if !oldInNew {
						if oldIt.IsWorking {
							needToStopIds = append(needToStopIds, oldIt.ID)
						}
					}
				}
			}
			// если просто cc.channelSetts = newSetts написать, то по приходу новых настроек cc.channelSetts. уже будет ссылаться на них
			s.channelSetts.ChSettings = append([]chansetts.Settings(nil), newSetts.ChSettings...)
			s.channelSetts.ChPort = newSetts.ChPort

			for _, val := range s.channelSetts.ChSettings {
				var chWork string
				if val.IsWorking {
					chWork = chanstate.StateError
				} else {
					chWork = chanstate.StateStopped
				}

				s.chStates[val.ID] = сhannelStateTime{State: chanstate.State{
					ID:          val.ID,
					InpName:     val.InpNameLat,
					OutName:     val.OutNameLat,
					CommonState: chWork,
					URL:         fmt.Sprintf("http://%s:%d/%s", val.URLAddress, val.URLPort, val.URLPath),
				},
					Time: time.Now()}
			}

			// останавливаем каналы AFTN
			if len(needToStopIds) > 0 {
				s.stopChannelsByIDs(needToStopIds)
			}
			// запускаем каналы AFTN
			if len(needToStartIds) > 0 {
				//s.startChannelsByIDs(needToStartIds)
			}

		// получен подключенный клиент от WS сервера для связи с AFTN каналами
		case curClnt := <-s.wsServer.ClntConnChan:
			logger.PrintfInfo("Подключен клиент AFTN канала с адресом: %s.", curClnt.RemoteAddr().String())
			logger.SetDebugParam(srvLastConnKey, curClnt.RemoteAddr().String()+" "+time.Now().Format(timeFormat), logger.StateDefaultColor)
			logger.SetDebugParam(srvClntListKey, fmt.Sprintf("%v", s.wsServer.ClientList()), logger.StateDefaultColor)

		// получен отключенный клиент от WS сервера для связи с AFTN каналами
		case curClnt := <-s.wsServer.ClntDisconnChan:
			logger.PrintfInfo("Отключен клиент AFTN канала с адресом: %s.", curClnt.RemoteAddr().String())
			logger.SetDebugParam(srvLastDisconnKey, curClnt.RemoteAddr().String()+" "+time.Now().Format(timeFormat), logger.StateDefaultColor)
			logger.SetDebugParam(srvClntListKey, fmt.Sprintf("%v", s.wsServer.ClientList()), logger.StateDefaultColor)

		// получен отклоненный клиент от WS сервера для связи с AFTN каналами
		case curClnt := <-s.wsServer.ClntRejectChan:
			logger.PrintfInfo("Отклонен клиент AFTN канала с адресом: %s.", curClnt.RemoteAddr().String())

		// получена ошибка от WS сервера для связи с AFTN каналами
		case wsErr := <-s.wsServer.ErrorChan:
			logger.PrintfErr("Возникла ошибка при работе WS сервера для взаимодействия с AFTN каналами. Ошибка: %s.", wsErr.Error())
			logger.SetDebugParam(srvLastErrKey, wsErr.Error(), logger.StateErrorColor)

		// получены данные от WS сервера
		case curWsPkg := <-s.wsServer.ReceiveDataChan:
			var curHdr HeaderMsg
			var unmErr error
			if unmErr = json.Unmarshal(curWsPkg.Data, &curHdr); unmErr == nil {
				switch curHdr.Header {

				case RequestSettingsHdr:
					var reqSettsMsg SettingsRequestMsg
					if err := json.Unmarshal(curWsPkg.Data, &reqSettsMsg); err == nil {
						var channelSetts chansetts.Settings
					SETTSL:
						for _, curSetts := range s.channelSetts.ChSettings {
							if curSetts.ID == reqSettsMsg.ChannelID {
								channelSetts = curSetts
								break SETTSL
							}
						}

						if settsData, err := json.Marshal(CreateSettingsMsg(channelSetts)); err == nil {
							s.wsServer.SendDataChan <- web_sock.WsPackage{Data: settsData, Sock: curWsPkg.Sock}
						}
					}

				case ChannelHeartbeatHdr:
					var curHbtMsg ChannelHeartbeatMsg
					if err := json.Unmarshal(curWsPkg.Data, &curHbtMsg); err == nil {
						s.chStates[curHbtMsg.ID] = сhannelStateTime{State: curHbtMsg.State, Time: time.Now()}
					}
				}

			} else {
				logger.PrintfErr("Ошибка разбора сообщения от приложения AFTN канала. Ошибка: %s.", unmErr)
			}

		// получено состояние работоспособности WS сервера для связи с AFTN каналами
		case connState := <-s.wsServer.StateChan:
			switch connState {
			case web_sock.ServerTryToStart:
				logger.PrintfInfo("Запускаем WS сервер для взаимодействия с AFTN каналами. Порт: %d.", s.channelSetts.ChPort)
				logger.SetDebugParam(srvStateKey, srvStateOkValue+" Порт: "+strconv.Itoa(s.channelSetts.ChPort), logger.StateOkColor)
			case web_sock.ServerError:
				logger.SetDebugParam(srvStateKey, srvStateErrorValue, logger.StateErrorColor)
				//case web_sock.ServerReadySend:
				//case web_sock.ServerNotReadySend:
			}

		// таймер отправки состояния каналов
		case <-s.stateTkr.C:
			var chStateSlice []chanstate.State
			nowTime := time.Now()
			for key, val := range s.chStates {
				if val.Time.Add(s.stateValidDur).Before(nowTime) {
					delete(s.chStates, key)
				} else {
					chStateSlice = append(chStateSlice, s.chStates[key].State)
				}
			}

			s.ChannelStates <- chStateSlice
		}
	}
}

// останавливаем каналы с указанным ID
func (s *Server) stopChannelsByIDs(idsToStop []int) {
STOPL:
	for _, stopID := range idsToStop {
		if channelBin, ok := s.ChannelBinMap[stopID]; ok {
			channelBin.killChan <- struct{}{}
			<-s.killerChan

			if !s.workWithDocker {
				if err := utils.RemoveBinary(channelBin.filePath); err != nil {
					logger.PrintfErr(err.Error())
					continue STOPL
				}
			}
			close(channelBin.killChan)
			delete(s.ChannelBinMap, stopID)
		}
	}
}

// запускаем каналы с указанным ID
func (s *Server) startChannelsByIDs(idsToStart []int) {
STARTL:
	for _, startID := range idsToStart {
	STARTL2:
		for _, newIt := range s.channelSetts.ChSettings {
			if startID == newIt.ID {
				if s.workWithDocker {
					s.ChannelBinMap[startID] = сhannelBin{killChan: make(chan struct{})}
					go s.startChannelContainer(newIt, s.ChannelBinMap[startID].killChan)

				} else {
					channelFilePath, err := utils.CopyBinary(newIt.Version, newIt.ID, newIt.InpNameLat, newIt.OutNameLat)
					if err != nil {
						logger.PrintfErr(err.Error())
						continue STARTL
					}
					s.ChannelBinMap[startID] = сhannelBin{filePath: channelFilePath, killChan: make(chan struct{})}
					go s.startChannelProcess(channelFilePath, newIt, s.ChannelBinMap[startID])
				}
				break STARTL2
			}
		}
	}
}

// запуск исполняемого файла AFTN канала
func (s *Server) startChannelProcess(channelFilePath string, chSett chansetts.Settings, binInfo сhannelBin) {
	cmd := exec.Command(channelFilePath, strconv.Itoa(s.channelSetts.ChPort), strconv.Itoa(chSett.ID),
		chSett.URLPath, strconv.Itoa(chSett.URLPort))

	if err := cmd.Start(); err != nil {
		logger.PrintfErr("Ошибка запуска приложения AFTN канала. Исполняемый файл: %s. Иденификатор канала: %d. Ошибка: %s.",
			channelFilePath, chSett.ID, err.Error())
		return
	}
	logger.PrintfInfo("Запущено приложения AFTN канала. Исполняемый файл: %s. Иденификатор канала: %d.",
		channelFilePath, chSett.ID)

	// канал, в который ошибка завершения отправится
	done := make(chan error, 1)
	go func() {
		done <- cmd.Wait()
	}()

	select {
	case err := <-done:
		if err != nil {
			logger.PrintfErr("Нештатное завершение приложения AFTN канала. Исполняемый файл: %s. Идентификатор канала: %d. Ошибка: %s.",
				channelFilePath, chSett.ID, err.Error())

			if _, ok := s.chStates[chSett.ID]; ok {
				curState := s.chStates[chSett.ID]
				curState.State.CommonState = chanstate.StateError
				s.chStates[chSett.ID] = curState
			}
		}
		return

	case <-binInfo.killChan:
		if err := cmd.Process.Kill(); err != nil {
			logger.PrintfErr("Ошибка завершения выполнения приложения AFTN канала. Ошибка: %s.", err.Error())
		} else {
			logger.PrintfInfo("Штатное завершение приложения AFTN канала. Исполняемый файл: %s. Идентификатор канала: %d.",
				channelFilePath, chSett.ID)
		}
		time.Sleep(1 * time.Second)
		s.killerChan <- struct{}{}
		return
	}
}

// запуск docker контейнера AFTN канала
func (s *Server) startChannelContainer(chSett chansetts.Settings, killChan chan struct{}) {
	ctx := context.Background()

	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	defer cli.Close()
	if err != nil {
		logger.PrintfErr("Ошибка создания клиента сервиса docker. Ошибка: %s", err.Error())
	}
	cli.NegotiateAPIVersion(ctx)

	//portBinding := make(nat.PortMap)
	//portSet := make(nat.PortSet)

	curContainerName := "aftn_channel_" + chSett.InpNameLat + "_" + chSett.OutNameLat + "_" + strconv.Itoa(chSett.ID)

	resp, crErr := cli.ContainerCreate(ctx,
		&container.Config{
			//ExposedPorts: portSet,
			Image: chiefconfig.ChiefCfg.DockerRegistry + "/" + chiefconfig.ChannelImageName + ":" + chSett.Version,
			Cmd: []string{strconv.Itoa(s.channelSetts.ChPort), strconv.Itoa(chSett.ID),
				chSett.URLPath, strconv.Itoa(chSett.URLPort)},
		},
		&container.HostConfig{
			Resources: container.Resources{
				CPUCount:   1,
				CPUPercent: 10,
				Memory:     100 * 1 << 20, // 100 MB
			},
			NetworkMode: "host",
			//PortBindings: portBinding,
			RestartPolicy: container.RestartPolicy{Name: "always"},
		},
		&network.NetworkingConfig{},
		curContainerName)

	if crErr != nil {
		logger.PrintfErr("Ошибка создания docker контейнера %s. Ошибка: %s.", curContainerName, crErr.Error())
	} else {
		logger.PrintfInfo("Создан docker контейнер %s.", curContainerName)
	}

	if err := cli.ContainerStart(ctx, resp.ID, types.ContainerStartOptions{}); err != nil {
		logger.PrintfErr("Ошибка запуска docker контейнера %s. Ошибка: %s.", curContainerName, err.Error())
	} else {
		logger.PrintfInfo("Запущен docker контейнер %s.", curContainerName)
	}

	statusCh, errCh := cli.ContainerWait(ctx, resp.ID, container.WaitConditionNotRunning)

	for {
		select {
		case cntErr := <-errCh:
			if cntErr != nil {
				logger.PrintfInfo("Ошибка в работе docker контейнера %s. Ошибка: %s.", curContainerName, cntErr.Error())
			}
		case <-statusCh:

		case <-killChan:
			var stopDur time.Duration = 100 * time.Millisecond

			if stopErr := cli.ContainerStop(ctx, resp.ID, &stopDur); stopErr == nil {
				logger.PrintfInfo("Остановлен docker контейнер %s.", curContainerName)
				if rmErr := cli.ContainerRemove(ctx, resp.ID, types.ContainerRemoveOptions{}); rmErr == nil {
					logger.PrintfInfo("Удален docker контейнер %s.", curContainerName)
				} else {
					logger.PrintfErr("Ошибка удаления docker контейнера %s.Ошибка: %s. ", curContainerName, rmErr.Error())
				}
			} else {
				logger.PrintfErr("Ошибка остановки docker контейнера %s. Ошибка %s.", curContainerName, stopErr.Error())
			}
			s.killerChan <- struct{}{}
			return
		}
	}
}
