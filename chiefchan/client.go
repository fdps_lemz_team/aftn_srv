package chiefchan

import (
	"encoding/json"
	"errors"

	"fdps/aftn/aftn-srv/urls"
	//"fdps/utils/logger/log_db"
	"fdps/utils/logger"
	"fdps/utils/web_sock"
)

// ClientSettings настройки клиента контроллера каналов
type ClientSettings struct {
	ChiefAddress string // адрес контроллера каналов
	ChiefPort    int    // порт приема данных контроллера каналов
	ChannelID    int    // идентификатор канала
}

// Client клиент взаимодействия контроллера и канала
type Client struct {
	doneChan    chan struct{} // канал для прекращения работы
	ReceiveChan chan []byte   // канал для приема данных от контроллера каналов
	SendChan    chan []byte   // канал для отправки данных контроллеру каналов
	SettChan    chan ClientSettings
	setts       ClientSettings // текущие настройки канала
	ws          *web_sock.WebSockClient
	lastConnErr error // последняя ошибка при подключении к WS серверу
}

// NewClient конструктор
func NewClient(done chan struct{}) *Client {
	return &Client{
		doneChan:    done,
		ReceiveChan: make(chan []byte, 10),
		SendChan:    make(chan []byte, 10),
		SettChan:    make(chan ClientSettings),
		ws:          web_sock.NewWebSockClient(done),
		lastConnErr: errors.New(""),
	}
}

// Work реализация работы клиента
func (c *Client) Work() {
	for {
		select {
		// новые настройки
		case newSetts := <-c.SettChan:
			if c.setts != newSetts {
				c.setts = newSetts
				go c.ws.Work(web_sock.WebSockClientSettings{
					ServerAddress:     newSetts.ChiefAddress,
					ServerPort:        newSetts.ChiefPort,
					UrlPath:           urls.ChannelURLPath,
					ReconnectInterval: 3,
				})
			}

		// данные для отправки
		case toChiefData := <-c.SendChan:
			c.ws.SendDataChan <- toChiefData

		// полученные данные от chief
		case fromChiefData := <-c.ws.ReceiveDataChan:
			c.ReceiveChan <- fromChiefData

		// ошибка WS
		case wsErr := <-c.ws.ErrorChan:
			if c.lastConnErr.Error() != wsErr.Error() {
				c.lastConnErr = wsErr
				logger.PrintfErr("Ошибка сетевого взаимодействия AFTN канала и контроллера. Ошибка: %s", wsErr.Error())
			}

		// полученные данные от chief
		case chiefClntState := <-c.ws.StateChan:
			if chiefClntState == web_sock.ClientConnected {
				logger.PrintfInfo("Установлено сетевое соединение с контроллером AFTN каналов. Адрес: %s", c.setts.ChiefAddress)

				if dataToSend, err := json.Marshal(CreateSettingsRequestMsg(c.setts.ChannelID)); err == nil {
					logger.PrintfInfo("Запрос настроек от AFTN канала id = %d.", c.setts.ChannelID)
					c.SendChan <- dataToSend
				}
			} else { //} if chiefClntState == web_sock.ClientDisconnected {
				logger.PrintfWarn("Потеряно сетевое соединение с контроллером AFTN каналов. Адрес: %s", c.setts.ChiefAddress)

			}
		}
	}
}
