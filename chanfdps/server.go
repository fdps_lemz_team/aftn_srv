package chanfdps

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strconv"
	"time"

	"fdps/aftn/aftn-srv/urls"
	"fdps/utils/logger"
	"fdps/utils/web_sock"
)

const (
	srvStateKey       = "FDPS. Состояние WS Server:"
	srvLastConnKey    = "FDPS. Последнее клиентское подключение:"
	srvLastDisconnKey = "FDPS. Последнее клиентское отключение:"
	srvLastErrKey     = "FDPS. Последняя ошибка:"
	srvClntListKey    = "FDPS. Список клиентов:"

	srvStateOkValue    = "Запущен."
	srvStateErrorValue = "Не запущен."

	timeFormat = "2006-01-02 15:04:05"
)

// ServerSetts настройки клиента
type ServerSetts struct {
	ServerPort      int
	PermitClientIps []string // список разрешенных клиентских адресов (если пустой, то всем можно подключаться)
}

// Server сервер для взаимодействия канала и fdps сервиса
type Server struct {
	ReceiveTlgChan chan Tlg
	ReceiveLamChan chan Lam
	SendTlgChan    chan Tlg
	SendLamChan    chan Lam

	ReadyChan chan bool // готов передавать FDPS сервису данные
	SettChan  chan ServerSetts

	doneChan    chan struct{} //
	setts       ServerSetts   // текущие настройки
	wsServer    *web_sock.WebSockServer
	isListening bool
}

// NewServer конструктор
func NewServer(done chan struct{}) *Server {
	return &Server{
		ReceiveTlgChan: make(chan Tlg, 10),
		ReceiveLamChan: make(chan Lam, 10),
		SendTlgChan:    make(chan Tlg, 10),
		SendLamChan:    make(chan Lam, 10),

		ReadyChan: make(chan bool, 10),
		SettChan:  make(chan ServerSetts),
		doneChan:  done,
		wsServer:  web_sock.NewWebSockServer(done),
	}
}

// Work реализация работы
func (s *Server) Work() {
	go s.wsServer.Work(urls.FdpsURLPath)

	logger.SetDebugParam(srvStateKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvLastConnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvLastDisconnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvLastErrKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(srvClntListKey, "-", logger.StateDefaultColor)

	for {
		select {
		// получены новые настройки
		case newSetts := <-s.SettChan:
			if !reflect.DeepEqual(s.setts, newSetts) {
				s.wsServer.SettingsChan <- web_sock.WebSockServerSettings{
					Port:            newSetts.ServerPort,
					PermitClientIps: newSetts.PermitClientIps,
				}
				s.setts = newSetts
			}

		// получены данные от FDPS сервиса
		case curWsPkg := <-s.wsServer.ReceiveDataChan:
			var curHdr AnyMsg
			var unmErr error
			if unmErr = json.Unmarshal(curWsPkg.Data, &curHdr); unmErr == nil {
				switch curHdr.Header.Header {
				case TlgHdr:
					var curTlg Tlg
					if errTlg := json.Unmarshal(curWsPkg.Data, &curTlg); errTlg == nil {
						s.ReceiveTlgChan <- curTlg
						fmt.Println("s.ReceiveTlgChan", len(s.ReceiveTlgChan), cap(s.ReceiveTlgChan))

						lamBt, _ := json.Marshal(&Lam{
							Header: Header{LamHdr},
							Id:     curTlg.Id,
							Type:   TlgForSend,
						})
						s.wsServer.SendDataChan <- web_sock.WsPackage{
							Data: lamBt,
							Sock: curWsPkg.Sock,
						}
					} else {
						logger.PrintfErr("Ошибка Unmarshall телеграммы: %v", errTlg)
					}

				case LamHdr:
					var curLam Lam
					if errLam := json.Unmarshal(curWsPkg.Data, &curLam); errLam == nil {
						s.ReceiveLamChan <- curLam
					} else {
						logger.PrintfErr("Ошибка Unmarshall телеграммы: %v", errLam)
					}
				}

			} else {
				logger.PrintfErr("Ошибка разбора сообщения от FDPS сервиса. Ошибка: %s.", unmErr)
			}

		// получена ТЛГ от worker
		case curTlg := <-s.SendTlgChan:
			tlgBt, _ := json.Marshal(&curTlg)
			s.wsServer.SendDataChan <- web_sock.WsPackage{
				Data: tlgBt,
				//Sock: curWsPkg.Sock,
			}

		// получен LAM от worker
		case curLam := <-s.SendLamChan:
			lamBt, _ := json.Marshal(&curLam)
			s.wsServer.SendDataChan <- web_sock.WsPackage{
				Data: lamBt,
				//Sock: curWsPkg.Sock,
			}

		// получено состояние работоспособности WS сервера для связи с FDPS
		case connState := <-s.wsServer.StateChan:
			switch connState {
			case web_sock.ServerTryToStart:
				s.isListening = true
				logger.PrintfInfo("Запускаем WS сервер для взаимодействия с FDPS. Порт: %d.", s.setts.ServerPort)
				logger.SetDebugParam(srvStateKey, srvStateOkValue+" Порт: "+strconv.Itoa(s.setts.ServerPort), logger.StateOkColor)
			case web_sock.ServerError:
				s.isListening = false
				logger.SetDebugParam(srvStateKey, srvStateErrorValue, logger.StateErrorColor)
			case web_sock.ServerReadySend:
				s.ReadyChan <- true
			case web_sock.ServerNotReadySend:
				s.ReadyChan <- false
			}

		// получена ошибка от WS сервера для связи с FDPS
		case wsErr := <-s.wsServer.ErrorChan:
			logger.PrintfErr("Возникла ошибка при работе WS сервера взаимодействия с FDPS. Ошибка: %s.", wsErr.Error())
			logger.SetDebugParam(srvLastErrKey, time.Now().Format(timeFormat)+""+wsErr.Error(), logger.StateErrorColor)

		// получен подключенный клиент от WS сервера для связи с FDPS
		case curClnt := <-s.wsServer.ClntConnChan:
			logger.PrintfInfo("Подключен клиент FDPS с адресом: %s.", curClnt.RemoteAddr().String())
			logger.SetDebugParam(srvLastConnKey, curClnt.RemoteAddr().String()+" "+time.Now().Format(timeFormat), logger.StateDefaultColor)
			logger.SetDebugParam(srvClntListKey, fmt.Sprintf("%v", s.wsServer.ClientList()), logger.StateDefaultColor)

		// получен отклоненный клиент от WS сервера для связи с FDPS
		case curClnt := <-s.wsServer.ClntRejectChan:
			logger.PrintfInfo("Отклонен клиент FDPS с адресом: %s.", curClnt.RemoteAddr().String())

		// получен отключенный клиент от WS сервера для связи с FDPS
		case curClnt := <-s.wsServer.ClntDisconnChan:
			logger.PrintfInfo("Отключен клиент FDPS с адресом: %s.", curClnt.RemoteAddr().String())
			logger.SetDebugParam(srvLastDisconnKey, curClnt.RemoteAddr().String()+" "+time.Now().Format(timeFormat), logger.StateDefaultColor)
			logger.SetDebugParam(srvClntListKey, fmt.Sprintf("%v", s.wsServer.ClientList()), logger.StateDefaultColor)
		}
	}
}

// IsListening работоспособность сервера
func (s *Server) IsListening() bool {
	return s.isListening
}

// ConnectedClients список подключенных клиентов
func (s *Server) ConnectedClients() string {
	var retValue string
	for _, val := range s.wsServer.ClientList() {
		retValue += val + ", "
	}
	if len(retValue) > 2 {
		retValue = retValue[:len(retValue)-2]
	}
	return retValue
}
