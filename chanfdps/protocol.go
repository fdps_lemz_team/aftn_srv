package chanfdps

// AFTN канал -> FDPS сервис:
//		- ...
// FDPS сервис -> AFTN канал:
//		- ...

const (
	TlgHdr = "Tlg"
	LamHdr = "Lam"

	TlgForSend  = "ForSend"
	TlgReceived = "Received"
)

// Header описание заголовка сообщений
type Header struct {
	Header string `json:"MessageType"`
}

type AnyMsg struct {
	Header Header
}

// ReceiveTlg
// канал -> контроллер (chief)
type Tlg struct {
	Header Header
	Id     string `json:"ID"`
	Text   string `json:"Text"`
	Type   string `json"Type"`
}

// ReceiveLam
// канал -> контроллер (chief)
type Lam struct {
	Header Header
	Id     string `json:"Id"`
	Type   string `json"Type"`
}
