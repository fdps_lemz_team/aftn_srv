package chanfdps

import (
	"encoding/json"
	"errors"
	"fdps/utils/logger"
	"fdps/utils/web_sock"
	"time"
)

const (
	clntStateKey       = "AFTN. Состояние WS Client:"
	clntLastConnKey    = "AFTN. Последнее подключение:"
	clntLastDisconnKey = "AFTN. Последнее отключение:"
	clntLastErrKey     = "AFTN. Последняя ошибка:"

	clntStateOkValue    = "Подключен."
	clntStateErrorValue = "Не подключен."
)

// ClientSettings настройки клиента контроллера каналов
type ClientSettings struct {
	ChannelIp   string
	ChannelPort int
	ChannelPath string
}

// Client клиент взаимодействия контроллера и канала
type Client struct {
	ReceiveTlgChan chan Tlg
	ReceiveLamChan chan Lam
	SendTlgChan    chan Tlg
	SendLamChan    chan Lam

	doneChan chan struct{} // канал для прекращения работы
	//ReceiveChan chan []byte   // канал для приема данных от контроллера каналов
	//SendChan    chan []byte   // канал для отправки данных контроллеру каналов
	SettChan chan ClientSettings
	setts    ClientSettings // текущие настройки канала
	ws       *web_sock.WebSockClient
	lastWsErr error
}

// NewClient конструктор
func NewClient(done chan struct{}) *Client {
	return &Client{
		ReceiveTlgChan: make(chan Tlg, 10),
		ReceiveLamChan: make(chan Lam, 10),
		SendTlgChan:    make(chan Tlg, 10),
		SendLamChan:    make(chan Lam, 10),
		doneChan:       done,
		SettChan: make(chan ClientSettings),
		ws:       web_sock.NewWebSockClient(done),
		lastWsErr: errors.New(""),
}

}

// Work реализация работы клиента
func (c *Client) Work() {
	logger.SetDebugParam(clntStateKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(clntLastConnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(clntLastDisconnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(clntLastErrKey, "-", logger.StateDefaultColor)

	for {
		select {
		// новые настройки
		case newSetts := <-c.SettChan:
			if c.setts != newSetts {
				c.setts = newSetts
				go c.ws.Work(web_sock.WebSockClientSettings{
					ServerAddress:     newSetts.ChannelIp,
					ServerPort:        newSetts.ChannelPort,
					UrlPath:           newSetts.ChannelPath,
					ReconnectInterval: 3,
				})
			}

		// TLG для отправки
		case curTlg := <-c.SendTlgChan:
			if c.ws.IsConnected() {
				tlgBt, _ := json.Marshal(&curTlg)
				logger.PrintfInfo("Отправка TLG в AFTN канал. %s", string(tlgBt))
				c.ws.SendDataChan <- tlgBt
			}

		// LAM для отправки
		case curLam := <-c.SendLamChan:
			if c.ws.IsConnected() {
				lamBt, _ := json.Marshal(&curLam)
				logger.PrintfInfo("Отправка LAM в AFTN канал. %s", string(lamBt))
				c.ws.SendDataChan <- lamBt
			}

		// полученные данные от chief
		case curData := <-c.ws.ReceiveDataChan:
			var curHdr AnyMsg
			var unmErr error
			if unmErr = json.Unmarshal(curData, &curHdr); unmErr == nil {
				switch curHdr.Header.Header {
				case TlgHdr:
					var curTlg Tlg
					json.Unmarshal(curData, &curTlg)
					c.ReceiveTlgChan <- curTlg

				case LamHdr:
					var curLam Lam
					json.Unmarshal(curData, &curLam)
					c.ReceiveLamChan <- curLam

				default:
					logger.PrintfErr("Получен неизвестный тип данных от AFTN: %s", string(curData))
				}

			} else {
				logger.PrintfErr("Ошибка разбора сообщения от FDPS сервиса. Ошибка: %s.", unmErr)
			}

		// ошибка WS
		case wsErr := <-c.ws.ErrorChan:
			if wsErr == nil {
				logger.PrintfInfo("Запущен WS клиент FDPS-AFTN_SERVICE.")

				//if dataToSend, err := json.Marshal(CreateSettingsRequestMsg(c.setts.ChannelID)); err == nil {
				//	logger.PrintfInfo("Запрос настроек от AFTN канала id = %d.", c.setts.ChannelID)
				//	c.SendChan <- dataToSend
				//}
			} else {
				if c.lastWsErr.Error() != wsErr.Error() {
					c.lastWsErr = wsErr
					logger.PrintfErr("Ошибка запуска WS клиента FDPS-AFTN_SERVICE. Ошибка: %s.", wsErr.Error())
				}
				logger.SetDebugParam(clntLastErrKey, time.Now().Format(timeFormat)+""+wsErr.Error(), logger.StateErrorColor)
			}

		case connState := <-c.ws.StateChan:
			switch connState {
			case web_sock.ClientConnected:
				logger.SetDebugParam(clntStateKey, srvStateOkValue, logger.StateOkColor)
				logger.SetDebugParam(clntLastConnKey, time.Now().Format(timeFormat), logger.StateDefaultColor)
			case web_sock.ClientDisconnected:
				logger.SetDebugParam(clntStateKey, srvStateErrorValue, logger.StateErrorColor)
				logger.SetDebugParam(clntLastDisconnKey, time.Now().Format(timeFormat), logger.StateDefaultColor)
			}
		}
	}
}
