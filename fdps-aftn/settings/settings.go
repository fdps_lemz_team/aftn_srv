package fdpssetts

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"fdps/utils"
)

// FdpsServiceSettings настройки сервиса
type FdpsServiceSettings struct {
	ChannelIpAddr      string   `json:"ChannelIP"`
	ChannelPort        int      `json:"ChannelPort"`
	TlgTextSlice       []string `json:"TlgTextSlice"`
	TlgSendIntervalSec int      `json:"TlgSendIntervalSec"`
}

var chiefSettingsFile = utils.AppPath() + "/config/fdps-aftn-settings.json"

// ReadFromFile чтение ранее сохраненных настроек из файла
func (cs *FdpsServiceSettings) ReadFromFile() error {
	if data, errRead := ioutil.ReadFile(chiefSettingsFile); errRead != nil {
		cs.SetDefault()
		cs.SaveToFile()
		return errRead
	} else {
		if errJson := json.Unmarshal(data, &cs); errJson != nil {
			cs.SetDefault()
			cs.SaveToFile()
			return errJson
		}
	}
	return nil
}

// SaveToFile сохранение настроек в файл
func (cs *FdpsServiceSettings) SaveToFile() error {
	confData, err := json.Marshal(cs)
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile(chiefSettingsFile, utils.JsonPrettyPrint(confData), os.ModePerm); err != nil {
		return err
	}
	return nil
}

// SetDefault настройки по умолчанию
func (cs *FdpsServiceSettings) SetDefault() {
	cs.ChannelIpAddr = "127.0.0.1"
	cs.ChannelPort = 9555
	cs.TlgTextSlice = []string{`FF UUUULEMZ
		311548 ALBERTRP
		CHIP AND
		DALY DOLB`,
		`FF UUUULEMZ
311548 ALBERTRP
CHIP AND
DALY DOLB FROM CHIP TO DALE 1`,
		`FF UUUULEMZ
311548 ALBERTRP
CHIP AND
DALY DOLB FROM DALE TO CHIP 2`,
		`FF UUUULEMZ
311548 ALBERTRP
CHIP AND
DALY DOLB FROM HAECHKA TO ROKKY 3`,
		`FF UUUULEMZ
311548 ALBERTRP
CHIP AND
DALY DOLB FROM ROKKY TO HAECHKA 4`,
	}
	cs.TlgSendIntervalSec = 3
}
