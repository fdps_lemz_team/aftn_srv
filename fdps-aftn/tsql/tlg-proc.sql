use AftnBase
go

alter table aftnserver add sha varchar(256)
go
alter table aftnserver add errors varchar(4096)
go
alter table aftnserver add createdate datetime default getutcdate()
go

update AftnServer set createdate=getutcdate() where createdate is null
go


drop procedure tlgForSend
go

create procedure    tlgForSend
                     @LastId    bigint                
                    ,@ChName    varchar(3) = null    
as
begin 

    if @chname is null or len(rtrim(ltrim(@chname)))=0
    begin
        select  id      as id
        ,       text    as tlgtext
        from    AftnServer
        where   id          > @lastId
        and     createdate  > getutcdate()-25.0/24-1000
        and     tlg_type    = '3'
        and     text        is not null
        order by id asc
    end
    else
    begin
        select  id      as id
        ,       text    as tlgtext
        from    AftnServer
        where   id          > @lastId
        and     createdate  > getutcdate()-25.0/24-1000
        and     tlg_type    = '3'
        and     text        is not null
        and     ch_name     = @ChName
        order by id asc
    end

end
go

grant  execute  on [dbo].[tlgForSend]  to [AftnMaster]
go


drop procedure tlgUpdateAfterSend
go

create procedure    tlgUpdateAfterSend
                     @id        bigint                
                    ,@text      varchar(max)          
                    ,@chnum     varchar(3)    
                    ,@errors    varchar(2048)=null      
as
begin 

    if @errors is null or len(rtrim(ltrim(@errors)))=0
    begin
        update AftnServer set 
            text        = @text
        ,   ch_num      = @chnum
        ,   tlg_type    = '4'       -- ������� ����, ��� ��� ����������   
        ,   ar_time     = replace(replace(replace(convert(varchar(128),getutcdate(),120),'-',''),' ',''),':','')
        ,   errors      = null
        ,   createdate  = getutcdate()
        where id = @id
    end
    else
    begin
        update AftnServer set 
            text        = @text
        ,   ch_num      = @chnum
        ,   tlg_type    = 'S'       -- ������� ����, ��� ��� �� ���������� � ������� ������
        ,   ar_time     = replace(replace(replace(convert(varchar(128),getutcdate(),120),'-',''),' ',''),':','')
        ,   errors      = @errors
        ,   createdate  = getutcdate()
        where id = @id
    end
    
end
go

grant  execute  on [dbo].[tlgUpdateAfterSend]  to [AftnMaster]
go

drop procedure tlgAppendRecv
go

create procedure    tlgAppendRecv
                     @text      varchar(max)          
                    ,@ChName    varchar(3)                
                    ,@ChNum     varchar(3)      = null    
                    ,@RdNum     varchar(3)      = null    
                    ,@AdrFrom   varchar(3)      = null    
                    ,@errors    varchar(2048)   = null      
                    ,@artime    datetime        = null
as
begin 

    insert into AftnServer 
    (text
    ,ch_name
    ,ch_num
    ,tlg_type
    ,ar_time
    ,RdNum
    ,AdrFrom
    ,errors
    ) 
    values 
    (@text
    ,@ChName
    ,@ChNum
    ,'1'
    ,replace(replace(replace(convert(varchar(128),isnull(@artime,getutcdate()),120),'-',''),' ',''),':','')
    ,@RdNum
    ,@AdrFrom
    ,@errors)
       
end
go

grant  execute  on [dbo].[tlgAppendRecv]  to [AftnMaster]
go

drop procedure tlgClear
go
    
create procedure    tlgClear    
as
begin 

    delete from AftnServer where createdate < getutcdate() - 31
    
end
go

grant  execute  on [dbo].[tlgClear]  to [AftnMaster]
go
