package main

import (
	"fdps/aftn/aftn-srv/fdps-aftn/worker"

	"fdps/utils"

	"fdps/utils/logger"
	"fdps/utils/logger/log_ljack"
	"fdps/utils/logger/log_std"
	"fdps/utils/logger/log_web"
)

const appName = "fdps-aftn-service"

func initLoggers() {
	logger.AppendLogger(log_std.LogStd)

	// логгер с web страничкой
	log_web.Initialize(log_web.LogWebSettings{
		StartHttp:  true,
		NetPort:    utils.AftnFdpsWebDefaultPort,
		LogURLPath: utils.AftnFdpsPath,
		Title:      "FDPS AFTN CHANNEL",
	})
	logger.AppendLogger(log_web.WebLogger)
	utils.AppendHandler(log_web.WebLogger)

	// логгер с сохранением в файлы
	var ljackSetts log_ljack.LjackSettings
	ljackSetts.GenDefault()
	ljackSetts.FilesName = appName + ".log"

	if err := log_ljack.Initialize(ljackSetts); err != nil {
		logger.PrintfErr("Ошибка инициализации логгера lumberjack. Ошибка: %s", err.Error())
	}
	logger.AppendLogger(log_ljack.LogLjack)
}

func main() {
	initLoggers()
	logger.PrintfInfo("FDPS-AFTN сервис запущен.")

	done := make(chan struct{})
	//web.Start(web.WebServerConf{Port: uint16(worker.WebPort)}, done)

	worker.NewWorker(done).Start()

	//return web.WaitShutdown() // завершение работы сервиса
}
