package worker

import (
	"strconv"
	"time"

	"fdps/aftn/aftn-srv/chanfdps"
	"fdps/aftn/aftn-srv/fdps-aftn/settings"
	"fdps/utils"
	"fdps/utils/logger"
)

type Worker struct {
	setts fdpssetts.FdpsServiceSettings
	Done  chan struct{}
}

func NewWorker(done chan struct{}) *Worker {
	return &Worker{
		Done: done,
	}
}

func (w *Worker) Start() {
	w.setts.ReadFromFile()

	channelClient := chanfdps.NewClient(w.Done)
	go channelClient.Work()
	channelClient.SettChan <- chanfdps.ClientSettings{ChannelIp: w.setts.ChannelIpAddr, ChannelPort: w.setts.ChannelPort, ChannelPath: utils.AftnFdpsChannelPath}

	tkr := time.NewTicker(time.Duration(w.setts.TlgSendIntervalSec) * time.Second)
	curTlgNum := 0
	curTlgIdx := 0

	for {
		select {
		// получена тлг от AFTN канала
		case curTlg := <-channelClient.ReceiveTlgChan:
			logger.PrintfInfo("Получена TLG: %v", curTlg)

			channelClient.SendLamChan <- chanfdps.Lam{
				Header: chanfdps.Header{chanfdps.LamHdr},
				Id:     curTlg.Id,
				Type:   curTlg.Type,
			}

		// получен LAM от AFTN канала
		case curLam := <-channelClient.ReceiveLamChan:
			logger.PrintfInfo("Получен LAM: %v", curLam)

		// получен LAM от AFTN канала
		case curLam := <-channelClient.ReceiveLamChan:
			logger.PrintfInfo("Получен LAM: %v", curLam)

		case <-tkr.C:
			var cutTlgText string
			if len(w.setts.TlgTextSlice) > 0 {
				cutTlgText = w.setts.TlgTextSlice[curTlgIdx]
				curTlgIdx++
				if curTlgIdx > len(w.setts.TlgTextSlice)-1 {
					curTlgIdx = 0
				}
			} else {
				cutTlgText = "NO TLG TEST SET IN CONFIG"
			}

			curTlgNum++

			curTlg := chanfdps.Tlg{
				Header: chanfdps.Header{Header: chanfdps.TlgHdr},
				Id:     strconv.Itoa(curTlgNum),
				Text:   cutTlgText,
				Type:   chanfdps.TlgForSend,
			}

			channelClient.SendTlgChan <- curTlg

		}
	}
}
