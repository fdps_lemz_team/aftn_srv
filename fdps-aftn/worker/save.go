package worker

import "fdps/aftn/aftn-srv/fdps-aftn/model"

func (w *Worker) save() {
	lid, t, ok := model.Telegrams.LocalDbGet()
	for ok {
		if t.Id > 0 {
			model.Telegrams.Update(t)
		} else {
			model.Telegrams.Append(t)
		}
		model.Telegrams.LocalDbDel(lid)
		lid, t, ok = model.Telegrams.LocalDbGet()
	}
	model.Telegrams.LocalDbClear()
}
