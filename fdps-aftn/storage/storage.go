package storage

import (
	"database/sql"
	"errors"
	"fdps/utils/logger"
	"sync"
)

var db = &sqlServer{IsOpen: false}
var mu sync.Mutex

func Close() {
	mu.Lock()
	defer mu.Unlock()

	//logger.Printf("%s", "db saver stoped")
	if err := db.Close(); err != nil {
		logger.PrintfErr("%v",err)
		//logger.Printf("%s %v", "db.Close() err", err)
	}
}

func Open() {
	mu.Lock()
	defer mu.Unlock()

	//logger.Printf("%s", "try to open database")
	connStr := ConnectionString()
	//logger.Printf("connections string ( %s )", connStr)
	if err := db.Open(connStr); err != nil {
		//logger.Printf("%s %v", "error creating connection pool: ", err)
	} else {
		//logger.Printf("%s", "connected")
		if v, err := db.Version(); err == nil {
			logger.PrintfDebug("%v",v)
			//logger.Printf("%s", v)
		}
	}
}

func IsOpen() bool {
	mu.Lock()
	defer mu.Unlock()

	return db.IsOpen
}

func Ping() bool {
	mu.Lock()
	defer mu.Unlock()

	if err := db.Ping(); err != nil {
		//logger.Printf("%s %v", "db ping error", err)
		return false
	} else {
		//logger.Printf("db ping ok")
		return true
	}
}

func Exec(query string, args ...interface{}) (sql.Result, error) {
	mu.Lock()
	defer mu.Unlock()

	return db.Db.Exec(query, args...)
}

func Query(query string, args ...interface{}) (*sql.Rows, error) {
	mu.Lock()
	defer mu.Unlock()

	if db.Db == nil {
		return &sql.Rows{}, errors.New("database isn't ready")
	}

	return db.Db.Query(query, args...)
}
