package storage

import (
	"errors"
	"fdps/utils"
	"fdps/utils/logger"
	"fmt"
)

type Config struct {
	Server   string
	Port     int
	User     string
	Password string
}

const configName = "./fdps-aftn-storage-config.json"

func (c Config) ConnectionString() string {
	return fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;encrypt=disable",
		c.Server, c.User, c.Password, c.Port)
}

func (c *Config) Load() error {
	err := utils.ReadFromFile(configName, c)
	if err != nil {
		return err
	} else if len(c.Server) == 0 {
		return errors.New("format config file is bad (server name)")
	}
	return nil
}

func (c *Config) Save() error {
	return utils.SaveToFile(configName, c)
}

func (c *Config) SetDefaults() {
	c.Server = "127.0.0.1"
	c.Port = 1433
	c.User = "AftnMaster"
	c.Password = "Arktur"
}

func ConnectionString() string{
	config := Config{}
	if e := config.Load(); e != nil {
		config.SetDefaults()
	}
	if err := config.Save(); err != nil {
		logger.PrintfErr("%v",err)
	}
	return  config.ConnectionString()
}