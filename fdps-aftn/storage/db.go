package storage

import (
	"context"
	"database/sql"
	"errors"
	"fdps/utils/logger"
	"fmt"
	_ "github.com/denisenkom/go-mssqldb"
)

type sqlServer struct {
	Db     *sql.DB
	IsOpen bool
	tx     *sql.Tx
}

func (s *sqlServer) Open(connStr string) error {
	var err error
	if s.Db, err = sql.Open("sqlserver", connStr); err != nil {
		return errors.New(fmt.Sprintf("%s %v", "error creating connection pool: ", err))
	}
	return s.Ping()
}

func (s *sqlServer) Version() (string, error) {
	ctx := context.Background()
	var result string
	if err := s.Db.QueryRowContext(ctx, "SELECT @@version").Scan(&result); err != nil {
		s.IsOpen = false
		return "", errors.New(fmt.Sprintf("%s %v", "SELECT @@version  failed", err))
	}
	return result, nil
}

func (s *sqlServer) Ping() error {
	if s.Db == nil {
		return errors.New(fmt.Sprintf("database wasn't opened"))
	}
	if err := s.Db.Ping(); err != nil {
		s.IsOpen = false
		return errors.New(fmt.Sprintf("%s %v", "db ping error", err))
	}
	s.IsOpen = true
	return nil
}

func (s *sqlServer) Close() error {
	if s.Db == nil {
		return errors.New(fmt.Sprintf("database wasn't opened"))
	}
	s.IsOpen = false
	if err := s.Db.Close(); err != nil {
		return errors.New(fmt.Sprintf("%s %v", "db.Close() err", err))
	}
	return nil
}

func (s *sqlServer) Begin() {
	var err error
	if s.tx, err = s.Db.Begin(); err != nil {
		logger.PrintfErr("Begin %v", err)
	}
}

func (s *sqlServer) Commit() {
	var err error
	if s.tx == nil {
		return
	}
	if err = s.tx.Commit(); err != nil {
		logger.PrintfErr("Commit %v", err)
	}
}

func (s *sqlServer) Rollback() {
	var err error
	if s.tx == nil {
		return
	}
	if err = s.tx.Rollback(); err != nil {
		logger.PrintfErr("Rollback %v", err)
	}
}
