package model

import (
	"time"
)

const DataKeepTime = 30 * 24 * time.Hour

// func TimeOrNull(t time.Time) sql.NullTime {
// 	if t.IsZero() {
// 		return sql.NullTime{}
// 	} else {
// 		return sql.NullTime{Time: t, Valid: true}
// 	}
// }

func ClearDatabase() {
	// удаление телеграмм с датой приема или отправки DataKeepTime дней
	//q := `	set nocount on;
	//		exec dbo.rvmClearExpired @keepDays = @p_keepDays`
	//
	//if _, err := storage.Exec(q, sql.Named("p_keepDays", RvmKeepDays)); err != nil {
	//	logger.Printf("clear database error [%v]", err)
	//} else {
	//	logger.Printf("clear database [ok]")
	//}
}
