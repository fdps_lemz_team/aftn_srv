package model

import (
	"database/sql"
	"fdps/aftn/aftn-srv/fdps-aftn/storage"
	"fdps/aftn/aftn-srv/channel/tlg"
	"fdps/utils"
	"fdps/utils/logger"
	_ "github.com/mattn/go-sqlite3"
	"time"
)

/*
		ID 			int		IDENTITY(1,1) NOT NULL,
		TEXT		text	NULL,
		CH_NUM		char(3) NULL,
		CH_NAME		char(3) NULL,
		AR_TIME		char(14)NULL, // yyyymmddhhnnss
		TLG_TYPE	char(1) NULL,
		RDNum		char(6) NULL,
		AdrFrom		char(8) NULL,
	+	SHA 		varchar(256)
 */

const (
	AftnServerArTimeFormat = "20060102150405"
	TlgTimeLifeForSend     = -25 * time.Hour
)

type TlgTable struct {
	//mu           sync.RWMutex
	LastLoadedId int64
	PrevLoadedId int64
	ChNameInpRus string
	Data         []tlg.Tlg
}

var Telegrams = NewTlgTable()

func NewTlgTable() *TlgTable {
	//tt := &TlgTable{LastLoadedId: 0, Data: make(map[string]*Tlg)}
	//tt := &TlgTable{LastLoadedId: 0, ChNameInpRus: "ТЗЗ"}
	tt := &TlgTable{LastLoadedId: 0}
	return tt
}

func (tt *TlgTable) LocalDbDel(lid int64) {
	if lid >= 0 && lid < int64(len(tt.Data)) {
		tt.Data[lid] = tt.Data[len(tt.Data)-1]
		tt.Data[len(tt.Data)-1] = tlg.Tlg{}
		tt.Data = tt.Data[:len(tt.Data)-1]
	}
}

func (tt *TlgTable) LocalDbClear() {
	if len(tt.Data) > 2048 {
		for i := 0; i < 1024; i++ {
			tt.LocalDbDel(int64(i))
		}
	}

	creTable := `create table if not exists tlg  (
		id integer 
		,tlgtext varchar(8192) 
		,chname varchar(3)
		,chnum varchar(3)
		,rdnum varchar(6)
		,adrfrom varchar(8)
		,qccat varchar(3)
		,addr varchar(512)
		,artime datetime
	);`

	db, err := sql.Open("sqlite3", "./fdps-aftn.db")
	if err != nil {
		logger.PrintfErr("%v", err)
		return
	}
	defer func() { _ = db.Close() }()

	_, err = db.Exec(creTable)
	if err != nil {
		logger.PrintfErr("%v", err)
		return
	}
}

func (tt *TlgTable) LocalDbGet() (int64, *tlg.Tlg, bool) {
	if len(tt.Data) > 0 {
		return 0, &tt.Data[0], true
	}
	return 0, &tlg.Tlg{}, false
}

func (tt *TlgTable) LocalDbPut(t *tlg.Tlg) {
	tt.Data = append(tt.Data, *t)
	_ = utils.SaveToFile("local-cache-tlg.json", tt.Data)
}

func (tt *TlgTable) LoadOne() (*tlg.Tlg, bool) {
	q := " set nocount on;"
	q += " exec dbo.tlgForSend"
	q += " @LastId	= @p_LastId"
	q += ",@ChName	= @p_ChName"

	var (
		rows *sql.Rows
		err  error
	)

	rows, err = storage.Query(q, sql.Named("p_LastId", tt.LastLoadedId),
		sql.Named("p_ChName", tt.ChNameInpRus))

	if err != nil {
		//logger.Printf("exec error [ %v ]", err)
		logger.PrintfErr("%v", err)
		return &tlg.Tlg{}, false
	}

	defer func() { _ = rows.Close() }()

	nt := &tlg.Tlg{}

	if rows.Next() {
		err = rows.Scan(&nt.Id, &nt.Text)
		if nt.Id > tt.LastLoadedId {
			tt.PrevLoadedId = tt.LastLoadedId
			tt.LastLoadedId = nt.Id
		}
	} else {
		logger.PrintfDebug("no for send")
		return &tlg.Tlg{}, false
	}

	return nt, true
}

func (tt *TlgTable) Rollback() {
	tt.LastLoadedId = tt.PrevLoadedId
}

func (tt *TlgTable) Update(t *tlg.Tlg) {
	if len(t.Text) == 0 {
		return
	}

	q := " set nocount on;"
	q += " exec dbo.tlgUpdateAfterSend"
	q += " @Id		= @p_Id"
	q += ",@Text	= @p_Text"
	q += ",@ChNum	= @p_ChNum"

	if len(t.ErrorsString()) == 0 {
		_, err := storage.Exec(q,
			sql.Named("p_Id", t.Id),
			sql.Named("p_Text", t.TextForDb()),
			sql.Named("p_ChNum", t.ChNum))

		if err != nil {
			logger.PrintfErr("%v", err)
			tt.LocalDbPut(t)
			return
		}
	} else {
		q += ",@Errors	= @p_Errors"
		_, err := storage.Exec(q,
			sql.Named("p_Id", t.Id),
			sql.Named("p_Text", t.TextForDb()),
			sql.Named("p_ChNum", t.ChNum),
			sql.Named("p_errors", t.ErrorsString()))

		if err != nil {
			logger.PrintfErr("%v", err)
			tt.LocalDbPut(t)
			return
		}
	}

	logger.PrintfDebug("%s %s", "saved to db", t.TextForDb())
}

func (tt *TlgTable) Append(t *tlg.Tlg) {
	/*
	 @text      varchar(max)
	,@ChName    varchar(3)
	,@ChNum     varchar(3)      = null
	,@RdNum     varchar(3)      = null
	,@AdrFrom   varchar(3)      = null
	,@errors    varchar(2048)   = null
	 */
	q := " set nocount on;"
	q += " exec dbo.tlgAppendRecv"
	q += " @Text	= @p_Text"
	q += ",@ChName	= @p_ChName"
	q += ",@ChNum	= @p_ChNum"
	q += ",@RdNum	= @p_RdNum"
	q += ",@AdrFrom	= @p_AdrFrom"
	q += ",@errors	= @p_errors"

	_, err := storage.Exec(q,
		sql.Named("p_Text", t.TextForDb()),
		sql.Named("p_ChName", t.ChName),
		sql.Named("p_ChNum", t.ChNum),
		sql.Named("p_RdNum", t.RdNum),
		sql.Named("p_AdrFrom", t.AddrFrom),
		sql.Named("p_errors", t.ErrorsString()))

	if err != nil {
		logger.PrintfErr("%v", err)
		tt.LocalDbPut(t)
		return
	}

	logger.PrintfErr("%s %s", "saved to db", t.TextForDb())
}
