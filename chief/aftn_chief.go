package main

import (
	"fdps/utils"
	"log"

	"fdps/aftn/aftn-srv/chiefchan"
	"fdps/aftn/aftn-srv/chiefconfig"
	"fdps/aftn/aftn-srv/chief/web"
	"fdps/utils/logger"
	"fdps/utils/logger/log_ljack"
	"fdps/utils/logger/log_std"
	"fdps/utils/logger/log_web"
)

const (
	appName    = "fdps-aftn-chief"
	appVersion = "2020-01-14 19:26"
)

var workWithDocker bool
var dockerVersion string

func initLoggers() {
	// логгер с web страничкой
	log_web.Initialize(log_web.LogWebSettings{
		StartHttp:  false,
		LogURLPath: utils.AftnChiefLogPath,
		Title:      "AFTN Chief",
	})
	logger.AppendLogger(log_web.WebLogger)
	utils.AppendHandler(log_web.WebLogger)
	log_web.SetVersion(appVersion)

	// логгер с сохранением в файлы
	var ljackSetts log_ljack.LjackSettings
	ljackSetts.GenDefault()
	ljackSetts.FilesName = appName + ".log"
	if err := log_ljack.Initialize(ljackSetts); err != nil {
		logger.PrintfErr("Ошибка инициализации логгера lumberjack. Ошибка: %s", err.Error())
	}
	logger.AppendLogger(log_ljack.LogLjack)

	logger.AppendLogger(log_std.LogStd)

	log.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.LUTC | log.Llongfile)
}

func initDockerInfo() {
	if dockerVersion, dockErr := utils.GetDockerVersion(); dockErr != nil {
		log_web.SetDockerVersion("???")
		logger.PrintfErr(dockErr.Error())

		utils.InitFileBinUtils(
			utils.AppPath()+"/versions",
			utils.AppPath()+"/runningChannels",
			".exe",
			"aftn-channel",
			"AFTN канал",
		)
	} else {
		workWithDocker = true
		log_web.SetDockerVersion(dockerVersion)
	}
}


func main() {
	initLoggers()
	initDockerInfo()

	done := make(chan struct{})

	chiefweb.Start(done) // web страничка контроллера

	chiefConfClient := chiefconfig.NewClient(workWithDocker) // клиент для связи с конфигуратором
	go chiefConfClient.Work()
	go chiefConfClient.Start() // отправляем запрос настроек контроллера

	chiefConfWeb := chiefconfig.NewWebServer() // web сервер для связи конфигуратора и контроллера
	go chiefConfWeb.Work()

	channelCntrl := chiefchan.NewServer(done, workWithDocker) // контроллер каналов
	go channelCntrl.Work()

	// контроллер, формирующий сообщения о состоянии
	hbtCntrl := chiefconfig.NewHeartbeatController(dockerVersion, appVersion)

	for {
		select {

		// получены настройки каналов
		case channelSetts := <-chiefConfClient.ChannelSettsChan:
			go hbtCntrl.Work()
			channelCntrl.ChannelSettsChan <- channelSetts

		// получены настройки Web сервера контроллера
		case chiefWebSetts := <-chiefConfClient.ChiefWebSetts:
			chiefConfWeb.SettingsChan <- chiefWebSetts

		// получено состояние каналов
		case curStates := <-channelCntrl.ChannelStates:
			chiefweb.SetChannelStates(curStates)
			hbtCntrl.ChannelStateChan <- curStates

		// получено сообщение с состоянием контроллера
		case curHbtMsg := <-hbtCntrl.HeartbeatChannel:
			chiefConfClient.HeartbeatChan <- curHbtMsg
		}
	}
}
