package chiefweb

import (
	"net/http"

	"fdps/utils/logger"
)

type ChiefChannelsHandler struct {
	handleURL string
	title     string
}

var ChiefChanHandler ChiefChannelsHandler

func (cw ChiefChannelsHandler) Path() string {
	return cw.handleURL
}

func (cw ChiefChannelsHandler) Caption() string {
	return cw.title
}

func (ch ChiefChannelsHandler) HttpHandler() func(http.ResponseWriter, *http.Request) {
	return chiefChannelsHandler
}

func InitChiefChannelsHandler(handleURL string, title string) {
	ChiefChanHandler.handleURL = "/" + handleURL
	ChiefChanHandler.title = title
}

func chiefChannelsHandler(w http.ResponseWriter, r *http.Request) {
	if err := srv.chiefChanPage.chiefChanTempl.ExecuteTemplate(w, "Chief", srv.chiefChanPage); err != nil {
		logger.PrintfErr("Ошибка выполнения HTML шаблона ChiefCahnnels. Ошибка: %s.", err)
	}
}
