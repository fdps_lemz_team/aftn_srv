package chiefweb

import (
	"html/template"
	"sync"

	"fdps/aftn/aftn-srv/channel/state"
	"fdps/utils/logger"
)

type ChiefChannelsPage struct {
	sync.RWMutex
	chiefChanTempl *template.Template
	Title          string

	ChannelStates []chanstate.State
}

func (cp *ChiefChannelsPage) initialize() {
	cp.Lock()
	defer cp.Unlock()

	var err error
	if cp.chiefChanTempl, err = template.New("Chief").Parse(ChiefTmpl); err != nil {
		logger.PrintfErr("ChiefChannelsPage template Parse ERROR: %v", err)
		return
	}
	cp.Title = ChiefChanHandler.Caption()
}

var ChiefTmpl = `{{define "Chief"}}
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>{{.Title}}</title>
		<script>
		//    window.onload = function() {
		//        setTimeout(function () {
		//            location.reload()
		//        }, 1000);
		//     };
		</script>
	</head>
	<body>
		<font size="3" face="verdana" color="black">

		<p style="font-weight:bold">AFTN каналы</p>

		<table width="100%" border="1" cellspacing="0" cellpadding="4" >
			<caption style="font-weight:bold">AFTN каналы</caption>
			<tr>
				<th>ID</th>
				<th>URL</th>
				<th>На прием</th>
				<th>На передачу</th>
				<th>Подкл. к ЦКС</th>
				<th>WS для связи с FDPS</th>
				<th>FDPS подключения</th>
				<th>Принято за сутки</th>
				<th>Передано за сутки</th>
				<th>Последняя принятая</th>
				<th>Последняя переданная</th>
				<th>Статус обновлен</th>
			</tr>
			{{with .ChannelStates}}
				{{range .}}
					<tr align="center" bgcolor="{{.StateColor}}">
						<td align="left"> {{.ID}} </td>
						<td align="left"> <a href="{{.URL}}" style="display:block;">{{.URL}}</a> </td>
						<td align="left"> {{.InpName}} </td>
						<td align="left"> {{.OutName}} </td>
						<td align="left"> {{.CcmConnect}} </td>
						<td align="left"> {{.FdpsWsState}} </td>
						<td align="left"> {{.FdpsWsClients}} </td>
						<td align="left"> {{.DailyInpTlgCount}} </td>
						<td align="left"> {{.DailyOutTlgCount}} </td>
						<td align="left"> {{.LastInpTlgDateTime}} </td>
						<td align="left"> {{.LastOutTlgDateTime}} </td>
						<td align="left"> {{with .RuntimeInfo}} {{.NowTimeUTC}} {{end}}</td>
					</tr>
				{{end}}
			{{end}}
		</table>
		</font>
	</body>
</html>
{{end}}
`
