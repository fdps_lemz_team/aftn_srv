// web
package chiefweb

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"fdps/aftn/aftn-srv/channel/state"
	"fdps/utils"
	"fdps/utils/logger"
)

type httpServer struct {
	http.Server
	shutdownReq chan bool
	done        chan struct{}
	//reqCount      uint32
	chiefChanPage *ChiefChannelsPage
}

var srv httpServer

func Start(done chan struct{}) {
	//wsc.load()
	//router := mux.NewRouter()

	srv = httpServer{
		Server: http.Server{
			//Handler:      router,
			Addr:         ":" + strconv.Itoa(utils.AftnChiefWebDefaultPort),
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 10 * time.Second,
		},
		//shutdownReq: sdc,
		done:          done,
		chiefChanPage: new(ChiefChannelsPage),
	}

	InitChiefChannelsHandler(utils.AftnChiefChannelsPath, "FDPS-AFTN-CHIEF-CHANNELS")
	utils.AppendHandler(ChiefChanHandler)

	srv.chiefChanPage.initialize()

	for _, h := range utils.HandlerList {
		//router.HandleFunc(h.Path(), h.HttpHandler())
		http.HandleFunc(h.Path(), h.HttpHandler())
	}

	logger.PrintfDebug("Запущен HTTP сервер страницы AFTN контроллера на порту %d", utils.AftnChiefWebDefaultPort)

	go func() {
		err := srv.ListenAndServe()
		if err != nil {
			logger.PrintfErr("Ошибка запуска HTTP сервера страницы AFTN контроллера. Ошибка: %v", err)
		}
	}()
}

func (s *httpServer) stop() {
	logger.PrintfInfo("Закрытия HTTP сервера страницы AFTN контроллера")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	defer cancel()

	err := s.Shutdown(ctx)

	if err != nil {
		logger.PrintfErr("Ошибка закрытия HTTP сервера страницы AFTN контроллера. Ошибка: %v", err)
	}
}

func SetChannelStates(states []chanstate.State) {
	srv.chiefChanPage.Lock()
	defer srv.chiefChanPage.Unlock()

	srv.chiefChanPage.ChannelStates = append([]chanstate.State(nil), states...)
}
