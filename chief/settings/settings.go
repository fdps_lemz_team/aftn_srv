package chiefsetts

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"

	"fdps/aftn/aftn-srv/channel/settings"
	"fdps/aftn/aftn-srv/chieflogger"
	"fdps/utils"
)

// ChiefSettings настройки контроллера(chief)
type ChiefSettings struct {
	IPAddr         string `json:"ControllerIP"`    // IP адрес контроллера
	CntrlID        int    `json:"ControllerID"`    // идентификатор контроллера
	Timestamp      string `json:"ConfigTimestamp"` // метка времени
	ChannelsPort   int    `json:"ChannelsPort"`    // TCP порт для связи с каналами.
	DockerRegistry string `json:"DockerRegistry"`  // репозиторий с docker образами каналовы
	ShowDebugMgs   bool   `json:"ShowDebugMgs"`    // показывать отладочные сообщений в журнале

	WebURL         string               `json:"ControllerWebUrl"`
	WebPort        int                  `json:"ControllerWebPort"`
	LoggerSettings chieflogger.Settings `json:"LoggerSettings"` // настройки связи с логгером
	ChannelSetts   []chansetts.Settings `json:"AftnChannels"`   // настройки каналов
	IsInitialised  bool                 `json:"-"`              // признак инициализации настроек (либо получены от конфигуратора, либо считаны из файла)
}

var chiefSettingsFile = utils.AppPath() + "/config/chief_settings.json"

// ReadFromFile чтение ранее сохраненных настроек из файла
func (cs *ChiefSettings) ReadFromFile() error {
	data, err := ioutil.ReadFile(chiefSettingsFile)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(data, &cs); err != nil {
		fmt.Println(err.Error())
		return err
	}
	return nil
}

// SaveToFile сохранение настроек в файл
func (cs *ChiefSettings) SaveToFile() error {
	confData, err := json.Marshal(cs)
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile(chiefSettingsFile, utils.JsonPrettyPrint(confData), os.ModePerm); err != nil {
		return err
	}
	return nil
}
