package runtimeinfo

const RuntimeInfoKey = "RuntimeInfo"

// RuntimeInfo вседения о runtime состоянии приложения
type RuntimeInfo struct {
	InWork           string `json:"InWork"`           // времени в работе
	NowTimeUTC       string `json:"NowTimeUTC"`       // текущее время на АРМ
	GoroutineCount   int    `json:"GoroutineCount"`   // кол-во горутин
	GcCount          int    `json:"GcCount"`          // кол-во проходов garbage collector
	SysMemory        string `json:"SysMemory"`        // размер системной памяти
	HeapInuseMemory  string `json:"HeapInuseMemory"`  // размер памяти, выделенной в куче
	StackInuseMemory string `json:"StackInuseMemory"` // размер памяти, выделенной на стэке
	InMemoryObjCount int    `json:"InMemoryObjCount"` // кол-во объектов в памяти
}
