package runtimeinfo

import (
	"fmt"
	"runtime"
	"strconv"
	"time"
)

const bytesInMB float64 = 1 << 20 // bytesInMB кол-во байт в 1 bytesInMB
const timeFormat = "2006-01-02 15:04:05.000"

// контроллер, раз в секунду высылающий сведения о runtime состоянии приложения
type RuntimeInfoController struct {
	InfoChan   chan RuntimeInfo
	infoTicker *time.Ticker
	startTime  time.Time
}

// NewController - конструктор
// intervalSecs - интервал отправки сведений по каналу InfoChan (сек.)
func NewController(intervalSecs int) *RuntimeInfoController {
	return &RuntimeInfoController{
		InfoChan:   make(chan RuntimeInfo, 1),
		infoTicker: time.NewTicker(time.Duration(intervalSecs) * time.Second),
	}
}

// Work - основной цикл работы
func (ric *RuntimeInfoController) Work() {
	ric.startTime = time.Now().UTC()

	for {
		select {
		case <-ric.infoTicker.C:
			var curInfo RuntimeInfo
			curInfo.GoroutineCount = runtime.NumGoroutine()

			var memStat runtime.MemStats
			runtime.ReadMemStats(&memStat)

			curInfo.SysMemory = strconv.FormatFloat(float64(memStat.Sys)/bytesInMB, 'g', 3, 64) + " MB"
			curInfo.HeapInuseMemory = strconv.FormatFloat(float64(memStat.HeapInuse)/bytesInMB, 'g', 3, 64) + " MB"
			curInfo.StackInuseMemory = strconv.FormatFloat(float64(memStat.StackInuse)/bytesInMB, 'g', 3, 64) + " MB"
			curInfo.GcCount = int(memStat.NumGC)
			curInfo.InMemoryObjCount = int(memStat.HeapObjects)

			secDuration := uint64(time.Since(ric.startTime).Seconds())
			days := strconv.FormatUint(secDuration/(24*60*60), 10)
			hours := strconv.FormatUint((secDuration/(60*60))%24, 10)
			mins := strconv.FormatUint((secDuration/60)%60, 10)
			secs := strconv.FormatUint(secDuration%60, 10)
			curInfo.InWork = fmt.Sprintf("%s дн. %s ч. %s м. %s с.", days, hours, mins, secs)

			curInfo.NowTimeUTC = time.Now().UTC().Format(timeFormat)

			ric.InfoChan <- curInfo
		}
	}
}
