package chiefconfig

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"fdps/aftn/aftn-srv/channel/settings"
	"fdps/aftn/aftn-srv/chief/settings"
	"fdps/utils"
	"fdps/utils/logger"
)

// ChiefCfg настройки контроллера каналов(chief)
var ChiefCfg chiefsetts.ChiefSettings = chiefsetts.ChiefSettings{CntrlID: -1, IPAddr: "127.0.0.1", IsInitialised: false}

const (
	ChannelImageName = "aftn_channel" // ChannelImageName имя образа приложения "AFTN канал"

	lastPostTimeKey        = "Время последнего POST:"
	lastSuccessPostTimeKey = "Время последнего успешного POST:"
	lastFailPostTimeKey    = "Время последнего неуспешного POST:"
	lastPostErrorKey       = "Последняя ошибка POST:"
	timeFormat             = "2006-01-02 15:04:05"
)

// клиент для подключения к конфигуратору
type Client struct {
	HeartbeatChan    chan HeartbeatMsg               // канал для приема сообщений о состоянии контроллера
	ChannelSettsChan chan chansetts.SettingsWithPort // канал для передачи настроек AFTN каналов
	//LoggerSettsChan  chan chieflogger.Settings                // канал для передачи настроек логгера
	ChiefWebSetts chan WebServerSetts // канал для передачи настроек Web сервера контроллера

	channelVersions        []string // ChannelVersion список версий исполняемых файлов/docker контейнеров AFTN канала
	configUrls             ConfiguratorUrls
	lastPostErr            error
	lastHbtErr             error
	postResultChan         chan json.RawMessage
	readLocalSettingsTimer *time.Timer // таймер, по которому настройки считаются из локального файла
	workWithDocker         bool
}

// NewChiefClient конструктор клиента
func NewClient(withDocker bool) *Client {
	return &Client{
		HeartbeatChan:    make(chan HeartbeatMsg, 10),
		ChannelSettsChan: make(chan chansetts.SettingsWithPort),
		//LoggerSettsChan:        make(chan common.LoggerSettings),
		ChiefWebSetts:          make(chan WebServerSetts),
		postResultChan:         make(chan json.RawMessage, 10),
		readLocalSettingsTimer: time.NewTimer(time.Second * 5),
		workWithDocker:         withDocker,
	}
}

// Work рабочий цикл
func (cl *Client) Work() {
	logger.SetDebugParam(lastPostTimeKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(lastFailPostTimeKey, "-", logger.StateWarningColor)
	logger.SetDebugParam(lastSuccessPostTimeKey, "-", logger.StateOkColor)
	logger.SetDebugParam(lastPostErrorKey, "-", logger.StateWarningColor)

	for {
		select {

		// результат выполнения POST запроса
		case postRes := <-cl.postResultChan:
			var msgHeader MessageHeader
			if unmErr := json.Unmarshal(postRes, &msgHeader); unmErr != nil {
				logger.PrintfErr("Ошибка разбора (unmarshall) сообщения от конфигуратора. Сообщение: %s. Ошибка: %s.", string(postRes), unmErr.Error())
			} else {
				switch msgHeader.Header {
				// ответ на запрос настроек
				case AnswerSettingsHeader:
					if unmErr = json.Unmarshal(postRes, &ChiefCfg); unmErr != nil {
						logger.PrintfErr("Ошибка разбора (unmarshall) ответа на запрос настроек. Сообщение: %s. Ошибка: %s.", string(postRes), unmErr.Error())
					} else {
						//chiefCfg = &curMsg
						ChiefCfg.IsInitialised = true
						cl.readLocalSettingsTimer.Stop()

						// todo
						if len(ChiefCfg.DockerRegistry) == 0 {
							ChiefCfg.DockerRegistry = "di.topaz-atcs.com"
						}
						// сохраняем настройки в файл
						ChiefCfg.SaveToFile()
						if !ChiefCfg.ShowDebugMgs {
							logger.SetMinSeverity(logger.SevInfo)
						}

						// отправляем настройки
						go cl.sendSettings()
					}

				// ответ на сообщение о состоянии контроллера
				case HeartbeatAnswerHeader:
					var curMsg HeartbeatAnswerMsg
					if unmErr := json.Unmarshal(postRes, &curMsg); unmErr != nil {
						logger.PrintfErr("Ошибка разбора (unmarshall) ответа на сообщение о состоянии контроллера. Сообщение: %s. Ошибка: %s.", string(postRes), unmErr.Error())

					} else {
						if curMsg.ConfigTimestamp != ChiefCfg.Timestamp {
							go cl.postSettingsRequest()
						}
					}

				default:
					logger.PrintfErr("Получено сообщение с неизвестным заголовком от конфигуратора. Загогловок: %s.", msgHeader.Header)
				}
			}

		// получено собщение о состоянии контроллера
		case hbMgs := <-cl.HeartbeatChan:
			go cl.postHeartbeat(&hbMgs)

		// сработал таймер считывания настроек из файла
		case <-cl.readLocalSettingsTimer.C:
			if fileErr := ChiefCfg.ReadFromFile(); fileErr != nil {
				logger.PrintfErr("Ошибка чтения настроек контроллера из файла. Ошибка: %s.", fileErr.Error())
			} else {
				logger.PrintfWarn("Настройки контроллера считаны из файла.")
				// отправляем настройки
				go cl.sendSettings()
			}
		}
	}
}

// Start запуск взаимодействия с конфигуратором
func (cl *Client) Start() {
	if err := cl.configUrls.Read(); err != nil {
		logger.PrintfErr("Ошибка чтения файла с URL конфигуратора. Ошибка: %s.", err.Error())
		return
	}
	cl.initChannelVersions()
	go cl.postSettingsRequest()
}

// отправка запроса настроек конфигуратору
func (cl *Client) postSettingsRequest() {
	curSettingsRequest := CreateSettingsRequestMsg(cl.channelVersions)
	if postErr := cl.postToConfigurator(cl.configUrls.SettingsURLStr, curSettingsRequest); postErr != nil {
		if cl.lastPostErr == nil || cl.lastPostErr.Error() != postErr.Error() {
			cl.lastPostErr = postErr
			logger.PrintfErr("Ошибка запроса настроек у конфигуратора. Запрос: %v. URL: %s. Ошибка: %s",
				curSettingsRequest, cl.configUrls.SettingsURLStr, postErr.Error())
		}

		// если настройки не инициализированы, снова их запрашиваем
		// при считывании из файла настроки не инициализируются (только при получении от конфигуратора)
		if !ChiefCfg.IsInitialised {
			time.AfterFunc(5*time.Second, func() { go cl.postSettingsRequest() })
		}
	}
}

// отправка конфигуратору состояния контроллера
func (cl *Client) postHeartbeat(hbtMsg *HeartbeatMsg) {
	if postErr := cl.postToConfigurator(cl.configUrls.HeartbeatURLStr, *hbtMsg); postErr != nil {
		if cl.lastHbtErr == nil || cl.lastHbtErr.Error() != postErr.Error() {
			cl.lastHbtErr = postErr
			logger.PrintfErr("Ошибка отправки состояния контроллера. Запрос: %v. URL: %s. Ошибка: %s",
				*hbtMsg, cl.configUrls.HeartbeatURLStr, postErr.Error())
		}
	}
}

// отправка POST сообщения конфигуратору и возврат ответа
func (cl *Client) postToConfigurator(url string, msg interface{}) error {
	errFunc := func(err error) error {
		logger.SetDebugParam(lastFailPostTimeKey, time.Now().Format(timeFormat), logger.StateWarningColor)
		logger.SetDebugParam(lastPostErrorKey, err.Error(), logger.StateWarningColor)
		return err
	}

	jsonValue, _ := json.Marshal(msg)
	resp, postErr := http.Post(url, "application/json", bytes.NewBuffer(jsonValue))
	logger.SetDebugParam(lastPostTimeKey, time.Now().Format(timeFormat), logger.StateDefaultColor)
	if postErr == nil {
		defer resp.Body.Close()
		if strings.Contains(resp.Status, "200") {
			if body, readErr := ioutil.ReadAll(resp.Body); readErr == nil {
				if bytes.Contains(body, []byte("error")) {
					return errFunc(fmt.Errorf("Не валидный пакет: %s", string(body)))
				}
				if ind := strings.Index(string(body), "{"); ind >= 0 {
					cl.postResultChan <- body[ind:]
				} else {
					return errFunc(fmt.Errorf("Не валидный пакет: %s", string(body)))
				}
			} else {
				return errFunc(readErr)
			}
		} else {
			return errFunc(fmt.Errorf("Статус ответа: %s", resp.Status))
		}
	} else {
		return errFunc(postErr)
	}

	logger.SetDebugParam(lastSuccessPostTimeKey, time.Now().Format(timeFormat), logger.StateOkColor)
	return nil
}

// инициализация до отправки запроса настроек у конфигуратора
// в POST запросе должны быть указаны версии каналов
func (cl *Client) initChannelVersions() {
	var versErr error

	if cl.workWithDocker {
		if cl.channelVersions, versErr = utils.GetDockerImageVersions(ChannelImageName); versErr != nil {
			logger.PrintfErr("Ошибка получения списка версий docker образов 'AFTN канал'. Ошибка: %s.", versErr.Error())
		} else {
			//web.SetChannelVersions(fmt.Sprintf("%v", ChannelVersions))
			logger.PrintfInfo("Получены версии doсker образов приложения 'AFTN канал'. Версии: %s.", cl.channelVersions)
		}

		// останавливаем и удаляем ранее запущенные контейнеры AFTN каналов
		if stopErr := utils.StopAndRmContainers(ChannelImageName); stopErr != nil {
			logger.PrintfErr("Ошибка остановки и удаления docker контейнеров приложения 'AFTN канал'. Ошибка: %s.", stopErr.Error())
		}

		// выкачиваем docker образы из репозитория
		if pullErr := utils.DockerPullImages(ChiefCfg.DockerRegistry); pullErr != nil {
			logger.PrintfErr("Ошибка загрузки docker образов (pull) приложения 'AFTN канал'. Ошибка: %s", pullErr.Error())
		} else {
			logger.PrintfInfo("Загружены docker образы приложения 'AFTN канал'. Образы: %s.", ChiefCfg.DockerRegistry)
		}
	} else {
		if cl.channelVersions, versErr = utils.GetChannelVersions(); versErr != nil {
			logger.PrintfErr("Ошибка получения списка версий приложения 'AFTN канал'. Ошибка: %s.", versErr.Error())
		} else {
			logger.PrintfInfo("Получены версии приложения 'AFTN канал'. Версии: %v.", cl.channelVersions)
		}
	}
}

// отправляе настройки каналам, провайдерам, клиенту логгера
func (cl *Client) sendSettings() {
	// добавляем в настройки URL
	for ind, _ := range ChiefCfg.ChannelSetts {
		ChiefCfg.ChannelSetts[ind].URLAddress = ChiefCfg.IPAddr
		ChiefCfg.ChannelSetts[ind].URLPath = utils.AftnChannelsPath
		ChiefCfg.ChannelSetts[ind].URLPort = utils.AftnChannelBeginPort + ChiefCfg.ChannelSetts[ind].ID
	}

	// отправляем настройки каналов
	cl.ChannelSettsChan <- chansetts.SettingsWithPort{
		ChSettings: ChiefCfg.ChannelSetts,
		ChPort:     ChiefCfg.ChannelsPort,
	}

	// отправляем настройки логгера
	//cc.LoggerSettsChan <- ChiefCfg.LoggerSetts

	// настройки web сервера контроллера
	cl.ChiefWebSetts <- WebServerSetts{handleURL: ChiefCfg.WebURL, netPort: ChiefCfg.WebPort}
}
