package chiefconfig

import (
	"context"
	"net/http"
	"reflect"
	"strconv"
	"time"

	"github.com/gorilla/mux"

	"fdps/utils/logger"
)

type WebServerSetts struct {
	handleURL string
	netPort   int
}

type WebServer struct {
	httpServer   *http.Server
	doneChan     chan struct{}
	SettingsChan chan WebServerSetts
	curSetts     WebServerSetts
}

func NewWebServer() *WebServer {
	return &WebServer{
		doneChan:     make(chan struct{}),
		SettingsChan: make(chan WebServerSetts, 1),
	}
}

func (ws *WebServer) Work() {
	for {
		select {
		case newSetts := <-ws.SettingsChan:
			if !reflect.DeepEqual(ws.curSetts, newSetts) {
				logger.PrintfInfo("Получены новые настройки Web сервера контроллера: %#v", newSetts)
				ws.curSetts = newSetts

				if ws.httpServer != nil {
					ws.stopNetServer()
				}

				router := mux.NewRouter()
				router.HandleFunc("/"+ws.curSetts.handleURL, ws.handlerMain)

				ws.httpServer = &http.Server{
					Handler:      router,
					Addr:         ":" + strconv.Itoa(ws.curSetts.netPort),
					ReadTimeout:  10 * time.Second,
					WriteTimeout: 10 * time.Second,
				}

				logger.PrintfDebug("Запущен HTTP сервер для связи с конфигуратором на порту %d", ws.curSetts.netPort)
				go func() {
					if err := ws.httpServer.ListenAndServe(); err != nil {
						logger.PrintfErr("Ошибка запуска Web сервера контроллера. Ошибка: <%s>", err.Error())
					}
				}()
			}
		case <-ws.doneChan:

		}
	}

}

func (ws *WebServer) handlerMain(w http.ResponseWriter, r *http.Request) {
	logger.PrintfInfo("HTTP HEADER: %v", r.Header)
	var bodyData []byte
	if _, errBody := r.Body.Read(bodyData); errBody != nil {
		logger.PrintfErr("ERROR READ HTTP BODY: %v", errBody)
	} else {
		logger.PrintfInfo("HTTP BODY: %v", string(bodyData))
	}
	w.WriteHeader(http.StatusOK)
}

func (ws *WebServer) stopNetServer() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	ws.httpServer.SetKeepAlivesEnabled(false)
	if err := ws.httpServer.Shutdown(ctx); err != nil {
		logger.PrintfErr("Ошибка закрытия Web сервера контроллера.", err.Error())
	} else {
		logger.PrintfInfo("Web сервера контроллера закрыт.")
	}
}
