package chiefconfig

import (
	"time"

	"fdps/aftn/aftn-srv/channel/state"
	"fdps/aftn/aftn-srv/chieflogger"
	"fdps/aftn/aftn-srv/runtimeinfo"
)

const (
	CommonStateOk    = "ok"
	CommonStateError = "error"
)

// HeartbeatController контроллер отвечает за прием сообщений о состоянии каналов.
// Аккумулирует состояния и отправляет раз в секунду сообщение с общим состоянием контроллера
type HeartbeatController struct {
	ChannelStateChan chan []chanstate.State // список состояний AFTN каналов
	HeartbeatChannel chan HeartbeatMsg
	runtumeCntrl     *runtimeinfo.RuntimeInfoController

	sendTicker      *time.Ticker
	curHeartbeatMsg HeartbeatMsg
	appVersion      string
	dockerVersion   string
}

// NewHeartbeatController конструктор
func NewHeartbeatController(dockerVers string, appVers string) *HeartbeatController {
	return &HeartbeatController{
		ChannelStateChan: make(chan []chanstate.State, 10),
		HeartbeatChannel: make(chan HeartbeatMsg, 10),
		runtumeCntrl:     runtimeinfo.NewController(1),
		sendTicker:       time.NewTicker(time.Second),

		curHeartbeatMsg: HeartbeatMsg{MessageHeader: MessageHeader{Header: HeartbeatHeader},
			CommonState: CommonStateError,
		},
		appVersion:    appVers,
		dockerVersion: dockerVers,
	}
}

// Work рабочий цикл контроллера
func (hc *HeartbeatController) Work() {
	// work вызывается после получения настроек каналов, поэтому ChiefCfg готов к использованию
	hc.curHeartbeatMsg.ControllerID = ChiefCfg.CntrlID
	hc.curHeartbeatMsg.ControllerIP = ChiefCfg.IPAddr
	hc.curHeartbeatMsg.ControllerVersion = hc.appVersion
	hc.curHeartbeatMsg.DockerVersion = hc.dockerVersion

	go hc.runtumeCntrl.Work()

	for {
		select {
		// принято состояние FMTP канала
		case channelMgs := <-hc.ChannelStateChan:
			hc.curHeartbeatMsg.ChannelStates = channelMgs

		case hc.curHeartbeatMsg.RuntimeInfo = <-hc.runtumeCntrl.InfoChan:

		// сработал таймер отправки собщения о состоянии
		case <-hc.sendTicker.C:
			hc.curHeartbeatMsg.CommonState = CommonStateOk
		CHSTL:
			for _, it := range hc.curHeartbeatMsg.ChannelStates {
				if it.CommonState != chanstate.StateOk {
					hc.curHeartbeatMsg.CommonState = CommonStateError
					break CHSTL
				}
			}

			if hc.curHeartbeatMsg.LoggerState.LoggerConnected != chieflogger.LoggerStateOk {
				hc.curHeartbeatMsg.CommonState = CommonStateError
			}

			hc.HeartbeatChannel <- hc.curHeartbeatMsg
		}
	}
}
