package chiefconfig

import (
	//"fmt"

	"fdps/aftn/aftn-srv/channel/state"
	"fdps/aftn/aftn-srv/chief/settings"
	"fdps/aftn/aftn-srv/chieflogger"
	"fdps/aftn/aftn-srv/runtimeinfo"

	"fdps/utils"
)

// от конфигуратора могут быть получены сообщения:
//		- настройки контроллера(chief)
// 		- ответ на сообщени о состоянии(содержит временную метку актуальных настроек)
// конфигуратору отправляется соообщение:
//		- запрос настроек контроллера(chief)
//		- сообщение о состоянии контроллера(chief)

const (
	// RequestSettingsHeader заголовок сообщения запроса настроек контроллера
	RequestSettingsHeader = "SettingsRequest"
	// AnswerSettingsHeader заголовок сообщения с настройками контроллера
	AnswerSettingsHeader = "SettingsAnswer"
	// HeartbeatHeader заголовок сообщения о состоянии контроллера
	HeartbeatHeader = "ControllerState"
	// HeartbeatAnswerHeader ответ на сообщение о состоянии контроллера
	HeartbeatAnswerHeader = "WebServerState"
)

// MessageHeader заголовок сообщений протокола
type MessageHeader struct {
	Header string `json:"MessageHeader"`
}

// SettingsRequestMsg сообщение запроса настроек контроллера
// контроллер (chief) -> конфигуратор
type SettingsRequestMsg struct {
	MessageHeader
	Versions    []string `json:"AvailableVersions"` // список доступных версий приложения 'канал'
	IPAddresses []string `json:"ControllerIPs"`     // список сетевых адресов контроллера
}

// CreateSettingsRequestMsg формирование запроса настроек
func CreateSettingsRequestMsg(versions []string) SettingsRequestMsg {
	curIPAddresses := utils.GetLocalIpv4List()
	//web.SetIPAddresses(fmt.Sprintf("%v", curIPAddresses))
	return SettingsRequestMsg{MessageHeader: MessageHeader{Header: RequestSettingsHeader},
		Versions: versions, IPAddresses: curIPAddresses}
}

// SettingsAnswerMsg сообщение c настройками контроллера
// конфигуратор -> контроллер (chief)
type SettingsAnswerMsg struct {
	MessageHeader
	chiefsetts.ChiefSettings
}

// HeartbeatMsg сообщение о состоянии контроллера
// контроллер (chief) -> конфигуратор
type HeartbeatMsg struct {
	MessageHeader
	ControllerID       int                     `json:"ControllerID"` // идентификатор контроллера
	ControllerIP       string                  `json:"ControllerIP"` // IP адрес контроллера
	CommonState        string                  `json:"CommonState"`
	CommonErrorMessage string                  `json:"CommonErrorMessage"`
	ControllerVersion  string                  `json:"ControllerVersion"`
	DockerVersion      string                  `json:"DockerVersion"`
	LoggerState        chieflogger.State       `json:"LoggerState"`   // состояние логгера
	ChannelStates      []chanstate.State       `json:"ChannelStates"` // состояние AFTN каналов
	RuntimeInfo        runtimeinfo.RuntimeInfo `json:"RuntimeInfo"`   // runtime состояние контроллера
}

// HeartbeatAnswerMsg сообщение - ответ на сообщение о состоянии контроллера
// конфигуратор -> контроллер (chief)
type HeartbeatAnswerMsg struct {
	MessageHeader
	ConfigTimestamp string `json:"ConfigTimestamp"` // временная метка акуальных настроек
	WebState        string `json:"WebServerState"`
}

// StopTransportRequest сообщение - запрос остановки передачи сообщений канала
// конфигуратор -> контроллер (chief)
type StopTransportRequestMsg struct {
	MessageHeader
	CntrlID   int `json:"ControllerID"` // идентификатор контроллера
	ChannelId int `json:"ChannelID"`    // идентифкаторо канала.
}

// RestoreTransportRequest сообщение - запрос восстановления передачи сообщений канала
// конфигуратор -> контроллер (chief)
type RestoreTransportRequestMsg struct {
	MessageHeader
	CntrlID   int `json:"ControllerID"` // идентификатор контроллера
	ChannelId int `json:"ChannelID"`    // идентифкаторо канала.
}
