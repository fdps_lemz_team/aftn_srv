package chieflogger

const (
	LoggerStateOk    = "ok"
	LoggerStateError = "error"
)

// состояние подключения к логгеру
type State struct {
	LoggerConnected string `json:"LoggerConnected"`
	LoggerLastError string `json:"LoggerLastError"`
}
