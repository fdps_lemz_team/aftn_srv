package chieflogger

// Settings настройки сервиса логирования
type Settings struct {
	LoggerIP   string `json:"LoggerIP"`
	LoggerPort string `json:"LoggerPort"`
}
