package chieflogger

//import (
//	"fmtp_pkg/logger/common"
//)

const (
	LogMessagesHeader    = "LogMessagesHeader"   // заголовок сообщения массива логов
)

// заголовок сообщений протокола
type MessageHeader struct {
	Header string `json:"MessageHeader"`
}

// сообщение с массивом логов
// контроллер (chief) -> логгер
type LoggerMsgSlice struct {
	MessageHeader
	//Messages []common.LogMessage `json:"LogMessageVector"` // массив сообщений
}
