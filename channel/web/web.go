// web
package chanweb

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"fdps/utils"
	"fdps/utils/logger"
)

const (
	FailStartWeb = 1005 // ошибка запуска web сервера
)

type httpServer struct {
	http.Server
	shutdownReq chan bool
	done        chan struct{}
	reqCount    uint32
}

var srv httpServer

func Start(httpConf WebServerConf, done chan struct{}) {
	doneC := done
	//if err := wsc.load(); err != nil {
	//	logger.PrintfErr("%v", err)
	//	wsc.setDefault()
	//}
	_ = httpConf.save()

	var sdc chan bool

	for _, h := range utils.HandlerList {
		http.HandleFunc(h.Path(), h.HttpHandler())
	}

	srv = httpServer{
		Server: http.Server{
			Addr:         fmt.Sprintf(":%d", httpConf.Port),//wsc.Port),
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 10 * time.Second,
		},
		shutdownReq: sdc,
		done:        doneC,
	}

	//logger.PrintfDebug("listening on %d port started", httpConf.Port)//wsc.Port)

	go func() {
		err := srv.ListenAndServe()
		if err != nil {
			logger.PrintfDebug("listen and serve error : %v", err)
		}
	}()
}

func (s *httpServer) stop() {
	logger.PrintfDebug("stoping http server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)

	defer cancel()

	err := s.Shutdown(ctx)

	if err != nil {
		logger.PrintfErr("shutdown http server error: %v", err)
	}
}

func WaitShutdown() int {
	go func() {
		for {
			select {
			case <-time.After(1 * time.Minute):
				logger.PrintfDebug("start GC")
				runtime.GC()
			case <-srv.done:
				return
			}
		}
	}()

	irqSig := make(chan os.Signal, 1)

	signal.Notify(irqSig, syscall.SIGINT, syscall.SIGTERM)

	select {
	case sig := <-irqSig:
		logger.PrintfDebug("shutdown signal: %v", sig)
	case sig := <-srv.shutdownReq:
		logger.PrintfDebug("shutdown request (/shutdown %v)", sig)
	case <-srv.done:
		logger.PrintfDebug("web server didn't start")
		return FailStartWeb
	}

	close(srv.done)

	srv.stop()

	return 0
}
