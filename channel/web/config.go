package chanweb

import (
	"fdps/utils"
	"fmt"
)

type WebServerConf struct {
	Port uint16 `json:"port"`
}

const configName = "./fdps-aftn-web-config.json"

var wsc WebServerConf

func (c *WebServerConf) load() error {

	if err := utils.ReadFromFile(configName, c); err != nil {
		return err
	}

	if c.Port == 0 {
		return fmt.Errorf("bad format config file (port %d) ", c.Port)
	}

	return nil
}

func (c *WebServerConf) save() error {
	return utils.SaveToFile(configName, c)
}

func (c *WebServerConf) setDefault() {
	c.Port = 80
}
