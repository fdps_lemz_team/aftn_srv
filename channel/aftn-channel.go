package main

import (
	"os"
	"strconv"

	"fdps/aftn/aftn-srv/channel/web"
	"fdps/aftn/aftn-srv/channel/worker"
	"fdps/aftn/aftn-srv/chiefchan"
	"fdps/utils"
	"fdps/utils/logger"
	"fdps/utils/logger/log_ljack"
	"fdps/utils/logger/log_std"
	"fdps/utils/logger/log_web"
)

const (
	appName    = "fdps-aftn-channel"
	appVersion = "2020-02-20 13:45"
)

var workWithDocker bool
var dockerVersion string

func initLoggers() {
	logger.AppendLogger(log_std.LogStd)

	// логгер с web страничкой
	log_web.Initialize(log_web.LogWebSettings{
		StartHttp:  false,
		LogURLPath: utils.AftnChannelsPath,
		Title:      "FDPS AFTN CHANNEL",
	})
	//log_web.SetMinSeverity(logger.SevInfo)
	logger.AppendLogger(log_web.WebLogger)
	utils.AppendHandler(log_web.WebLogger)

	// логгер с сохранением в файлы
	var ljackSetts log_ljack.LjackSettings
	ljackSetts.GenDefault()
	ljackSetts.FilesName = appName + ".log"

	if err := log_ljack.Initialize(ljackSetts); err != nil {
		logger.PrintfErr("Ошибка инициализации логгера lumberjack. Ошибка: %s", err.Error())
	}
	logger.AppendLogger(log_ljack.LogLjack)
}

func initDockerInfo() {
	if dockerVersion, dockErr := utils.GetDockerVersion(); dockErr != nil {
		log_web.SetDockerVersion("???")
		logger.PrintfErr(dockErr.Error())
	} else {
		workWithDocker = true
		log_web.SetDockerVersion(dockerVersion)
	}
}

func main() {
	//aftn.TestPrepareUserTlgForSend()
	//aftn.TestQCatDetect()
	//return
	os.Exit(mainReturnWithCode())
}

func mainReturnWithCode() int {
	initLoggers()
	initDockerInfo()

	logger.PrintfInfo("AFTN канал запущен. Аргументы командной строки: %v", os.Args)

	// анализ аргументов командной строки
	// _, ChiefPort, ID, WebPath, WebPort
	if len(os.Args) == 5 {
		var err error
		worker.ChiefPort, err = strconv.Atoi(os.Args[1])
		if err != nil {
			logger.PrintfErr("FATAL. Невалидный номер сетевого порта для связи с конторллером.")
			return chiefchan.InvalidNetPort
		}
		worker.ChannelSetts.ID, err = strconv.Atoi(os.Args[2])
		if err != nil {
			logger.PrintfErr("FATAL. Невалидный ID AFTN канала.")
			return chiefchan.InvalidChannelID
		}
		worker.WebPath = os.Args[3]
		worker.WebPort, err = strconv.Atoi(os.Args[4])
		if err != nil {
			logger.PrintfErr("FATAL. Невалидный порт Web страницы AFTN канала.")
			return chiefchan.InvalidWebPort
		}

	} else {
		logger.PrintfErr("FATAL. Неверное кол-во аргументов командной строки. Получено: %d. Необходимо 5 (_, ChiefPort, ID, WebPath, WebPort).",
			strconv.Itoa(len(os.Args)))
		return chiefchan.InvalidParamCount
	}

	done := make(chan struct{})
	chanweb.Start(chanweb.WebServerConf{Port: uint16(worker.WebPort)}, done)

	worker.NewWorker(done).Start()

	//close(done)
	//test()

	return chanweb.WaitShutdown() // завершение работы канала
}
