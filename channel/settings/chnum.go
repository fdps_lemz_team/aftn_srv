package chansetts

import (
	"fmt"
	"time"
)

type ChNum struct {
	CurrNum  int
	LastDate time.Time
}

func (cn *ChNum) Set(nn int, dd time.Time) {
	cn.CurrNum = nn
	cn.LastDate = dd
	cn.fix()
}

func (cn *ChNum) fix() {
	if cn.CurrNum > 999 || cn.CurrNum < 0 {
		cn.CurrNum = 0
	}
}

func (cn *ChNum) Next() *ChNum {
	// Исправляем дату, если были скачки времени вперед...
	// Дата последней отправки не может быть больше текущего времени
	if cn.LastDate.After(time.Now().UTC()) {
		cn.LastDate = time.Now().UTC()
	}
	// Новые сутки
	if cn.LastDate.Truncate(24 * time.Hour).Before(time.Now().UTC().Truncate(24 * time.Hour)) {
		cn.LastDate = time.Now().UTC()
		cn.CurrNum = 1
	} else {
		cn.CurrNum++
	}
	cn.fix()
	return cn
}

func (cn ChNum) Int() int {
	return cn.CurrNum
}

func (cn ChNum) String() string {
	return fmt.Sprintf("%03d", cn.CurrNum)
}
