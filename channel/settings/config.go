package chansetts

import (
	"time"

	"github.com/satori/go.uuid"

	"fdps/utils"
	"fdps/utils/logger"
)

const configName = "./fdps-aftn-channel-config.json"

type Config struct {
	ID                int       // идентификатор канала
	Uuid              string    // уникальный ключ конфигурации
	InpName           []string  // обозначение канала на прием лат/рус
	OutName           []string  // обозначение канала на передачу лат/рус
	CcmAddress        []string  // адрес цкс лат/рус
	OwnAddress        []string  // главный адресный указатель лат/рус (свой адрес)
	CcmIpAddress      string    //IP адрес ЦКС
	CcmIpPort         int       // порт подключения к ЦКС
	InpNum            ChNum     // последний полученный канальный номер
	OutNum            ChNum     // последний отправленный канальный номер
	OutNumSkipCounter int       // количество игнорируемых сообщений о подстройке канальных номеров
	CrLf              string    // символы конца строки  (\r\n, \r\r\n)
	SplitSignal       bool      // сигнал разделения сообщений
	ChControl         bool      // контроль входящих ЦХ
	WorkTime          string    // время работы (круглосуточно, по расписанию)
	SSConfirm         bool      // подтверждение тлг категории срочности SS (автоматическое, ручное)
	QCatSvc           []string  // категория срочности служебных сообщений лат/рус
	MaxOutTlgLen      int       // максимальная длина сообщения на передачу (символы)
	MaxInpTlgLen      int       // максимальная длина сообщения на прием (символы)
	ChFormat          string    // (ЦХ, ЦХ ЛР, не передавать)
	CurrDate          time.Time // текущая дата
	PrevDateLs        int       // последний отправленный номер за предыдущие сутки
	PrevDateLr        int       // последний полученный номер за предыдущие сутки
}

//func GetConfig() Config {
//	c := Config{}
//	c.Load()
//	return c
//}

func (c *Config) Save() {
	u, _ := uuid.NewV1()
	c.Uuid = u.String()
	logger.PrintfErr("%v", utils.SaveToFile(configName, c))
}

//func (c *Config) Load() {
//	if err := utils.ReadFromFile(configName, c); err != nil {
//		logger.PrintfErr("%v", err)
//		c.SetDefault()
//		c.Save()
//	}
//}

//func (c *Config) UpdateIf() {
//	nc := Config{}
//	nc.Load()
//	if c.Uuid != nc.Uuid {
//		c.Uuid = nc.Uuid
//		c.InpName = append([]string{}, nc.InpName...)
//		c.OutName = append([]string{}, nc.OutName...)
//		c.CcmAddress = append([]string{}, nc.CcmAddress...)
//		c.OwnAddress = append([]string{}, nc.OwnAddress...)
//		c.InpNum = nc.InpNum
//		c.OutNum = nc.OutNum
//		c.OutNumSkipCounter = nc.OutNumSkipCounter
//		c.CrLf = nc.CrLf
//		c.QCatSvc = append([]string{}, nc.QCatSvc...)
//		c.MaxOutTlgLen = nc.MaxOutTlgLen
//		c.CurrDate = nc.CurrDate
//		c.PrevDateLs = nc.PrevDateLs
//		c.PrevDateLr = nc.PrevDateLr
//	}
//}

//func (c *Config) SetDefault() {
//	u, _ := uuid.NewV1()
//	c.Uuid = u.String()
//	c.CrLf = aftn.CrLfRus
//	c.QCatSvc = []string{"ФФ", "FF"}
//	c.CcmAddress = []string{"УУУУУУУУ", "UUUUUUUU"}
//	c.OwnAddress = []string{"ЛЛЛЛЛЛЛЛ", "LLLLLLLL"}
//	c.InpName = []string{"ТЗЗ", "TZZ"}
//	c.OutName = []string{"ТЗЗ", "TZZ"}
//	c.MaxOutTlgLen = 3000
//}
