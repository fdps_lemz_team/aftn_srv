package chansetts

import (
	"errors"
	"strconv"
	"time"

	"fdps/aftn/aftn-srv/channel/aftn"
)

const (
	startText string = "running"
	stopText  string = "stopped"
)

type Settings struct {
	ID                int    `json:"ChannelID"`         // идентификатор канала
	IsWorking         bool   `json:"IsWorking"`         // работоспособность канала
	InpNameLat        string `json:"InpNameLat"`        // обозначение канала на прием лат
	OutNameLat        string `json:"OutNameLat"`        // обозначение канала на передачу лат
	CcmAddressLat     string `json:"CcmAddressLat"`     // адрес цкс лат
	OwnAddressLat     string `json:"OwnAddressLat"`     // главный адресный указатель лат (свой адрес)
	CcmIpAddress      string `json:"CcmIpAddress"`      //IP адрес ЦКС
	CcmIpPort         int    `json:"CcmIpPort"`         // порт подключения к ЦКС
	FdpsWsPort        int    `json:"FdpsWsPort"`        // порт подключения fdps-aftn
	InpNum            int    `json:"InpNum"`            // последний полученный канальный номер
	OutNum            int    `json:"OutNum"`            // последний отправленный канальный номер
	OutNumSkipCounter int    `json:"OutNumSkipCounter"` // количество игнорируемых сообщений о подстройке канальных номеров
	CrLf              string `json:"CrLf"`              // символы конца строки  (\r\n, \r\r\n)
	SplitSignal       bool   `json:"SplitSignal"`       // сигнал разделения сообщений
	ChControl         bool   `json:"ChControl"`         // контроль входящих ЦХ
	WorkTime          string `json:"WorkTime"`          // время работы (круглосуточно, по расписанию)
	SSConfirm         bool   `json:"SSConfirm"`         // подтверждение тлг категории срочности SS (автоматическое, ручное)
	QCatSvcLat        string `json:"QCatSvcLat"`        // категория срочности служебных сообщений лат
	MaxOutTlgLen      int    `json:"MaxOutTlgLen"`      // максимальная длина сообщения на передачу (символы)
	MaxInpTlgLen      int    `json:"MaxInpTlgLen"`      // максимальная длина сообщения на прием (символы)
	ChFormat          string `json:"ChFormat"`          // (ЦХ, ЦХ ЛР, не передавать)
	PrevDateLs        int    `json:"PrevDateLs"`        // последний отправленный номер за предыдущие сутки
	PrevDateLr        int    `json:"PrevDateLr"`        // последний полученный номер за предыдущие сутки
	Version           string `json:"Version"`           // версия приложения
	StatInpBytesCount int    `json:"StatInpBytesCount"` // кол-во передаваемых конфигуратору Байт принятых телеграм
	StatOutBytesCount int    `json:"StatOutBytesCount"` // кол-во передаваемых конфигуратору Байт отправленных телеграм
	URLAddress        string `json:"URLAddress"`        // IP адрес для доступа к web страничке
	URLPath           string `json:"URLPath"`           // путь для доступа к web страничке
	URLPort           int    `json:"URLPort"`           // порт для доступа к web страничке
	ShowDebugMgs      bool   `json:"ShowDebugMgs"`      // показывать отладочные сообщений в журнале
}

func (s *Settings) ToConfig() Config {
	return Config{
		//Uuid:,
		ID:                s.ID,
		InpName:           []string{aftn.ToRus(s.InpNameLat), s.InpNameLat},
		OutName:           []string{aftn.ToRus(s.OutNameLat), s.OutNameLat},
		CcmAddress:        []string{aftn.ToRus(s.CcmAddressLat), s.CcmAddressLat},
		OwnAddress:        []string{aftn.ToRus(s.OwnAddressLat), s.OwnAddressLat},
		CcmIpAddress:      s.CcmIpAddress,
		CcmIpPort:         s.CcmIpPort,
		InpNum:            ChNum{CurrNum: s.InpNum, LastDate: time.Now()},
		OutNum:            ChNum{CurrNum: s.OutNum, LastDate: time.Now()},
		OutNumSkipCounter: s.OutNumSkipCounter,
		CrLf:              s.CrLf,
		SplitSignal:       s.SplitSignal,
		ChControl:         s.ChControl,
		WorkTime:          s.WorkTime,
		SSConfirm:         s.SSConfirm,
		QCatSvc:           []string{aftn.ToRus(s.QCatSvcLat), s.QCatSvcLat},
		MaxOutTlgLen:      s.MaxOutTlgLen,
		MaxInpTlgLen:      s.MaxInpTlgLen,
		ChFormat:          s.ChFormat,
		CurrDate:          time.Now(),
		PrevDateLs:        s.PrevDateLs,
		PrevDateLr:        s.PrevDateLr,
	}
}

// ToLogMessage строка для вывода в лог.
func (s *Settings) ToLogMessage() string {
	var retValue string
	retValue += "Версия: " + s.Version + " ,"
	retValue += "Идентификатор: " + strconv.Itoa(s.ID) + " ,"
	retValue += "Обозначение канала на прием: " + s.InpNameLat + " ,\n"
	retValue += "Обозначение канала на передачу: " + s.OutNameLat + " ,"
	retValue += "Адрес ЦКС: " + s.CcmAddressLat + " ,"
	retValue += "Главный адресный указатель: " + s.OwnAddressLat + " ,"
	retValue += "Сетевой адрес ЦКС: " + s.CcmIpAddress + " ,"
	retValue += "Сетевой порт ЦКС: " + strconv.Itoa(s.CcmIpPort) + " ,"
	retValue += "Сетевой порт FDPS сервиса: " + strconv.Itoa(s.FdpsWsPort) + " ,"
	retValue += "Последний полученный канальный номер: " + strconv.Itoa(s.InpNum) + " ,"
	retValue += "Последний отправленный канальный номер: " + strconv.Itoa(s.OutNum) + " ,"
	retValue += "Количество игнорируемых сообщений о подстройке канальных номеров: " + strconv.Itoa(s.OutNumSkipCounter) + " ,"
	retValue += "Символы конца строки: " + s.CrLf + " ,"
	retValue += "Cигнал разделения сообщений: "
	if s.SplitSignal {
		retValue += " да. "
	} else {
		retValue += " нет. "
	}
	retValue += "Контроль входящих ЦХ: "
	if s.ChControl {
		retValue += " да. "
	} else {
		retValue += " нет. "
	}
	retValue += "Время работы: " + s.WorkTime + " ,"

	retValue += "Автоматическое подтверждение тлг категории срочности SS "
	if s.SSConfirm {
		retValue += " да. "
	} else {
		retValue += " нет. "
	}
	retValue += "Категория срочности служебных сообщений: " + s.QCatSvcLat + " ,"
	retValue += "Максимальная длина сообщения на передачу: " + strconv.Itoa(s.MaxOutTlgLen) + " ,"
	retValue += "Максимальная длина сообщения на прием: " + strconv.Itoa(s.MaxInpTlgLen) + " ,"
	retValue += "Формат ЦХ: " + s.ChFormat + " ,"
	retValue += "Последний отправленный номер за предыдущие сутки: " + strconv.Itoa(s.PrevDateLs) + " ,"
	retValue += "Последний полученный номер за предыдущие сутки: " + strconv.Itoa(s.PrevDateLr) + " ,"
	retValue += "Кол-во передаваемых конфигуратору Байт принятых телеграм: " + strconv.Itoa(s.StatInpBytesCount) + " ,"
	retValue += "Кол-во передаваемых конфигуратору Байт отправленных телеграм: " + strconv.Itoa(s.StatOutBytesCount) + " ,"
	retValue += "IP адрес для доступа к web страничке: " + s.URLAddress + " ,"
	retValue += "Путь для доступа к web странице: " + s.URLPath + " ,"
	retValue += "Порт для доступа к web странице: " + strconv.Itoa(s.URLPort) + ""

	return retValue
}

// CheckSettings проверка настроек после unmarshall
func (s *Settings) CheckSettings() error {
	if len(s.InpNameLat) != 3 {
		return errors.New("Не верная длина обозначения канала на прием.")
	}
	if len(s.OutNameLat) != 3 {
		return errors.New("Не верная длина обозначения канала на передачу.")
	}
	if s.CcmAddressLat == "" {
		return errors.New("Не задан адрес цкс.")
	}
	if s.OwnAddressLat == "" {
		return errors.New("Не задан главный адресный указатель.")
	}
	if s.CcmIpAddress == "" {
		return errors.New("Не задан сетевой адрес ЦКС.")
	}
	if s.CcmIpPort == 0 {
		return errors.New("Не задан сетевой порт ЦКС.")
	}
	if s.FdpsWsPort == 0 {
		return errors.New("Не задан сетевой порт подключения FDPS сервиса.")
	}
	if s.WorkTime == "" {
		return errors.New("Не задано время работы канала.")
	}
	if s.QCatSvcLat == "" {
		return errors.New("Не задана категория срочности служебных сообщений.")
	}
	if s.ChFormat == "" {
		return errors.New("Не задан формат ЦХ.")
	}

	return nil
}

// SettingsWithPort настройки каналов, плюс порт для взяимодействия с каналами
type SettingsWithPort struct {
	ChSettings []Settings
	ChPort     int
}
