package tlg

import (
	"bytes"
	"errors"
	"fdps/aftn/aftn-srv/channel/mtk2"
	"fdps/utils/logger"
)

type Parser struct {
	buf       []byte
	tlg       []byte
	maxTlgLen int
}

const (
	zczcLen = 4
	nnnnLen = 4
)

func NewParser() *Parser {
	p := &Parser{}
	p.maxTlgLen = 2100
	//p.buf = make([]byte, 0, maxMessageLen)
	//p.tlg = make([]byte, 0, maxMessageLen)
	return p
}

func (p *Parser) Buf() []byte {
	return p.buf
}

// r есть сообщение
// z есть начало сообщения
// n есть конец сообщения
// e ошибки
func (p *Parser) Write(b []byte) (r bool, z bool, n bool, e error) {
	var err error

	p.buf = append(p.buf, b...)

	zIdx := bytes.Index(p.buf, mtk2.ZCZC)
	nIdx := bytes.Index(p.buf, mtk2.NNNN)

	//defer logger.PrintfDebug("r=%v z=%v n=%v e=%v len(p.buf)=%v zIdx=%v nIdx=%v", r, z, n, e, len(p.buf), zIdx, nIdx)

	// в буфере нет ни начала ни конца сообщения
	if zIdx == -1 && nIdx == -1 {
		if len(p.buf) > p.maxTlgLen*2 {
			p.buf = p.buf[len(p.buf)-p.maxTlgLen*2:]
			return false, false, false, errors.New("garbage")
		}
		return false, false, false, nil
	}

	if zIdx > -1 && nIdx > -1 && nIdx > zIdx {
		if zIdx > 0 {
			p.tlg = make([]byte, nIdx+nnnnLen+1)
			copy(p.tlg, p.buf[zIdx-1:nIdx+nnnnLen])
		} else {
			p.tlg = make([]byte, nIdx+nnnnLen)
			copy(p.tlg, p.buf[zIdx:nIdx+nnnnLen])
			err = errors.New("no reg before zczc")
		}
		p.buf = p.buf[nIdx+nnnnLen:]
		logger.PrintfDebug("r=%v z=%v n=%v e=%v len(p.buf)=%v zIdx=%v nIdx=%v", r, z, n, e, len(p.buf), zIdx, nIdx)
		return true, true, true, err
	}

	if zIdx > -1 && nIdx > -1 && nIdx < zIdx {
		p.tlg = make([]byte, 0, nIdx+nnnnLen)
		copy(p.tlg, p.buf)
		p.buf = p.buf[nIdx+nnnnLen:]
		return true, false, true, errors.New("no zczc")
	}

	if zIdx > -1 && nIdx == -1 {
		z2Idx := bytes.Index(p.buf[zIdx+zczcLen:], mtk2.ZCZC) + zIdx + zczcLen
		if z2Idx > zIdx+zczcLen-1 {
			if zIdx > 0 {
				p.tlg = make([]byte, z2Idx+1)
				copy(p.tlg, p.buf[zIdx-1:z2Idx])
			} else {
				p.tlg = make([]byte, z2Idx)
				copy(p.tlg, p.buf[zIdx:z2Idx])
			}
			p.buf = p.buf[z2Idx-1:]
			err = errors.New("no nnnn")
			return true, true, false, err
		}

		if len(p.buf)-zIdx > p.maxTlgLen+nnnnLen+1 {
			if zIdx > 0 {
				p.tlg = make([]byte, zIdx+p.maxTlgLen+1)
				copy(p.tlg, p.buf[zIdx-1:zIdx+p.maxTlgLen])
			} else {
				p.tlg = make([]byte, zIdx+p.maxTlgLen)
				copy(p.tlg, p.buf[zIdx:zIdx+p.maxTlgLen])
			}
			p.buf = p.buf[zIdx+p.maxTlgLen:]
			err = errors.New("no end max len")
			return true, true, false, err
		}
		return false, true, false, nil
	}

	if zIdx == -1 && nIdx > -1 {
		p.tlg = make([]byte, 0, nIdx+nnnnLen)
		copy(p.tlg, p.buf)
		p.buf = p.buf[nIdx+nnnnLen:]
		return true, false, true, errors.New("no zczc")
	}

	logger.PrintfErr("NO R NO Z NO N NO E NO ALBERT")
	return false, false, false, nil
}

func (p *Parser) GetTlg() (*Tlg, bool) {
	if len(p.tlg) == 0 {
		return nil, false
	}

	coder := mtk2.Mtk2{}
	tlg := &Tlg{}
	tlg.Text = coder.ToString(p.tlg)
	tlg.ParseRecv()

	return tlg, true
}
