package tlg

import (
	"fdps/aftn/aftn-srv/channel/aftn"
	"fdps/aftn/aftn-srv/channel/mtk2"
	"fdps/parser/lex"
	"fdps/str"
	"fmt"
	"github.com/pkg/errors"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

const (
	defDelimChars = " \t\r\n"
	defSpecChars  = "-()/"
	defAbc        = "ЕАСИУДРЙНФЦКТЗЛВХЫПЯОБГМЬЖEASIUDRJNFCKTZLWHYPQOBGMXV3'874ЮЭ:5+2Щ6019?Ш.=" +
		"еасиудрйнфцктзлвхыпяобгмьжeasiudrjnfcktzlwhypqobgmxv3'874юэ:5+2щ6019?ш.=_"
)

type Tlg struct {
	Id              int64     //
	Text            string    //
	ChName          string    //
	ChNum           string    //
	ChTime          string    //
	QCat            string    //
	Addr            string    //
	RdNum           string    //
	AddrFrom        string    //
	AddrFromJJJJJ   string    //
	ZCZCDetected    bool      //
	NNNNDetected    bool      //
	ChNumDetected   bool      //
	ChTimeDetected  bool      //
	QCatDetected    bool      //
	AddrDetected    bool      //
	RdNumDetected   bool      //
	AdrFromDetected bool      //
	Errors          []error   //
	ArrTime         time.Time //
	TlgType         string    //
	IsCh            bool      //
	NumLs           string    //
	NumLr           string    //
	IsExpRec        bool      //
	NumExp          string    //
	NumRec          string    //
	RdNumRpt        string    //
	AddrFromRpt     string    //
	IsSvcQtaRpt     bool      //
	NumFromRpt      string    //
	NumToRpt        string    //
	IsSvcQtaMis     bool      //
	NumFromMis      string    //
	NumToMis        string    //
	IsSvcAds        bool      //
	NumAds          string    //
	RdNumAds        string    //
	AddrFromAds     string    //
	UnknownAddr     string    //
	IsMsr           bool      //
	NumMsr          string    //
	IsOgn           bool      //
	NumOgn          string    //
	IsTxtTooLong    bool      //
	NumTxtTooLong   string    //
	IsLsExp         bool      //
	IsLrLs          bool      //
}

func (t *Tlg) From(s *Tlg) {
	t.Id = s.Id
	t.Text = s.Text
	t.ChName = s.ChName
	t.ChNum = s.ChNum
	t.ChTime = s.ChTime
	t.QCat = s.QCat
	t.Addr = s.Addr
	t.RdNum = s.RdNum
	t.AddrFrom = s.AddrFrom
	t.ZCZCDetected = s.ZCZCDetected
	t.NNNNDetected = s.NNNNDetected
	t.ChNumDetected = s.ChNumDetected
	t.ChTimeDetected = s.ChTimeDetected
	t.QCatDetected = s.QCatDetected
	t.AddrDetected = s.AddrDetected
	t.RdNumDetected = s.RdNumDetected
	t.AdrFromDetected = s.AdrFromDetected
	t.Errors = append(t.Errors, s.Errors...)
	t.ArrTime = s.ArrTime
	t.TlgType = s.TlgType
}

func (t Tlg) ErrorsString() string {
	var b strings.Builder
	for _, e := range t.Errors {
		b.WriteString("\n")
		b.WriteString(e.Error())
	}
	return b.String()
}

func (t Tlg) IsTestMessage() bool {
	if strings.Contains(t.Text, "ZCZC QJH") && strings.Contains(t.Text, "RYRY") {
		return true
	}
	return false
}

func (t Tlg) IsChMessage() bool {
	if strings.TrimSpace(t.Text) == "CH" ||
		strings.TrimSpace(t.Text) == "ЦХ" ||
		strings.HasPrefix(t.Text, "CH LR") ||
		strings.HasPrefix(t.Text, "ЦХ ЛР") {
		return true
	}
	return false
}

func (t Tlg) TextForDb() string {
	return aftn.RemoveRegs(t.Text)
}

// ошибка в адресной строке
func (t Tlg) Ogn() bool {
	return !t.AddrDetected
}

// ошибка в строке отправителя
func (t Tlg) Ads() bool {
	return !t.AdrFromDetected || !t.RdNumDetected
}

func (t *Tlg) ParseRecv() {
	if len(t.Text) < 1 {
		return
	}

	abc := strings.Join(aftn.ABC, "")
	s := lex.NewScanner("AFTN Scanner", aftn.RemoveRegs(t.Text), defDelimChars, defSpecChars, abc)
	s.AddToSpec(string(mtk2.RusRegRune))
	s.AddToSpec(string(mtk2.LatRegRune))
	s.AddToSpec(string(mtk2.NumRegRune))
	s.Debug = true

	t.TryZcZc(s)
	t.TryFirstStr(s)
	if t.TryCh(s) {
		return
	}
	t.TryAddress(s)
	t.TryRdAdrFrom(s)
	t.TrySvcMessages(s)
	t.TryNnnn(s)
}

func (t *Tlg) TryZcZc(s *lex.Scanner) bool {
	for {
		s.Next()
		if s.CurrIs("") { // все закончилось
			return false
		}
		// Начало сообщения
		for _, zczc := range aftn.ZCZC {
			if s.CurrIs(zczc) {
				t.ZCZCDetected = true
				return true
			}
		}
	} // ZcZc
}

func (t *Tlg) TryNnnn(s *lex.Scanner) bool {
	for { // Поиск NNNN // Конец сообщения
		s.Next()
		if s.CurrIs("") { // все закончилось
			return false
		}
		for _, nnnn := range aftn.NNNN {
			if s.CurrIs(nnnn) {
				t.NNNNDetected = true
				return true
			}
		}
	}
}

func (t *Tlg) TryUnknown(s *lex.Scanner) bool {
	for { // Поиск NNNN // Конец сообщения
		s.Next()
		if s.CurrIs("") { // все закончилось
			return false
		}

		if s.CurrAny("UNKNOWN", "НЕИЗВЕСТНО") {
			t.UnknownAddr = s.Next().Value
			return true
		}

		for _, nnnn := range aftn.NNNN {
			if s.CurrIs(nnnn) {
				t.NNNNDetected = true
				return false
			}
		}
	}
}

// Первая строка
func (t *Tlg) TryFirstStr(s *lex.Scanner) bool {
	for {
		if s.Next().IsNewLine {
			s.Backup()
			return true
		}
		if s.CurrIs("") { // все закончилось
			return false
		}
		// Имя канала, канальный номер
		if !t.ChNumDetected {
			if s.MaskIs("AAA999") {
				t.ChName = str.RuneSubStr(s.Curr().Value, 0, 3)
				t.ChNum = str.RuneSubStr(s.Curr().Value, 3, 6)
				t.ChNumDetected = true
			} else if s.MaskIs("AAA") {
				t.ChName = s.Curr().Value
				if s.NextIs(string(mtk2.NumRegRune)) {
					if s.NextMaskIs("999") {
						t.ChNum = s.Curr().Value
					} else {
						t.ChNumDetected = true
					}
				}
			}
		}
		// Канальное время
		if !t.ChTimeDetected && s.MaskIs("9999") {
			t.ChTime = s.Curr().Value
			t.ChTimeDetected = true
		}
	}
}

// Адресная строка
func (t *Tlg) TryAddress(s *lex.Scanner) bool {
	for {
		s.Next()
		if s.CurrIs("") { // все закончилось
			return false
		}
		if s.CurrIs(string(mtk2.NumRegRune)) {
			return true
		}
		if s.MaskIs("999999") {
			s.Backup()
			return true
		}
		// Категория срочности
		if s.MaskAny("A", "AA", "AAA") {
			t.QCat, t.QCatDetected = aftn.QCatDetect(s.Curr().Value)
			if !t.QCatDetected {
				t.AddrDetected = false
				return false
			}
		}
		// Адрес
		if s.MaskIs("AAAAAAAA") {
			t.Addr += " " + s.Curr().Value
			t.AddrDetected = true
		} else {
			if t.AddrDetected {
				t.AddrDetected = false
				return false
			}
		}
	}
}

// Номер РД и Отправитель
func (t *Tlg) TryRdAdrFrom(s *lex.Scanner) bool {
	for {
		s.Next()
		if s.CurrIs("") { // все закончилось
			return false
		}
		for _, nnnn := range aftn.NNNN {
			if s.CurrIs(nnnn) {
				t.NNNNDetected = true
				return false
			}
		}
		if s.MaskIs("999999") {
			t.RdNum = s.Curr().Value
			t.RdNumDetected = true
			if s.NextAny(string(mtk2.RusRegRune), string(mtk2.LatRegRune)) {
				s.Next()
			}
			if s.MaskIs("AAAAAAAA") {
				t.AddrFrom = s.Curr().Value
				t.AdrFromDetected = true
				return true
			} else if s.MaskIs("AAAAAAAAAAAAA") &&
				(strings.Contains(s.Curr().Value, "JJJJJ") || strings.Contains(s.Curr().Value, "ЮЮЮЮЮ")) {
				t.AddrFrom = str.RuneSubStr(s.Curr().Value, 0, 8)
				t.AddrFromJJJJJ = str.RuneSubStr(s.Curr().Value, 8, 13)
				t.AdrFromDetected = true
				return true
			} else {
				s.Backup()
				return false
			}
		}
	}
}

// Проверка служебного сообщения ЦХ
func (t *Tlg) TryCh(s *lex.Scanner) bool {
	if s.NextAny("ЦХ", "CH") {
		t.IsCh = true
		if s.NextAny("LR", "ЛР") {
			s.Next()
			if s.MaskIs("AAA999") {
				t.NumLr = str.RuneSubStr(s.Curr().Value, 3, 6)
			} else if s.MaskIs("AAA") {
				if s.NextAny(string(mtk2.LatRegRune), string(mtk2.RusRegRune), string(mtk2.NumRegRune)) {
					if s.NextMaskIs("999") {
						t.NumLr = s.Curr().Value
					}
				}
			}
		} else {
			s.Backup()
		}
		t.TryNnnn(s)
		return true
	}
	s.Backup()
	return false
}

// Проверка служебных сообщений
func (t *Tlg) TrySvcMessages(s *lex.Scanner) bool {
	if s.Next().IsNewLine {
		if s.CurrAny("SVC", "СЖЦ") {
			s.Next()
			if s.CurrAny("QTA", "ЩТА") {
				s.Next()
				if s.CurrAny("RPT", "РПТ") {
					t.IsSvcQtaRpt = true
					s.Next()
					if s.MaskIs("999999") {
						t.RdNumRpt = s.Curr().Value
						if s.NextMaskIs("AAAAAAAA") {
							t.AddrFromRpt = s.Curr().Value
						}
					} else if s.MaskIs("AAA999") {
						t.NumFromRpt = str.RuneSubStr(s.Curr().Value, 3, 6)
						if s.NextIs("-") {
							if s.NextMaskIs("999") {
								t.NumToRpt = str.RuneSubStr(s.Curr().Value, 3, 6)
							}
						} else {
							s.Backup()
						}
					} else {
						s.Backup()
					}
				} else if s.CurrAny("MIS", "МИС") {
					t.IsSvcQtaMis = true
					if s.NextMaskIs("AAA999") {
						t.NumFromMis = str.RuneSubStr(s.Curr().Value, 3, 6)
						if s.NextIs("-") {
							if s.NextMaskIs("999") {
								t.NumToMis = str.RuneSubStr(s.Curr().Value, 3, 6)
							}
						} else {
							s.Backup()
						}
					}
				} else if s.CurrAny("ADS", "АДС") {
					if s.NextMaskIs("AAA999") {
						t.NumAds = str.RuneSubStr(s.Curr().Value, 3, 6)
					}
					if s.NextAny("CORRUPT", "ИСКАЖЕНО") {
						t.IsSvcAds = true
					}
				} else if s.CurrAny("MSR", "МСР") {
					t.IsMsr = true
					if s.NextMaskIs("AAA999") {
						t.NumMsr = str.RuneSubStr(s.Curr().Value, 3, 6)
					}
				} else if s.CurrAny("OGN", "ОГН") {
					t.IsOgn = true
					if s.NextMaskIs("AAA999") {
						t.NumOgn = str.RuneSubStr(s.Curr().Value, 3, 6)
					}
				}
			} else if s.CurrAny("ADS", "АДС") {
				t.IsSvcAds = true
				s.Next()
				if s.MaskIs("999999") {
					t.RdNumAds = s.Curr().Value
					if s.NextMaskIs("AAAAAAAA") {
						t.AddrFromAds = s.Curr().Value
					}
					t.TryUnknown(s)
				} else if s.MaskIs("AAA999") {
					t.NumAds = str.RuneSubStr(s.Curr().Value, 3, 6)
				}
			} else if s.CurrAny("TXT", "ТЕКСТ") {
				if s.NextMaskIs("AAA999") {
					t.NumTxtTooLong = str.RuneSubStr(s.Curr().Value, 3, 6)
					if s.NextAny("TOO", "О4ЕНЬ") {
						if s.NextAny("LONG", "ДЛИННЫЙ") {
							t.IsTxtTooLong = true
						}
					}
				}
			} else if s.CurrAny("LR", "ПОЛУ4ЕН") {
				if s.NextMaskIs("AAA999") {
					t.NumLr = str.RuneSubStr(s.Curr().Value, 3, 6)
					s.Next()
					if s.CurrAny("EXP", "ОЖИДАЛСЯ") {
						if s.NextMaskIs("AAA999") {
							t.NumExp = str.RuneSubStr(s.Curr().Value, 3, 6)
							t.IsLsExp = true
						}
					} else if s.CurrAny("LS", "ЛС") {
						if s.NextMaskIs("AAA999") {
							t.NumLs = str.RuneSubStr(s.Curr().Value, 3, 6)
							t.IsLrLs = true
						}
					}
				}
			} else {
				s.Backup()
			}
		} else if s.CurrAny("EXP", "ОЖИДАЛСЯ") {
			if s.NextMaskIs("AAA999") {
				t.NumExp = str.RuneSubStr(s.Curr().Value, 3, 6)
				if s.NextAny("REC", "ПОЛУ4ЕН") {
					s.Next()
					if s.MaskIs("AAA999") {
						t.NumRec = str.RuneSubStr(s.Curr().Value, 3, 6)
						t.IsExpRec = true
						return true
					} else if s.MaskIs("AAAXXX") {
						t.IsExpRec = true
						return true
					}
					return false
				}
			}
		} else {
			s.Backup()
		}
	}
	return true
}

func (t *Tlg) CheckTextForSend(MaxOutLen int) bool {
	txt := aftn.RemoveRegs(t.Text)

	if len(txt) == 0 {
		t.Errors = append(t.Errors, errors.New("empty tlg text"))
		return false
	}

	if t.IsChMessage() {
		return true
	}

	var ers []error

	if utf8.RuneCountInString(txt) > MaxOutLen {
		ers = append(ers, errors.New(fmt.Sprintf("Длина телеграммы больше %d символов", MaxOutLen)))
	}

	for _, z := range aftn.ZCZC {
		if strings.Contains(txt, z) {
			ers = append(ers, errors.New(fmt.Sprintf("Текст содержит недопустимую комбинацию символов [%s]", z)))
		}
	}

	for _, n := range aftn.NNNN {
		if strings.Contains(txt, n) {
			ers = append(ers, errors.New(fmt.Sprintf("Текст содержит недопустимую комбинацию символов [%s]", n)))
		}
	}

	abc := strings.Join(aftn.ABC, "")
	s := lex.NewScanner("AFTN Out Tlg Scanner", txt, "", "", abc)
	s.AddToSpec(string(mtk2.RusRegRune))
	s.AddToSpec(string(mtk2.LatRegRune))
	s.AddToSpec(string(mtk2.NumRegRune))
	s.Debug = true

	latreg := false
	rusreg := false
	numreg := false
	rAlonce := true
	nronce := true

	rln := func(v string) {
		r, l, n := aftn.ContainRegisters(v)
		if !latreg {
			latreg = l
		}
		if !rusreg {
			rusreg = r
		}
		if !numreg {
			numreg = n
		}
		if nronce && numreg {
			ers = append(ers, errors.New(fmt.Sprintf("Адресная строка содержит символы цифрового регистра [%s]",
				s.Curr().Value)))
			nronce = false
		}
		if rAlonce && rusreg && latreg {
			ers = append(ers, errors.New(fmt.Sprintf("Адресная строка содержит одновременно символы рус. и лат. регистра [%s]",
				s.Curr().Value)))
			rAlonce = false
		}
	}

	if !s.NextMaskIs("AA") {
		ers = append(ers, errors.New(fmt.Sprintf("Категория срочности не найдена [%s]",
			s.Curr().Value)))
	}
	rln(s.Curr().Value)

	if _, ok := aftn.QCatDetect(s.Curr().Value); !ok {
		ers = append(ers, errors.New(fmt.Sprintf("Ошибочное значение категории срочности [%s]",
			s.Curr().Value)))
	}

	for {
		s.Next()

		if s.CurrIs("") {
			// все закончилось
			ers = append(ers, errors.New(fmt.Sprintf("Строка отправителя не найдена [%s]",
				s.Curr().Value)))
			break
		}

		if s.MaskIs("999999") {
			s.Next()
			if s.MaskIs("AAAAAAAA") {
				rln(s.Curr().Value)
			} else if s.MaskIs("AAAAAAAAAAAAA") &&
				(strings.HasSuffix(s.Curr().Value, "JJJJJ") || strings.HasSuffix(s.Curr().Value, "ЮЮЮЮЮ")) {
				rln(s.Curr().Value)
			} else {
				ers = append(ers, errors.New(fmt.Sprintf("Адрес отправителя не найден [%s]",
					s.Curr().Value)))
			}
			break
		}

		if s.MaskIs("AA") {
			if _, ok := aftn.QCatDetect(s.Curr().Value); !ok {
				ers = append(ers, errors.New(fmt.Sprintf("Ошибочное значение категории срочности [%s]",
					s.Curr().Value)))
			}
		} else if s.MaskIs("AAAAAAAA") {
		} else {
			ers = append(ers, errors.New(fmt.Sprintf("Ошибочный элемент адресной строки [%s]",
				s.Curr().Value)))
		}

		rln(s.Curr().Value)
	}

	t.Errors = append(t.Errors, ers...)

	return len(ers) == 0
}

func (t Tlg) ChNumInt() (int, error) {
	if len(t.ChNum) != 3 {
		return 0, errors.New("empty chnum")
	}
	return strconv.Atoi(t.ChNum)
}
