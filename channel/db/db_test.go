package db

import (
	"bytes"
	"fmt"
	"testing"

	"fdps/aftn/aftn-srv/channel/aftn"
	"fdps/aftn/aftn-srv/channel/tlg"

	//. "bitbucket.org/splice/blog/example"

)

func TestDb(tst *testing.T) {
	if dbErr := InitDb("./test-fdps-aftn.db"); dbErr != nil {
		tst.Errorf("%v", dbErr)
		tst.Fail()
	}
}

func createTestTlg(num int) *tlg.Tlg {
	var b bytes.Buffer

	regIdx := 0
	tlgIdx := 123

	// заголовок телеграфного сообщения
	b.WriteString(string(aftn.REG[regIdx]))
	b.WriteString(aftn.ZCZC[regIdx])
	b.WriteString(" ")
	b.WriteString("ЗТЗ")
	tlgNum := fmt.Sprintf("%03d", num)
	b.WriteString(tlgNum)
	b.WriteString(" ")
	b.WriteString(aftn.TimeString())
	b.WriteString("     ")
	b.WriteString(string(aftn.REG[regIdx]))
	b.WriteString("\r\r\n")
	b.WriteString(fmt.Sprintf("ALBERT MOLODEC %d", tlgIdx))
	b.WriteString("\r\r\n")
	b.WriteString(aftn.NNNN[regIdx])
	b.WriteString(aftn.MessageDelimiter)

	//tlgChan <- mtk2.Mtk2{}.FromString(b.String())

	retValue := tlg.Tlg{
		Text: b.String(),
	}

	return &retValue
}

func TestDbTlg(tst *testing.T) {

	for nn := 0; nn < 100; nn++ {
		curTlg := createTestTlg(nn)

		// кладем в БД новую тлг от fdps
		testTlg:=`FF UUUULEMZ
311548 ALBERTRP
CHIP AND
DALY DOLB
`
		insId := FdpsTlgAppend(curTlg, fmt.Sprintf("%d", nn+10))
		// lam to fdps
		FdpsTlgLamToFdps(insId)

		if tlgText, dbId, err := FdpsTlgGetOne(); err != nil {
			tst.Errorf("Ошибка получения телеграммы из БД: %v", err)
			tst.Fail()
		} else {
			if len(tlgText) > 0 {
				FdpsTlgSendToCcm(fmt.Sprintf("FULL TEXT %s", tlgText), dbId)
			}
		}
	}
}
