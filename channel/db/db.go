package db

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/mattn/go-sqlite3"

	"fdps/utils/logger"
)

const (
	TlgTimeLifeForSend = -25 * time.Hour
	InvalidInsertId    = -1
)

// TlgStorage локальное хранилище телеграмм в виде БД sqlite
type TlgStorage struct {
	sqliteDb  *sql.DB
	lastDbErr error
	isOpen    bool
}

var tlgStor TlgStorage

// InitDb инициализация структуры БД
func InitDb(dbPath string) error {
	creReceiveTable := `create table if not exists receive_tlg (
		id integer NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE
		,fdps_id varchar(1024)
		,receive_from_ccm_time datetime NOT NULL
		,txt varchar(8192) NULL
		,lam_to_ccm datetime DEFAULT ""
		,send_to_fdps_time datetime DEFAULT ""
		,lam_from_fdps datetime DEFAULT ""		
		);`

	creSendTable := `create table if not exists send_tlg (
		id integer NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE
		,fdps_id varchar(1024) NOT NULL
		,receive_from_fdps_time datetime NOT NULL
		,txt_orig varchar(8192) NOT NULL
		,txt_full varchar(8192) NULL
		,send_to_ccm_time datetime DEFAULT ""
		,lam_from_ccm_time datetime DEFAULT ""
		,lam_to_fdps_time datetime DEFAULT ""
		,send_to_fdps_time datetime DEFAULT ""
		,lam_from_fdps_time datetime DEFAULT ""
		);`

	receiveFdpsIdTrg := `create trigger if not exists receive_fdps_id_trg
		after insert on receive_tlg
		begin
		update receive_tlg set fdps_id=cast(new.id as text) where id=new.id;
		end;`

	if tlgStor.sqliteDb, tlgStor.lastDbErr = sql.Open("sqlite3", dbPath); tlgStor.lastDbErr != nil {
		tlgStor.isOpen = false
		return fmt.Errorf("Ошибка открытия БД для хранения телеграмм: %v.", tlgStor.lastDbErr)
	}

	tlgStor.isOpen = true

	if _, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec(creReceiveTable); tlgStor.lastDbErr != nil {
		return fmt.Errorf("Ошибка инициализации БД для хранения телеграмм: %v.", tlgStor.lastDbErr)
	}

	if _, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec(creSendTable); tlgStor.lastDbErr != nil {
		return fmt.Errorf("Ошибка инициализации БД для хранения телеграмм: %v.", tlgStor.lastDbErr)
	}

	if _, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec(receiveFdpsIdTrg); tlgStor.lastDbErr != nil {
		return fmt.Errorf("Ошибка создания триггера в БД для тадлицы телеграмм от ЦКС: %v.", tlgStor.lastDbErr)
	}

	return nil
}

// FdpsTlgAppend получена телеграма от FDPS сервиса
func FdpsTlgAppend(tlgText string, fdpsId string) (insId int64) {
	insId = InvalidInsertId
	if tlgStor.isOpen {
		var sqlRes sql.Result
		if sqlRes, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec("insert into send_tlg (fdps_id, txt_orig, receive_from_fdps_time) values($1, $2, datetime('now'))",
			fdpsId, tlgText); tlgStor.lastDbErr != nil {
			logger.PrintfErr("Ошибка выполнения запроса. Ошибка: %v.", tlgStor.lastDbErr)
		} else {
			insId, _ = sqlRes.LastInsertId()
		}
	}
	return insId
}

// FdpsTlgLamToFdps отправлено подтверждение получения телеграммы FDPS сервису
func FdpsTlgLamToFdps(dbId int64) {
	if tlgStor.isOpen {
		if _, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec("update send_tlg set lam_to_fdps_time = datetime('now') where id = $1;",
			dbId); tlgStor.lastDbErr != nil {
			logger.PrintfErr("Ошибка выполнения запроса. Ошибка: %v.", tlgStor.lastDbErr)
		}
	}
}

// FdpsTlgSendToCcm отправлена телеграмма в ЦКС (с измененным текстом)
func FdpsTlgSendToCcm(tlgFullText string, dbId int64) {
	if tlgStor.isOpen {
		if _, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec("update send_tlg set send_to_ccm_time = datetime('now'), txt_full = $1 where id = $2;",
			tlgFullText, dbId); tlgStor.lastDbErr != nil {
			logger.PrintfErr("Ошибка выполнения запроса. Ошибка: %v.", tlgStor.lastDbErr)
		}
	}
}

// FdpsTlgLamFromCcm получено подтверждение получения телеграммы от ЦКС
func FdpsTlgLamFromCcm(dbId int64) {
	if tlgStor.isOpen {
		if _, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec("update send_tlg set lam_from_ccm_time = datetime('now') where id = $1;",
			dbId); tlgStor.lastDbErr != nil {
			logger.PrintfErr("Ошибка выполнения запроса: %v.", tlgStor.lastDbErr)
		}
	}
}

// FdpsTlgGetOneForCcm предоставить телеграмму, полученную от FDPS сервиса для отправки в ЦКС
func FdpsTlgGetOneForCcm() (tlgText string, dbId int64, err error) {
	var retTlgText string

	if tlgStor.isOpen {
		var rows *sql.Rows
		rows, tlgStor.lastDbErr = tlgStor.sqliteDb.Query("select id, txt_orig from send_tlg where send_to_ccm_time='' order by id asc limit 1;")
		defer rows.Close()

		if tlgStor.lastDbErr != nil {
			return retTlgText, dbId, fmt.Errorf("Ошибка выполнения запроса: %v.", tlgStor.lastDbErr)
		} else {
			if rows.Next() {
				if tlgStor.lastDbErr = rows.Scan(&dbId, &retTlgText); tlgStor.lastDbErr != nil {
					return retTlgText, dbId, tlgStor.lastDbErr
				} else {
					return retTlgText, dbId, nil
				}
			} else {
				return retTlgText, dbId, nil
			}
		}
	}
	return retTlgText, dbId, fmt.Errorf("БД не работоспособна.")
}

// FdpsTlgSendToFdps отправлена телеграмма в FDPS (с измененным текстом)
func FdpsTlgSendToFdps(tlgFdpsId string) {
	if tlgStor.isOpen {
		if _, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec("update send_tlg set send_to_fdps_time = datetime('now') where fdps_id = $1;",
			tlgFdpsId); tlgStor.lastDbErr != nil {
			logger.PrintfErr("Ошибка выполнения запроса. Ошибка: %v.", tlgStor.lastDbErr)
		}
	}
}

// FdpsTlgLamFromFdps получено подтверждение получения телеграммы от FDPS
func FdpsTlgLamFromFdps(tlgFdpsId string) {
	if tlgStor.isOpen {
		if _, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec("update send_tlg set lam_from_fdps_time = datetime('now') where fdps_id = $1;",
			tlgFdpsId); tlgStor.lastDbErr != nil {
			logger.PrintfErr("Ошибка выполнения запроса: %v.", tlgStor.lastDbErr)
		}
	}
}

// FdpsTlgGetOneForFdps предоставить телеграмму для отправки в FDPS (после изменения текста)
func FdpsTlgGetOneForFdps() (tlgText string, tlgFdpsId string, err error) {
	var retTlgText string

	if tlgStor.isOpen {
		var rows *sql.Rows
		rows, tlgStor.lastDbErr = tlgStor.sqliteDb.Query("select fdps_id, txt_full from send_tlg where send_to_fdps_time='' and send_to_ccm_time<>'' order by id asc limit 1;")

		if rows != nil {
			defer rows.Close()

			if tlgStor.lastDbErr != nil {
				return retTlgText, tlgFdpsId, fmt.Errorf("Ошибка выполнения запроса: %v.", tlgStor.lastDbErr)
			} else {
				if rows.Next() {
					if tlgStor.lastDbErr = rows.Scan(&tlgFdpsId, &retTlgText); tlgStor.lastDbErr != nil {
						return retTlgText, tlgFdpsId, tlgStor.lastDbErr
					} else {
						return retTlgText, tlgFdpsId, nil
					}
				} else {
					return retTlgText, tlgFdpsId, nil
				}
			}
		}
	}
	return retTlgText, tlgFdpsId, fmt.Errorf("БД не работоспособна.")
}

// FdpsTlgDeleteOld удалить телеграммы на отправку старше dayNum дней
func FdpsTlgDeleteOld(dayNum int) {
	if tlgStor.isOpen {
		bottomTime := time.Now().AddDate(0, 0, -dayNum).Format("2006-01-02 15:04:05")
		logger.PrintfErr("bottomTime %v", bottomTime)

		if _, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec("delete from send_tlg where receive_from_fdps_time < strftime('%Y-%m-%d %H:%M:%S', $1 );",
			bottomTime); tlgStor.lastDbErr != nil {
			logger.PrintfErr("Ошибка выполнения запроса: %v.", tlgStor.lastDbErr)
		}
	}
}

// CcmTlgAppend получена телеграмма от ЦКС
func CcmTlgAppend(tlgText string) (insId int64) {
	insId = InvalidInsertId
	if tlgStor.isOpen {
		var sqlRes sql.Result
		if sqlRes, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec("insert into receive_tlg (txt, receive_from_ccm_time) values( $1, datetime('now') );",
			tlgText); tlgStor.lastDbErr != nil {
			logger.PrintfErr("Ошибка выполнения запроса: %v.", tlgStor.lastDbErr)
		} else {
			insId, _ = sqlRes.LastInsertId()
		}
	}
	return insId
}

// CcmTlgLamToCcm отправлено подтверждение получения телеграммы В ЦКС
func CcmTlgLamToCcm(dbId int64) {
	if tlgStor.isOpen {
		if _, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec("update receive_tlg set lam_to_ccm = datetime('now') where id = $1;",
			dbId); tlgStor.lastDbErr != nil {
			logger.PrintfErr("Ошибка выполнения запроса: %v.", tlgStor.lastDbErr)
		}
	}
}

// CcmTlgSendToFdps отправлена телеграмма сервису FDPS
func CcmTlgSendToFdps(fdpsId string) {
	if tlgStor.isOpen {
		if _, tlgStor.lastDbErr = tlgStor.sqliteDb.Exec("update receive_tlg set send_to_fdps_time = datetime('now') where fdps_id = $1;",
			fdpsId); tlgStor.lastDbErr != nil {
			logger.PrintfErr("Ошибка выполнения запроса: %v.", tlgStor.lastDbErr)
		}
	}
}

// CcmTlgLamFromFdps получено подтверждение получения телеграммы от сервиса FDPS
func CcmTlgLamFromFdps(fdpsId string) {

}

// CcmTlgGetOneForFdps предоставить полученную от ЦКС телеграмму для отправки в FDPS
func CcmTlgGetOneForFdps() (tlgText string, tlgFdpsId string, err error) {
	var retTlgText string

	if tlgStor.isOpen {
		var rows *sql.Rows
		rows, tlgStor.lastDbErr = tlgStor.sqliteDb.Query("select fdps_id, txt from receive_tlg where send_to_fdps_time='' and lam_to_ccm<>'' order by id asc limit 1;")

		if rows != nil {
			defer rows.Close()

			if tlgStor.lastDbErr != nil {
				return retTlgText, tlgFdpsId, fmt.Errorf("Ошибка выполнения запроса: %v.", tlgStor.lastDbErr)
			} else {
				if rows.Next() {
					if tlgStor.lastDbErr = rows.Scan(&tlgFdpsId, &retTlgText); tlgStor.lastDbErr != nil {
						return retTlgText, tlgFdpsId, tlgStor.lastDbErr
					} else {
						return retTlgText, tlgFdpsId, nil
					}
				} else {
					return retTlgText, tlgFdpsId, nil
				}
			}
		}
	}
	return retTlgText, tlgFdpsId, fmt.Errorf("БД не работоспособна.")
}
