package proto

import (
	"errors"
	"fdps/aftn/aftn-srv/channel/aftn"
	"fdps/aftn/aftn-srv/channel/mtk2"
	"fdps/parser/lex"
	"fmt"
	"log"
	"strings"
	"unicode/utf8"
)

var (
	testUserTlg = []string{
		`FF UUUUUUUU ASDFGHJK
FF ASDFGHJK 
271842 QWERTYUI
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSAGE STR2

`,
		`SS UUUUUUUU ASDFGHJK
271842 QWERTYUIJJJJJ
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSAGE STR2

`,
		`SS UUUUUUUU ASDFGHJK
271842 QWERTYUIJJJJJ
TEST MESSNNNNAGE STR1
TEST MEZCZCSSAGE STR3
TE,,,,ST MESS+:+:AGE STR2

`,
		`ФФ ФЫВАПРОЛ АВПЮВПВП
271842 КККККККК
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSФGE STR2

`,
		`ФФ ФЫВАПРОЛ УУУУУУУУ
271842 ККККККК
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSФGE STR2

`,
		`ФФ ФЫВАПРОЛ УУУУУУУУ
271842 ККККККЮК
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSФGE STR2

`, `FF ФЫВАПРОЛ УУУУУУУУ
271842 КККККККК
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSФGE STR2

`,
	}
)

func TestPrepareUserTlgForSend() {
	for n, txt := range testUserTlg {
		if n != 0 {
			break
		}
		m, e := prepareUserTlgForSend(txt, 2000, "")
		log.Println("n =>", n)
		log.Println("message =>", m)
		log.Println("errors =>", e)
	}
	log.Println(aftn.MessageDelimiter)
}

var Debug = false

func prepareUserTlgForSend(txtOrig string, maxLen int, CrLf string) (string, []error) {
	var (
		b   strings.Builder
		ers []error
	)

	txt := aftn.RemoveRegs(txtOrig)

	if len(txt) == 0 {
		ers = append(ers, errors.New("empty tlg text"))
		return txtOrig, ers
	}

	if utf8.RuneCountInString(txt) > maxLen {
		ers = append(ers, errors.New(fmt.Sprintf("Длина телеграммы больше %d символов", maxLen)))
	}

	for _, z := range aftn.ZCZC {
		if strings.Contains(txt, z) {
			ers = append(ers, errors.New(fmt.Sprintf("Текст содержит недопустимую комбинацию символов [%s]", z)))
		}
	}

	for _, n := range aftn.NNNN {
		if strings.Contains(txt, n) {
			ers = append(ers, errors.New(fmt.Sprintf("Текст содержит недопустимую комбинацию символов [%s]", n)))
		}
	}

	if len(ers) > 0 {
		return txtOrig, ers
	}

	abc := strings.Join(aftn.ABC, "")
	s := lex.NewScanner("AFTN Out Tlg Scanner", txt, "", "", abc)
	s.AddToSpec(mtk2.RusRegStr)
	s.AddToSpec(mtk2.LatRegStr)
	s.AddToSpec(mtk2.NumRegStr)
	s.Debug = Debug

	latreg := false
	rusreg := false
	numreg := false
	rAlonce := true
	nronce := true

	rln := func(v string) {
		r, l, n := aftn.ContainRegisters(v)
		if !latreg {
			latreg = l
		}
		if !rusreg {
			rusreg = r
		}
		if !numreg {
			numreg = n
		}
		if nronce && numreg {
			ers = append(ers, errors.New(fmt.Sprintf("Адресная строка содержит символы цифрового регистра [%s]",
				s.Curr().Value)))
			nronce = false
		}
		if rAlonce && rusreg && latreg {
			ers = append(ers, errors.New(fmt.Sprintf("Адресная строка содержит одновременно символы рус. и лат. регистра [%s]",
				s.Curr().Value)))
			rAlonce = false
		}
	}

	if !s.NextMaskIs("AA") {
		ers = append(ers, errors.New(fmt.Sprintf("Категория срочности не найдена [%s]",
			s.Curr().Value)))
	}
	rln(s.Curr().Value)

	if _, ok := aftn.QCatDetect(s.Curr().Value); !ok {
		ers = append(ers, errors.New(fmt.Sprintf("Ошибочное значение категории срочности [%s]",
			s.Curr().Value)))
	}

	if len(ers) > 0 {
		return txtOrig, ers
	}

	// категория срочности
	b.WriteString(s.Curr().Value)

	// проверка адресной строки и поиск строки отправителя
	for {
		s.Next()
		if s.CurrIs("") {
			ers = append(ers, errors.New(fmt.Sprintf("Строка отправителя не найдена [%s]",
				s.Curr().Value)))
			return txtOrig, ers
		}

		if s.MaskIs("999999") && s.Delim().IsNewLine {
			b.WriteString(s.Delim().Value)
			b.WriteString(s.Curr().Value)
			s.Next()
			if s.MaskIs("AAAAAAAA") {
				rln(s.Curr().Value)
				b.WriteString(aftn.RegForText(s.Curr().Value))
				b.WriteString(" ")
				b.WriteString(s.Curr().Value)
			} else if s.MaskIs("AAAAAAAAAAAAA") {
				ok := false
				for _, jjjjj := range []string{"JJJJJ", "ЮЮЮЮЮ"} {
					if i := strings.Index(s.Curr().Value, jjjjj); i > -1 {
						b.WriteString(aftn.RegForText(s.Curr().Value[:i]))
						b.WriteString(" ")
						b.WriteString(s.Curr().Value[:i])
						b.WriteString(string(aftn.NUM))
						b.WriteString(jjjjj)
						b.WriteString(aftn.RegForText(s.Curr().Value[:i]))
						ok = true
						rln(s.Curr().Value[:i])
						break
					}
				}
				if !ok {
					ers = append(ers, errors.New(fmt.Sprintf("Адрес отправителя не найден [%s]",
						s.Curr().Value)))
				}
			} else {
				ers = append(ers, errors.New(fmt.Sprintf("Ошибка в адресе отправителя [%s] длина [%d]",
					s.Curr().Value, utf8.RuneCountInString(s.Curr().Value))))
			}

			if len(ers) > 0 {
				return txtOrig, ers
			}
			break
		}

		if s.MaskIs("AA") {
			if _, ok := aftn.QCatDetect(s.Curr().Value); !ok {
				ers = append(ers, errors.New(fmt.Sprintf("Ошибочное значение категории срочности [%s]",
					s.Curr().Value)))
			}
		} else if s.MaskIs("AAAAAAAA") {
		} else {
			ers = append(ers, errors.New(fmt.Sprintf("Ошибочный элемент адресной строки [%s]",
				s.Curr().Value)))
		}
		rln(s.Curr().Value)
		if len(ers) > 0 {
			return txtOrig, ers
		}
		b.WriteString(s.Delim().Value)
		b.WriteString(s.Curr().Value)
	}

	// текст сообщения
	for {
		s.Next()
		if s.CurrIs("") {
			break
		}
		b.WriteString(s.Delim().Value)
		b.WriteString(s.Curr().Value)
	}

	if CrLf != "\r\r\n" && CrLf != "\r\n" {
		CrLf = "\r\r\n"
	}

	return aftn.ChangeCrLf(b.String(), CrLf), nil
}
