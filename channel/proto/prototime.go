package proto

import "time"

const rdNumFormat = "021504"
const channTimeFormat = "1504"

func genRdNumString() string {
	return time.Now().UTC().Format(rdNumFormat)
}

func genChanTimeString() string {
	return time.Now().UTC().Format(channTimeFormat)
}
