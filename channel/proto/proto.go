package proto

import (
	"fdps/aftn/aftn-srv/channel/aftn"
	"fdps/aftn/aftn-srv/channel/mtk2"
	"fdps/aftn/aftn-srv/channel/tlg"
	"fmt"
	"strings"
	"time"
)

type Proto struct {
	InpName           []string  // обозначение канала на прием рус/лат
	OutName           []string  // обозначение канала на передачу рус/лат
	CcmAddress        []string  // адрес цкс рус/лат
	OwnAddress        []string  // главный адресный указатель рус/лат (свой адрес)
	InpNum            ChNum     // последний полученный канальный номер
	OutNum            ChNum     // последний отправленный канальный номер
	OutNumSkipCounter int       // количество игнорируемых сообщений о подстройке канальных номеров
	CrLf              string    // символы конца строки  (\r\n, \r\r\n)
	SplitSignal       bool      // сигнал разделения сообщений
	ChControl         bool      // контроль входящих ЦХ
	WorkTime          string    // время работы (круглосуточно, по расписанию)
	QCatSvc           []string  // категория срочности служебных сообщений рус/лат
	MaxOutTlgLen      int       // максимальная длина сообщения на передачу (символы)
	MaxInpTlgLen      int       // максимальная длина сообщения на прием (символы)
	ChFormat          string    // (ЦХ, ЦХ ЛР, не передавать)
	CurrDate          time.Time // текущая дата
	PrevDateLs        int       // последний отправленный номер за предыдущие сутки
	PrevDateLr        int       // последний полученный номер за предыдущие сутки
}

//func (c *Channel) Start() {
//	if !c.conn.IsOpen() {
//		if err := c.conn.Open(mtk2.Mtk2{}.FromString(c.MakeStartReceiving())); err != nil {
//			logger.PrintfErr("%v", err)
//		}
//	}
//}

//func (c *Channel) SendCH() {
//	ch := &tlg.Tlg{}
//	ch.Text = c.MakeCH()
//	st, _ := c.Send(ch)
//	logger.PrintfDebug(st.Text)
//}
//
//func (c *Channel) CheckCH() {
//
//}
//
//func (c *Channel) CheckParams() {
//
//}
//
//func (c *Channel) Recv() (*tlg.Tlg, bool) {
//	b := make([]byte, 4096)
//
//	cnt := 0//c.conn.Read(b)
//
//	r, z, n, e := c.parser.Write(b[:cnt])
//	if !r {
//		logger.PrintfDebug("r=%v z=%v n=%v e=%v", r, z, n, e)
//		return nil, false
//	}
//	logger.PrintfDebug("r=%v z=%v n=%v e=%v", r, z, n, e)
//
//	//_ = c.conn.Lam()
//
//	rt, ok := c.parser.GetTlg()
//
//	if rt.IsTestMessage() {
//		return rt, ok
//	}
//
//	c.ProcLrLs(rt)
//	nn, _ := strconv.Atoi(rt.ChNum)
//	c.Cfg.InpNum.Set(nn, time.Now().UTC())
//
//	/*
//       CheckEndOfMsg;
//       // Отработка сообщения "ожидался получен"
//       CheckLrExp;
//       CheckSS;
//       if Params^.ConnectionStatus = stOPEN   then
//       begin
//         CheckTlgLen; // Проверяем длину сообщения только после установки канального номера
//         CheckFormat; // Проверка формата адресной строки и строки отправителя
//       end;
//	 */
//	return rt, ok
//}

//func (c *Channel) ProcLrLs(t *tlg.Tlg) {
//	if !t.IsLrLs {
//		return
//	}
//	cond := false
//	for _, a := range c.Cfg.CcmAddress {
//		if t.AddrFrom == a {
//			cond = true
//			break
//		}
//	}
//	if !cond {
//		return
//	}
//
//	if time.Now().UTC().Hour() == 0 && time.Now().UTC().Minute() < 10 {
//		lrls := &tlg.Tlg{}
//		lrls.Text = c.MakeLrLs(c.Cfg.PrevDateLr, c.Cfg.PrevDateLs, aftn.RegIdxFromText(t.Text))
//		_, _ = c.Send(lrls)
//	} else {
//	}
//}

// Сформироватть ТЛГ для отправки
func (p *Proto) PrepareTlgForSend(text string) (mktStr []byte, t *tlg.Tlg) {
	rt := &tlg.Tlg{Text: text}

	if !rt.CheckTextForSend(p.MaxOutTlgLen) {
		return []byte{}, rt
	}

	regIdx := aftn.RegIdxFromText(rt.Text[0:3])

	var b strings.Builder

	// заголовок телеграфного сообщения
	b.WriteString(string(aftn.REG[regIdx]))
	b.WriteString(aftn.ZCZC[regIdx])
	b.WriteString(" ")
	b.WriteString(p.OutName[regIdx])
	nn := p.OutNum.Next().String()
	b.WriteString(nn)
	b.WriteString(" ")
	b.WriteString(genChanTimeString())
	b.WriteString("     ")
	b.WriteString(string(aftn.REG[regIdx]))
	b.WriteString(p.CrLf)
	b.WriteString(aftn.ChangeCrLf(rt.Text, p.CrLf))
	if !rt.IsChMessage() {
		b.WriteString(string(aftn.REG[regIdx]))
	}
	b.WriteString(p.CrLf)
	if !rt.IsChMessage() {
		b.WriteString(aftn.TelegramDelimiter)
	}
	b.WriteString(aftn.NNNN[regIdx])
	b.WriteString(aftn.MessageDelimiter)

	return mtk2.Mtk2{}.FromString(b.String()), rt
}

func (p *Proto) SaveChNumsForPrevDay() {
	if p.CurrDate.After(time.Now().UTC()) {
		p.CurrDate = time.Now().UTC()
		return
	}
	// Новые сутки
	if p.CurrDate.UTC().Truncate(24 * time.Hour).Before(time.Now().UTC().Truncate(24 * time.Hour)) {
		p.CurrDate = time.Now().UTC()
		p.PrevDateLr = p.InpNum.Int()
		p.PrevDateLs = p.OutNum.Int()
	}
}

func (p Proto) MakeLrLs(lr, ls int, idx byte) string {
	var b strings.Builder

	text1 := []string{"СЖЦ ЛР ", "SVC LR "}
	text2 := []string{" ЛС ", " LS "}

	b.WriteString(p.QCatSvc[idx])
	b.WriteString(" ")
	b.WriteString(p.CcmAddress[idx])
	b.WriteString(p.CrLf)
	b.WriteString(genRdNumString())
	b.WriteString(string(aftn.REG[idx]))
	b.WriteString(" ")
	b.WriteString(p.OwnAddress[idx])
	b.WriteString(p.CrLf)
	b.WriteString(text1[idx])
	b.WriteString(p.InpName[idx])
	b.WriteString(fmt.Sprintf("%03d", lr))
	b.WriteString(text2[idx])
	b.WriteString(fmt.Sprintf("%03d", ls))

	return b.String()
}

func (p Proto) MakeCH() string {
	var b strings.Builder

	b.WriteString("CH")
	b.WriteString(p.CrLf)

	return b.String()
}

// Запрос на повтор отправителю  [rdnum  sender]
func (p Proto) MakeSvcRpt(sender, rdnum string) string {
	var b strings.Builder

	text := []string{"СЖЦ ЩТА РПТ ", "SVC QTA RPT "}

	reg := 0
	if !aftn.IsRusText(sender) {
		reg = 1
	}

	b.WriteString(p.QCatSvc[reg])
	b.WriteString(" ")
	b.WriteString(sender)
	b.WriteString(p.CrLf)
	b.WriteString(genRdNumString())
	b.WriteString(string(aftn.REG[reg]))
	b.WriteString(" ")
	b.WriteString(p.OwnAddress[reg])
	b.WriteString(p.CrLf)
	b.WriteString(text[reg])
	b.WriteString(rdnum)
	b.WriteString(string(aftn.REG[reg]))
	b.WriteString(" ")
	b.WriteString(sender)

	return b.String()
}

// Ошибка в адресной строке
// Если есть РД и Отправитель, то дополнительно посылается RPT !!!!!!!!
func (p Proto) MakeAds(sender, chnum string) string {
	text1 := []string{"СЖЦ ЩТА АДС ", "SVC QTA ADS "}
	text2 := []string{" ИСКАЖЕНО", " CORRUPT"}

	return p.MakeMessage2(sender, chnum, text1, text2)
}

// Ошибка в строке отправителя
// Если номер не определили то шлем в сообщении ожидаемый
func (p Proto) MakeOgn(sender, chnum string) string {
	text1 := []string{"СЖЦ ЩТА ОГН ", "SVC QTA OGN "}
	text2 := []string{" ИСКАЖЕНО", " CORRUPT"}

	return p.MakeMessage2(sender, chnum, text1, text2)
}

func (p Proto) MakeNoEndOfMessage(sender, chnum string) string {
	text1 := []string{"СЖЦ ПОВТОРИТЕ ", "SVC "}
	text2 := []string{" НЕТ КОНЦА СООБЩЕНИЯ", " RPT NO END OF MESSAGE"}

	return p.MakeMessage2(sender, chnum, text1, text2)
}

func (p Proto) MakeTooLongMessage(sender, chnum string) string {
	text1 := []string{"СЖЦ ТЕКСТ ", "SVC TXT "}
	text2 := []string{" ОЧЕНЬ ДЛИННЫЙ", " TOO LONG"}

	return p.MakeMessage2(sender, chnum, text1, text2)
}

func (p Proto) MakeMessage2(sender, chnum string, text1, text2 []string) string {
	var b strings.Builder

	if len(text1) == 0 || len(text2) == 0 {
		return ""
	}

	reg := 0
	if !aftn.IsRusText(sender) {
		reg = 1
	}

	b.WriteString(p.QCatSvc[reg])
	b.WriteString(" ")
	b.WriteString(p.CcmAddress[reg])
	b.WriteString(p.CrLf)
	b.WriteString(genRdNumString())
	b.WriteString(string(aftn.REG[reg]))
	b.WriteString(" ")
	b.WriteString(p.OwnAddress[reg])
	b.WriteString(p.CrLf)
	b.WriteString(text1[reg])
	b.WriteString(p.InpName[reg])
	b.WriteString(chnum)
	b.WriteString(string(aftn.REG[reg]))
	b.WriteString(text2[reg])

	return b.String()
}

func (p Proto) MakeTestMessage() string {
	var b strings.Builder

	b.WriteString("ZCZC QJH")
	b.WriteString(p.CrLf)
	b.WriteString(p.OwnAddress[aftn.IdxLat])
	b.WriteString(p.CrLf)
	b.WriteString("RYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRY")
	b.WriteString(p.CrLf)
	b.WriteString("RYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRY")
	b.WriteString(p.CrLf)
	b.WriteString("RYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRY")
	b.WriteString(p.CrLf)
	b.WriteString("NNNN")

	return b.String()
}

// MakeStartReceiving сообщение прекращения сессии (Mtk2)
func (p Proto) MakeStopReceiving() []byte {
	var b strings.Builder

	b.WriteString(p.QCatSvc[aftn.IdxRus])
	b.WriteString(" ")
	b.WriteString(p.CcmAddress[aftn.IdxRus])
	b.WriteString(p.CrLf)
	b.WriteString(genRdNumString())
	b.WriteString(string(aftn.RUS))
	b.WriteString(" ")
	b.WriteString(p.OwnAddress[aftn.IdxRus])
	b.WriteString(p.CrLf)
	b.WriteString("СЖЦ КОНЕЦ ПРИЕМА")

	return mtk2.Mtk2{}.FromString(b.String())
}

// MakeStartReceiving сообщение начала сессии (Mtk2)
func (p Proto) MakeStartReceiving() []byte {
	var b strings.Builder

	b.WriteString(p.QCatSvc[aftn.IdxRus])
	b.WriteString(" ")
	b.WriteString(p.CcmAddress[aftn.IdxRus])
	b.WriteString(p.CrLf)
	b.WriteString(genRdNumString())
	b.WriteString(string(aftn.RUS))
	b.WriteString(" ")
	b.WriteString(p.OwnAddress[aftn.IdxRus])
	b.WriteString(p.CrLf)
	b.WriteString("СЖЦ ГОТОВ К ПРИЕМУ")

	return mtk2.Mtk2{}.FromString(b.String())
}
