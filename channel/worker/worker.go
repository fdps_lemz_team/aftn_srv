// todo добавить обнуление кол-ва отправленных и полученных тлг за сутки

package worker

import (
	"encoding/json"
	"fdps/aftn/aftn-srv/channel/state"
	"fmt"
	"strings"
	"time"

	"fdps/aftn/aftn-srv/chanfdps"
	"fdps/aftn/aftn-srv/channel/aftn"
	"fdps/aftn/aftn-srv/channel/db"
	"fdps/aftn/aftn-srv/channel/mtk2"
	"fdps/aftn/aftn-srv/channel/settings"
	"fdps/aftn/aftn-srv/channel/sockmon"
	"fdps/aftn/aftn-srv/chiefchan"
	"fdps/aftn/aftn-srv/runtimeinfo"
	"fdps/utils/logger"
)

var ChannelSetts chansetts.Settings // настройки сервиса, оплученные от конфигуратора
var channelConf chansetts.Config    // настройки AFTN канала
var ChiefPort int                   // порт подключения к контроллеру
var WebPath string                  // path web странички
var WebPort int                     // порт web странички

const (
	InpDataBytes = 4096 // max размер принятых данных для состояния канала
	OutDataBytes = 4096 // max размер отправленных данных для состояния канала
)

type Worker struct {
	Done             chan struct{}
	QueueEnabled     bool
	SSAutoConfirm    bool
	MessageDelimiter string
	IsInpCHCheck     bool
	MaxMessageLen    int
	sockMonBusy      bool
	outTlg           *aftn.OutTlg
	fdpsReady        bool

	stateTicker  *time.Ticker // тикер для отправки сообщений текущего состояния
	sockMon      *sockmon.SockMon
	fdpsWsServer *chanfdps.Server
	runtumeCntrl *runtimeinfo.RuntimeInfoController
	state        chanstate.State
}

func NewWorker(done chan struct{}) *Worker {
	wrk := Worker{
		Done:          done,
		SSAutoConfirm: true,
		QueueEnabled:  true,
		stateTicker:   time.NewTicker(1 * time.Second),
		sockMon:       sockmon.NewSockMon(),
		sockMonBusy:   true,
		fdpsReady:     false,
		fdpsWsServer:  chanfdps.NewServer(done),
		runtumeCntrl:  runtimeinfo.NewController(1),
	}
	wrk.stateTicker.Stop()
	return &wrk
}

func (w *Worker) Start() {
	go w.runtumeCntrl.Work()

	w.outTlg = aftn.NewOutTlg(2100, "")

	chiefClient := chiefchan.NewClient(w.Done)
	go chiefClient.Work()
	chiefClient.SettChan <- chiefchan.ClientSettings{ChiefAddress: "127.0.0.1", ChiefPort: ChiefPort, ChannelID: ChannelSetts.ID}

	go w.fdpsWsServer.Work()

	go w.sockMon.Work()

	if dbErr := db.InitDb("./fdps-aftn.db"); dbErr != nil {
		logger.PrintfErr("%v", dbErr)
	}

	for {
		select {
		// получены считанные данные от контроллера каналов
		case curData := <-chiefClient.ReceiveChan:
			var headerMsg chiefchan.HeaderMsg

			if err := json.Unmarshal(curData, &headerMsg); err == nil {
				switch headerMsg.Header {
				case chiefchan.SettingsHdr:
					if err := json.Unmarshal(curData, &ChannelSetts); err == nil {
						if checkErr := ChannelSetts.CheckSettings(); checkErr != nil {
							logger.PrintfErr("Получены некорректные настройки. Настройки: %s. Ошибка: %s", ChannelSetts.ToLogMessage(), checkErr.Error())
						} else {
							logger.PrintfInfo("Получены настройки. Настройки: %s.", ChannelSetts.ToLogMessage())
							channelConf = ChannelSetts.ToConfig()

							w.sockMon.SettsChan <- sockmon.Settings{
								OwnAddressUtf8: channelConf.OwnAddress[aftn.IdxRus],
								Tping:          0x00,
								Ips:            []string{channelConf.CcmIpAddress},
								Port:           uint16(channelConf.CcmIpPort),
								SessionMsgFunc: MakeStartReceiving,
							}

							w.state.ID = ChannelSetts.ID
							w.state.InpName = ChannelSetts.InpNameLat
							w.state.OutName = ChannelSetts.OutNameLat
							w.state.CommonState = chanstate.StateOk
							w.state.StateColor = logger.StateOkColor
							w.state.URL = fmt.Sprintf("http://%s:%d/%s", ChannelSetts.URLAddress, ChannelSetts.URLPort, ChannelSetts.URLPath)
							w.state.CcmConnect = "???"
							w.state.FdpsWsState = "???"
							w.state.FdpsWsClients = "???"

							w.stateTicker.Stop()
							w.stateTicker = time.NewTicker(1 * time.Second)

							w.fdpsWsServer.SettChan <- chanfdps.ServerSetts{ServerPort: ChannelSetts.FdpsWsPort}

							if !ChannelSetts.ShowDebugMgs {
								logger.SetMinSeverity(logger.SevInfo)
							}

						}
					} else {
						logger.PrintfErr("Ошибка разбора сообщения с настройками канала. Сообщение: %s. Ошибка: %s.",
							string(curData), err.Error())
					}

				case chiefchan.StopTransportRequestHdr:
					var curStopMsg chiefchan.StopTransportRequestMsg
					if err := json.Unmarshal(curData, &curStopMsg); err == nil {
						logger.PrintfInfo("Получен запрос остановки передачи телеграмм.")
					} else {
						logger.PrintfErr("Ошибка разбора запроса остановки передачи телеграмм. Сообщение: %s. Ошибка: %s.",
							string(curData), err.Error())
					}

				case chiefchan.RestoreTransportRequestHdr:
					var curRestoreMsg chiefchan.RestoreTransportRequestMsg
					if err := json.Unmarshal(curData, &curRestoreMsg); err == nil {
						logger.PrintfInfo("Получен запрос возобновления передачи телеграмм.")
					} else {
						logger.PrintfErr("Ошибка разбора запроса возобновления передачи телеграмм. Сообщение: %s. Ошибка: %s.",
							string(curData), err.Error())
					}

				case chiefchan.SendTestRequestHdr:
					var curTestMsg chiefchan.SendTestRequestMsg
					if err := json.Unmarshal(curData, &curTestMsg); err == nil {
						logger.PrintfInfo("Получен запрос отправки тестового телеграммы.")
					} else {
						logger.PrintfErr("Ошибка разбора запроса отправки тстовой телеграммы. Сообщение: %s. Ошибка: %s.",
							string(curData), err.Error())
					}

				default:
					logger.PrintfErr("От контроллера получено сообщение неизвестного формата. Сообщение: %s. Ошибка: %s.",
						string(curData), err.Error())
					continue
				}
			} else {
				logger.PrintfErr("Ошибка разбора сообщения от контроллера. Сообщение: %s. Ошибка: %s.",
					string(curData), err.Error())
			}

		// получено runtime состояние
		case w.state.RuntimeInfo = <-w.runtumeCntrl.InfoChan:

		// сработал таймер отправки состояния канала
		case <-w.stateTicker.C:
			if w.sockMon.IsOpen() {
				w.state.CcmConnect = chanstate.StateOk
			} else {
				w.state.CcmConnect = chanstate.StateError
			}

			if w.fdpsWsServer.IsListening() {
				w.state.FdpsWsState = chanstate.StateOk

			} else {
				w.state.FdpsWsState = chanstate.StateError
			}
			w.state.FdpsWsClients = w.fdpsWsServer.ConnectedClients()

			if w.sockMon.IsOpen() && w.fdpsWsServer.IsListening() {
				w.state.CommonState = chanstate.StateOk
				w.state.StateColor = logger.StateOkColor

				if len(w.state.FdpsWsClients) == 0 {
					w.state.CommonState = chanstate.StateWarn
					w.state.StateColor = logger.StateWarningColor
				}

			} else {
				w.state.CommonState = chanstate.StateError
				w.state.StateColor = logger.StateErrorColor
			}

			// отправляем heartbeat сообщение контроллеру
			if dataToSend, err := json.Marshal(chiefchan.CreateChannelHeartbeatMsg(w.state)); err == nil {
				chiefClient.SendChan <- dataToSend
			}

		// состояниеуFDPS WS сервера
		case w.fdpsReady = <-w.fdpsWsServer.ReadyChan:
			if w.fdpsReady {
				w.sendFdpsTlgBackToFdps()
				w.sendReceivedTlgToFdps()
			}

		// Tlg от FDPS сервиса
		case tlgFromFdps := <-w.fdpsWsServer.ReceiveTlgChan:
			logger.PrintfInfo("Получены данные от FDPS сервиса: %v", tlgFromFdps)
			db.FdpsTlgAppend(tlgFromFdps.Text, tlgFromFdps.Id)
			//w.enqueueForSend(&tlgFromFdps) // пишем сообщение в очередь на отправку

			w.sendToCcm() // отправить сообщение из очереди на отправку

		// Lam от FDPS сервиса
		case lamFromFdps := <-w.fdpsWsServer.ReceiveLamChan:
			logger.PrintfInfo("Получен LAM от FDPS сервиса: %v", lamFromFdps)

			// отправили тлг с полным текстом после отправки в ЦКС
			if lamFromFdps.Type == chanfdps.TlgForSend {
				db.FdpsTlgLamFromFdps(lamFromFdps.Id)
				w.sendFdpsTlgBackToFdps()
			} else if lamFromFdps.Type == chanfdps.TlgReceived { // lam на полученную от ЦКС тлг
				db.CcmTlgLamFromFdps(lamFromFdps.Id)
				w.sendReceivedTlgToFdps()
			}

		// данные от SockMon
		case sockMonData := <-w.sockMon.ReceivedChan:
			// todo распарсить тлг
			if strings.Contains(mtk2.Mtk2{}.ToString(sockMonData), "CHIP") {
				logger.PrintfInfo("Получены данные от ЦКС: %v.", mtk2.Mtk2{}.ToString(sockMonData))

				dbId := db.CcmTlgAppend(mtk2.Mtk2{}.ToString(sockMonData))

				w.state.DailyInpTlgCount++
				w.state.LastInpTlgDateTime = time.Now().Format(chanstate.TimeFormat)
				w.state.LastInpData += string(sockMonData)
				if len(w.state.LastInpData) > InpDataBytes {
					w.state.LastInpData = w.state.LastInpData[len(w.state.LastInpData)-InpDataBytes:]
				}

				if lamErr := w.sockMon.Lam(); lamErr == nil {
					db.CcmTlgLamToCcm(dbId)

					w.sendReceivedTlgToFdps() // отправляем тлг в FDPS
				} else {
					logger.PrintfErr("Ошибка отправки LAM в ЦКС: %v", lamErr)
				}
			}

		// признак занятости канала от SockMon
		case w.sockMonBusy = <-w.sockMon.BusyChan:
			logger.PrintfErr("Получен признак занятости от SockMon. Занят: %v", w.sockMonBusy)

			w.sendToCcm() // отправить сообщение из очереди на отправку

		// получен Lam от ЦКС
		case tlgDbId := <-w.sockMon.LamFromCcmChan:
			db.FdpsTlgLamFromCcm(tlgDbId)
			logger.PrintfInfo("LAM от ЦКС. Tlg ID: %d.", tlgDbId)

			w.sendFdpsTlgBackToFdps() // отправляем tlg с измененным текстом опять в FDPS
		}
	}
}

// передача тлг в канал по одной
func (w *Worker) sendToCcm() {
	if w.sockMonBusy {
		logger.PrintfWarn("SockMon is busy")
		return
	}

	// попытка отправить в ЦКС
	if tlgText, dbId, err := db.FdpsTlgGetOneForCcm(); err != nil {
		logger.PrintfErr("Ошибка получения телеграммы для отправки в ЦКС из БД: %v", err)
	} else {
		if len(tlgText) > 0 {
			if valid := w.outTlg.SetText(tlgText); valid {
				if fulltext, e := w.outTlg.MakeTlg(channelConf.OutName, "001"); e == nil {
					if _, e := w.sockMon.Write(mtk2.Mtk2{}.FromString(fulltext), dbId); e == nil {
						db.FdpsTlgSendToCcm(fulltext, dbId)

						w.state.DailyOutTlgCount++
						w.state.LastOutTlgDateTime = time.Now().Format(chanstate.TimeFormat)
						w.state.LastOutData += fulltext
						if len(w.state.LastOutData) > OutDataBytes {
							w.state.LastOutData = w.state.LastOutData[len(w.state.LastOutData)-OutDataBytes:]
						}
					}
					w.sockMonBusy = true
				}
			} else {
				logger.PrintfErr("Телеграмма не отправлена в ЦКС. Ошибки: %v", w.outTlg.Errors)
			}
		}
	}
}

//запись тлг в очередь на передачу
//func (w *Worker) enqueueForSend(tlg *chanfdps.Tlg) {
//	db.FdpsTlgAppend(tlg.Text, tlg.Id)
//	logger.PrintfErr("enqueueForSend")
//}

// передача тлг в FDPS после отправки в ЦКС
func (w *Worker) sendFdpsTlgBackToFdps() {
	if !w.fdpsReady {
		logger.PrintfWarn("fdps not ready")
		return
	}

	if tlgText, fdpsId, err := db.FdpsTlgGetOneForFdps(); err != nil {
		logger.PrintfErr("Ошибка получения телеграммы для отправки в FDPS из БД: %v", err)
	} else {
		if len(tlgText) > 0 {
			w.fdpsWsServer.SendTlgChan <- chanfdps.Tlg{
				Header: chanfdps.Header{chanfdps.TlgHdr},
				Text:   tlgText,
				Id:     fdpsId,
				Type:   chanfdps.TlgForSend,
			}
			db.FdpsTlgSendToFdps(fdpsId)
		}
	}
}

// передача полученной от ЦКС тлг в FDPS
func (w *Worker) sendReceivedTlgToFdps() {
	if !w.fdpsReady {
		logger.PrintfWarn("fdps not ready")
		return
	}

	if tlgText, fdpsId, err := db.CcmTlgGetOneForFdps(); err != nil {
		logger.PrintfErr("Ошибка получения телеграммы для отправки в FDPS из БД: %v", err)
	} else {
		if len(tlgText) > 0 {
			w.fdpsWsServer.SendTlgChan <- chanfdps.Tlg{
				Header: chanfdps.Header{chanfdps.TlgHdr},
				Text:   tlgText,
				Id:     fdpsId,
				Type:   chanfdps.TlgReceived,
			}
			db.CcmTlgSendToFdps(fdpsId)
		}
	}
}
