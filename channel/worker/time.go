package worker

import "time"

//const sqliteDateTimeFormat = "2006-01-02 15:04:05"
const RdNumFormat = "021504"
const ChannTimeFormat = "1504"

func timeString() string {
	return time.Now().UTC().Format(RdNumFormat)
}

// для имитатора
func TimeString() string {
	return timeString()
}

func timeChannString() string {
	return time.Now().UTC().Format(ChannTimeFormat)
}
