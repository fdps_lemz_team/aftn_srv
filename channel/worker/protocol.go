package worker

import (
	"strings"

	"fdps/aftn/aftn-srv/channel/aftn"
	"fdps/aftn/aftn-srv/channel/mtk2"
)

// MakeStartReceiving сообщение прекращения сессии (Mtk2)
func MakeStopReceiving() []byte {
	var (
		b strings.Builder
	)

	b.WriteString(channelConf.QCatSvc[aftn.IdxRus])
	b.WriteString(" ")
	b.WriteString(channelConf.CcmAddress[aftn.IdxRus])
	b.WriteString(channelConf.CrLf)
	b.WriteString(timeString())
	b.WriteString(string(aftn.RUS))
	b.WriteString(" ")
	b.WriteString(channelConf.OwnAddress[aftn.IdxRus])
	b.WriteString(channelConf.CrLf)
	b.WriteString("СЖЦ КОНЕЦ ПРИЕМА")

	return mtk2.Mtk2{}.FromString(b.String())
}

// MakeStartReceiving сообщение начала сессии (Mtk2)
func MakeStartReceiving() []byte {
	var (
		b strings.Builder
	)

	b.WriteString(channelConf.QCatSvc[aftn.IdxRus])
	b.WriteString(" ")
	b.WriteString(channelConf.CcmAddress[aftn.IdxRus])
	b.WriteString(channelConf.CrLf)
	b.WriteString(timeString())
	b.WriteString(string(aftn.RUS))
	b.WriteString(" ")
	b.WriteString(channelConf.OwnAddress[aftn.IdxRus])
	b.WriteString(channelConf.CrLf)
	b.WriteString("СЖЦ ГОТОВ К ПРИЕМУ")

	return mtk2.Mtk2{}.FromString(b.String())
}
