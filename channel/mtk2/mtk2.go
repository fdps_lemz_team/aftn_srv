package mtk2

import (
	"strings"
	"unicode/utf8"
)

const (
	RusRegRune = rune('↑')
	LatRegRune = rune('↓')
	NumRegRune = rune('→')
	RusRegStr  = string(RusRegRune)
	LatRegStr  = string(LatRegRune)
	NumRegStr  = string(NumRegRune)
)

var (
	ZCZC    = []byte{17, 14, 17, 14}
	NNNN    = []byte{12, 12, 12, 12}
	RUS_REG = byte(0)
	LAT_REG = byte(31)
	NUM_REG = byte(27)
	LF      = byte(2) // перевод строки
	SPACE   = byte(4) // space	пробел
	CR      = byte(8) // возврат каретки
)

var utf8Letters = [32][3]rune{
	{RusRegRune, RusRegRune, RusRegRune},
	{'Е', 'E', '3'},
	{'\n', '\n', '\n'},
	{'А', 'A', '-'},
	{' ', ' ', ' '},
	{'С', 'S', '\''},
	{'И', 'I', '8'},
	{'У', 'U', '7'},
	{'\r', '\r', '\r'},
	{'Д', 'D', ' '},
	{'Р', 'R', '4'},
	{'Й', 'J', 'Ю'},
	{'Н', 'N', ','},
	{'Ф', 'F', 'Э'},
	{'Ц', 'C', ':'},
	{'К', 'K', '('},
	{'Т', 'T', '5'},
	{'З', 'Z', '+'},
	{'Л', 'L', ')'},
	{'В', 'W', '2'},
	{'Х', 'H', 'Щ'},
	{'Ы', 'Y', '6'},
	{'П', 'P', '0'},
	{'Я', 'Q', '1'},
	{'О', 'O', '9'},
	{'Б', 'B', '?'},
	{'Г', 'G', 'Ш'},
	{NumRegRune, NumRegRune, NumRegRune},
	{'М', 'M', '.'},
	{'Ь', 'X', '/'},
	{'Ж', 'V', '='},
	{LatRegRune, LatRegRune, LatRegRune},
}

var (
	mtk2Rus map[rune]byte
	mtk2Lat map[rune]byte
	mtk2Num map[rune]byte
)

func init() {
	mtk2Rus = make(map[rune]byte, 32)
	mtk2Lat = make(map[rune]byte, 32)
	mtk2Num = make(map[rune]byte, 32)

	mtk2Rus[RusRegRune], mtk2Lat[RusRegRune], mtk2Num[RusRegRune] = 0, 0, 0
	mtk2Rus['Е'], mtk2Lat['E'], mtk2Num['3'] = 1, 1, 1
	mtk2Rus['\n'], mtk2Lat['\n'], mtk2Num['\n'] = 2, 2, 2
	mtk2Rus['А'], mtk2Lat['A'], mtk2Num['-'] = 3, 3, 3
	mtk2Rus[' '], mtk2Lat[' '], mtk2Num[' '] = 4, 4, 4
	mtk2Rus['С'], mtk2Lat['S'], mtk2Num['\\'] = 5, 5, 5
	mtk2Rus['И'], mtk2Lat['I'], mtk2Num['8'] = 6, 6, 6
	mtk2Rus['У'], mtk2Lat['U'], mtk2Num['7'] = 7, 7, 7
	mtk2Rus['\r'], mtk2Lat['\r'], mtk2Num['\r'] = 8, 8, 8
	mtk2Rus['Д'], mtk2Lat['D'], mtk2Num[0xFF] = 9, 9, 9
	mtk2Rus['Р'], mtk2Lat['R'], mtk2Num['4'] = 10, 10, 10
	mtk2Rus['Й'], mtk2Lat['J'], mtk2Num['Ю'] = 11, 11, 11
	mtk2Rus['Н'], mtk2Lat['N'], mtk2Num[','] = 12, 12, 12
	mtk2Rus['Ф'], mtk2Lat['F'], mtk2Num['Э'] = 13, 13, 13
	mtk2Rus['Ц'], mtk2Lat['C'], mtk2Num[':'] = 14, 14, 14
	mtk2Rus['К'], mtk2Lat['K'], mtk2Num['('] = 15, 15, 15
	mtk2Rus['Т'], mtk2Lat['T'], mtk2Num['5'] = 16, 16, 16
	mtk2Rus['З'], mtk2Lat['Z'], mtk2Num['+'] = 17, 17, 17
	mtk2Rus['Л'], mtk2Lat['L'], mtk2Num[')'] = 18, 18, 18
	mtk2Rus['В'], mtk2Lat['W'], mtk2Num['2'] = 19, 19, 19
	mtk2Rus['Х'], mtk2Lat['H'], mtk2Num['Щ'] = 20, 20, 20
	mtk2Rus['Ы'], mtk2Lat['Y'], mtk2Num['6'] = 21, 21, 21
	mtk2Rus['П'], mtk2Lat['P'], mtk2Num['0'] = 22, 22, 22
	mtk2Rus['Я'], mtk2Lat['Q'], mtk2Num['1'] = 23, 23, 23
	mtk2Rus['О'], mtk2Lat['O'], mtk2Num['9'] = 24, 24, 24
	mtk2Rus['Б'], mtk2Lat['B'], mtk2Num['?'] = 25, 25, 25
	mtk2Rus['Г'], mtk2Lat['G'], mtk2Num['Ш'] = 26, 26, 26
	mtk2Rus[NumRegRune], mtk2Lat[NumRegRune], mtk2Num[NumRegRune] = 27, 27, 27
	mtk2Rus['М'], mtk2Lat['M'], mtk2Num['.'] = 28, 28, 28
	mtk2Rus['Ь'], mtk2Lat['X'], mtk2Num['/'] = 29, 29, 29
	mtk2Rus['Ж'], mtk2Lat['V'], mtk2Num['='] = 30, 30, 30
	mtk2Rus[LatRegRune], mtk2Lat[LatRegRune], mtk2Num[LatRegRune] = 31, 31, 31
}

type Mtk2 struct {
	idxReg int
}

func (m *Mtk2) Utf8(mtk2Letter byte) rune {
	mtk2Letter &= 0x1F
	utf8Rune := utf8Letters[mtk2Letter][m.idxReg]
	switch utf8Rune {
	case RusRegRune:
		m.idxReg = 0
	case LatRegRune:
		m.idxReg = 1
	case NumRegRune:
		m.idxReg = 2
	}
	return utf8Rune
}

func (m *Mtk2) Mtk2(utf8Rune rune) []byte {
	switch utf8Rune {
	case RusRegRune:
		m.idxReg = 0
		return []byte{mtk2Num[utf8Rune]}
	case LatRegRune:
		m.idxReg = 1
		return []byte{mtk2Num[utf8Rune]}
	case NumRegRune:
		m.idxReg = 2
		return []byte{mtk2Num[utf8Rune]}
	}
	// буквы Ч нет в мтк2
	if utf8Rune == 'Ч' {
		utf8Rune = '4'
	}

	var (
		mtk2Byte   byte
		mtk2RegNew byte
		idxRegNew  int
		ok         bool
	)

	if mtk2Byte, ok = mtk2Rus[utf8Rune]; ok {
		idxRegNew = 0
		mtk2RegNew = RUS_REG
	} else if mtk2Byte, ok = mtk2Lat[utf8Rune]; ok {
		idxRegNew = 1
		mtk2RegNew = LAT_REG
	} else if mtk2Byte, ok = mtk2Num[utf8Rune]; ok {
		idxRegNew = 2
		mtk2RegNew = NUM_REG
	}

	if !ok {
		return []byte{}
	}

	// перевод строки	// возврат каретки	// пробел
	if mtk2Byte == LF || mtk2Byte == CR || mtk2Byte == SPACE {
		return []byte{mtk2Byte}
	}

	if m.idxReg != idxRegNew {
		m.idxReg = idxRegNew
		return []byte{mtk2RegNew, mtk2Byte}
	}

	return []byte{mtk2Byte}
}

func (m Mtk2) FromString(value string) []byte {
	var resMtk2 []byte
	for i, w := 0, 0; i < len(value); i += w {
		runeValue, width := utf8.DecodeRuneInString(value[i:])
		w = width
		resMtk2 = append(resMtk2, m.Mtk2(runeValue)...)
	}
	return resMtk2
}

func (m Mtk2) ToString(value []byte) string {
	var txt strings.Builder
	for _, b := range value {
		txt.WriteRune(m.Utf8(b))
	}
	return txt.String()
}
