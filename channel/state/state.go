package chanstate

import "fdps/aftn/aftn-srv/runtimeinfo"

const (
	StateOk      = "ok"
	StateStopped = "stopped"
	StateWarn    = "warning"
	StateError   = "error"

	TimeFormat = "2006-01-02 15:04:05.000"
)

// состояние канала FMTP
type State struct {
	ID                 int                     `json:"ChannelID"`          // идентификатор канала
	InpName            string                  `json:"InpName"`            // обозначение канала на прием лат
	OutName            string                  `json:"OutName"`            // обозначение канала на передачу лат
	URL                string                  `json:"ChannelURL"`         // URL web странички канала
	CommonState        string                  `json:"CommonState"`        // состояние канала
	StateColor         string                  `json:"StateColor"`         // подсветка состояния
	CcmConnect         string                  `json:"CcmConnect"`         // состояние подключения к ЦКС
	FdpsWsState        string                  `json:"FdpsWsState"`        // состояние WS для связи с FDPS-AFTN service
	FdpsWsClients      string                  `json:"FdpsWsClients"`      // список клиентов FDPS-AFTN service
	LastInpData        string                  `json:"LastInpData"`        // последние 4 KB принятых данных
	LastOutData        string                  `json:"LastOutData"`        // последние 4 KB отправленных данных
	DailyInpTlgCount   int                     `json:"DailyInpTlgCount"`   // кол-во принятых тлг с начала суток
	DailyOutTlgCount   int                     `json:"DailyOutTlgCount"`   // кол-во отправленных тлг с начала суток
	LastInpTlgDateTime string                  `json:"LastInpTlgDateTime"` // дата, время последней принятой тлг
	LastOutTlgDateTime string                  `json:"LastOutTlgDateTime"` // дата, время последней отправленной тлг
	RuntimeInfo        runtimeinfo.RuntimeInfo `json:"RuntimeInfo"`        // runtime состояние канала
}
