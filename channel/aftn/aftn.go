package aftn

import (
	"fdps/aftn/aftn-srv/channel/mtk2"
	"strings"
	"unicode/utf8"
)

const (
	IdxRus byte = iota
	IdxLat
	IdxNum
)

const (
	// AddressLen длина адреса абонента/ЦКС
	AddressLen = 8
	// IdChanLen длина телеграфного идентификатора
	IdChanLen = 3
	// PingLen длина интервала пинга
	//PingLen = 1
)

var (
	ZCZC              = []string{"ЗЦЗЦ", "ZCZC", "+:+:"}
	NNNN              = []string{"НННН", "NNNN", ",,,,"}
	QCat              = []string{"СС", "ДД", "ФФ", "ГГ", "КК", "ЛЛ", "SS", "DD", "FF", "GG", "KK"}
	RUS               = mtk2.RusRegRune
	LAT               = mtk2.LatRegRune
	NUM               = mtk2.NumRegRune
	REG               = []rune{RUS, LAT, NUM}
	REGSTR            = []string{mtk2.RusRegStr, mtk2.LatRegStr, mtk2.NumRegStr}
	MessageDelimiter  = string([]rune{LAT, LAT, LAT, LAT, LAT, LAT, LAT, LAT, LAT, LAT, LAT, LAT})
	TelegramDelimiter = string("\n\n\n\n\n\n\n")
	CrLfRus           = string("\r\r\n")
	CrLfIcao          = string("\r\n")
	CrLf              = CrLfIcao
	NewEndingRus      = []string{"ПРОВЕРИТЬ", "         ТЕКСТ", "              ДОБАВЛЕНО НОВОЕ ОКОНЧАНИЕ"}
	NewEndingLat      = []string{"CHECK", "     TEXT", "         NEW ENDING ADDED"}
	NewEnding         = [][]string{NewEndingRus, NewEndingLat}
	ABC               = []string{
		string(RUS) + "Е\nА СИУ\rДРЙНФЦКТЗЛВХЫПЯОБГ" + string(NUM) + "МЬЖ" + string(LAT),
		string(RUS) + "E\nA SIU\rDRJNFCKTZLWHYPQOBG" + string(NUM) + "MXV" + string(LAT),
		string(RUS) + "13\n- '87\r 4Ю,Э:(5+)2Щ6019?Ш" + string(NUM) + "./=" + string(LAT),
	}
)

func QCatDetect(qc string) (string, bool) {
	l := utf8.RuneCountInString(qc)

	if l < 1 || l > 3 {
		return "", false
	}

	defQC := "FF"

	if RegIdxFromText(qc) == IdxRus {
		defQC = "ФФ"
	}

	if l == 2 {
		for _, q := range QCat {
			if qc == q {
				return qc, true
			}
		}
	}

	for _, q := range QCat {
		if strings.ContainsAny(q, qc) {
			return q, true
		}
	}

	return defQC, true
}

func RemoveRegs(txt string) string {
	txt = strings.ReplaceAll(txt, mtk2.LatRegStr, "")
	txt = strings.ReplaceAll(txt, mtk2.RusRegStr, "")
	txt = strings.ReplaceAll(txt, mtk2.NumRegStr, "")
	return txt
}

func ChangeCrLf(value, CrLf string) string {
	var (
		b    strings.Builder
		i, w int
		r    rune
		once = true
	)
	for i, w = 0, 0; i < len(value); i += w {
		r, w = utf8.DecodeRuneInString(value[i:])
		if r == '\r' || r == '\n' {
			if once {
				b.WriteString(CrLf)
				once = false
			}
		} else {
			b.WriteRune(r)
			once = true
		}
	}
	return b.String()
}

func RegIdxFromText(val string) byte {
	if IsRusText(val) {
		return IdxRus
	} else {
		return IdxLat
	}
}

func RegForText(val string) string {
	return string(REG[RegIdxFromText(val)])
}
