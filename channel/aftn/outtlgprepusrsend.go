package aftn

import (
	"errors"
	"fdps/aftn/aftn-srv/channel/mtk2"
	"fdps/parser/lex"
	"fmt"
	"strings"
	"unicode/utf8"
)

func (t *OutTlg) prepareUserTlgForSend() (string, []error) {
	var (
		b   strings.Builder
		ers []error
	)

	txt := RemoveRegs(t.textOrig)

	if len(txt) == 0 {
		ers = append(ers, errors.New("empty tlg text"))
		return "", ers
	}

	if utf8.RuneCountInString(txt) > t.MaxLen {
		ers = append(ers, errors.New(fmt.Sprintf("Длина телеграммы больше %d символов", t.MaxLen)))
	}

	for _, z := range ZCZC {
		if strings.Contains(txt, z) {
			ers = append(ers, errors.New(fmt.Sprintf("Текст содержит недопустимую комбинацию символов [%s]", z)))
		}
	}

	for _, n := range NNNN {
		if strings.Contains(txt, n) {
			ers = append(ers, errors.New(fmt.Sprintf("Текст содержит недопустимую комбинацию символов [%s]", n)))
		}
	}

	if len(ers) > 0 {
		return "", ers
	}

	abc := strings.Join(ABC, "")
	s := lex.NewScanner("AFTN Out Tlg Scanner", txt, "", "", abc)
	s.AddToSpec(mtk2.RusRegStr)
	s.AddToSpec(mtk2.LatRegStr)
	s.AddToSpec(mtk2.NumRegStr)
	s.Debug = DebugOutTlg

	latreg := false
	rusreg := false
	numreg := false
	rAlonce := true
	nronce := true
	wrongChar := "ЧЭШЩЮ"

	rln := func(v string) {
		r, l, n := ContainRegisters(v)
		if !latreg {
			latreg = l
		}
		if !rusreg {
			rusreg = r
		}
		if !numreg {
			numreg = n
		}
		if nronce && numreg {
			ers = append(ers, errors.New(fmt.Sprintf("Адресная строка содержит символы цифрового регистра [%s]",
				s.Curr().Value)))
			nronce = false
		}
		if rAlonce && rusreg && latreg {
			ers = append(ers, errors.New(fmt.Sprintf("Адресная строка содержит одновременно символы рус. и лат. регистра [%s]",
				s.Curr().Value)))
			rAlonce = false
		}

		if strings.ContainsAny(v, wrongChar) {
			ers = append(ers, errors.New(fmt.Sprintf("Адресная строка содержит недопустимые символы (%s) [%s]",
				wrongChar, s.Curr().Value)))
		}
	}

	if !s.NextMaskIs("AA") {
		ers = append(ers, errors.New(fmt.Sprintf("Категория срочности не найдена [%s]",
			s.Curr().Value)))
	}
	rln(s.Curr().Value)

	if _, ok := QCatDetect(s.Curr().Value); !ok {
		ers = append(ers, errors.New(fmt.Sprintf("Ошибочное значение категории срочности [%s]",
			s.Curr().Value)))
	}

	if len(ers) > 0 {
		return "", ers
	}

	// категория срочности
	b.WriteString(s.Curr().Value)
	t.regIdx = RegIdxFromText(s.Curr().Value)

	// проверка адресной строки и поиск строки отправителя
	for {
		s.Next()
		if s.CurrIs("") {
			ers = append(ers, errors.New(fmt.Sprintf("Строка отправителя не найдена [%s]",
				s.Curr().Value)))
			return "", ers
		}

		if s.MaskIs("999999") && s.Delim().IsNewLine {
			b.WriteString(s.Delim().Value)
			b.WriteString(s.Curr().Value)
			s.Next()
			if s.MaskIs("AAAAAAAA") {
				rln(s.Curr().Value)
				b.WriteString(REGSTR[t.regIdx])
				b.WriteString(" ")
				b.WriteString(s.Curr().Value)
			} else if s.MaskIs("AAAAAAAAAAAAA") {
				ok := false
				for _, jjjjj := range []string{"JJJJJ", "ЮЮЮЮЮ"} {
					if i := strings.Index(s.Curr().Value, jjjjj); i > -1 {
						b.WriteString(REGSTR[t.regIdx])
						b.WriteString(" ")
						b.WriteString(s.Curr().Value[:i])
						b.WriteString(mtk2.NumRegStr)
						b.WriteString(jjjjj)
						b.WriteString(REGSTR[t.regIdx])
						ok = true
						rln(s.Curr().Value[:i])
						break
					}
				}
				if !ok {
					ers = append(ers, errors.New(fmt.Sprintf("Адрес отправителя не найден [%s]",
						s.Curr().Value)))
				}
			} else {
				ers = append(ers, errors.New(fmt.Sprintf("Ошибка в адресе отправителя [%s] длина [%d]",
					s.Curr().Value, utf8.RuneCountInString(s.Curr().Value))))
			}

			if len(ers) > 0 {
				return "", ers
			}
			break
		}

		if s.MaskIs("AA") {
			if _, ok := QCatDetect(s.Curr().Value); !ok {
				ers = append(ers, errors.New(fmt.Sprintf("Ошибочное значение категории срочности [%s]",
					s.Curr().Value)))
			}
		} else if s.MaskIs("AAAAAAAA") {
		} else {
			ers = append(ers, errors.New(fmt.Sprintf("Ошибочный элемент адресной строки [%s]",
				s.Curr().Value)))
		}
		rln(s.Curr().Value)
		if len(ers) > 0 {
			return "", ers
		}
		b.WriteString(s.Delim().Value)
		b.WriteString(s.Curr().Value)
	}

	// текст сообщения
	for {
		s.Next()
		if s.CurrIs("") {
			break
		}
		b.WriteString(s.Delim().Value)
		b.WriteString(s.Curr().Value)
	}

	return ChangeCrLf(b.String(), t.CrLf), nil
}
