package aftn

import (
	"fmt"
	"time"
)

type OutChNum struct {
	CurrNum  int
	LastDate time.Time
}

func (cn *OutChNum) Set(nn int, dd time.Time) {
	cn.CurrNum = nn
	cn.LastDate = dd
	cn.fix()
}

func (cn *OutChNum) fix() {
	if cn.CurrNum > 999 || cn.CurrNum < 0 {
		cn.CurrNum = 0
	}
}

func (cn *OutChNum) Next() *OutChNum {
	// Исправляем дату, если были скачки времени вперед...
	// Дата последней отправки не может быть больше текущего времени
	if cn.LastDate.After(time.Now().UTC()) {
		cn.LastDate = time.Now().UTC()
	}
	// Новые сутки
	if cn.LastDate.Truncate(24 * time.Hour).Before(time.Now().UTC().Truncate(24 * time.Hour)) {
		cn.LastDate = time.Now().UTC()
		cn.CurrNum = 1
	} else {
		cn.CurrNum++
	}
	cn.fix()
	return cn
}

func (cn OutChNum) Int() int {
	return cn.CurrNum
}

func (cn OutChNum) String() string {
	return fmt.Sprintf("%03d", cn.CurrNum)
}
