package aftn

import (
	"errors"
	"strings"
)

var DebugOutTlg = false

type OutTlg struct {
	//ChNum OutChNum
	textOrig string
	addrBody string
	fullTlg  string
	regIdx   byte
	CrLf     string
	MaxLen   int
	IsValid  bool
	Errors   []error
}

func NewOutTlg(maxLen int, crLf string) *OutTlg {
	if maxLen < 1800 {
		maxLen = 2100
	}
	if crLf != "\r\r\n" && crLf != "\r\n" {
		crLf = "\r\r\n"
	}

	return &OutTlg{MaxLen: maxLen, CrLf: crLf}
}

func (t *OutTlg) SetText(txt string) bool {
	t.textOrig = txt                                 //
	t.IsValid = false                                //
	t.regIdx = 1                                     // LAT регистр
	t.Errors = nil                                   //
	t.addrBody, t.Errors = t.prepareUserTlgForSend() //
	t.IsValid = (len(t.addrBody) > 0)                //
	return t.IsValid
}

func (t OutTlg) AddrAndBody() string {
	return t.addrBody
}

func (t OutTlg) MakeTlg(chname []string, chnum string) (string, error) {
	var (
		b strings.Builder
	)

	if !t.IsValid {
		return "", errors.New("tlg isn't valid")
	}

	if len(chname) < 2 {
		return "", errors.New("chname isn't valid")
	}

	if len(chnum) != 3 {
		return "", errors.New("chnum isn't valid")
	}

	b.WriteString(REGSTR[t.regIdx])
	b.WriteString(ZCZC[t.regIdx])
	b.WriteString(" ")
	b.WriteString(chname[t.regIdx])
	b.WriteString(chnum)
	b.WriteString(" ")
	b.WriteString(chanTimeString())
	b.WriteString("     ")
	b.WriteString(REGSTR[t.regIdx])
	b.WriteString(t.CrLf)
	b.WriteString(t.addrBody)
	b.WriteString(CrLf)
	b.WriteString(NNNN[t.regIdx])
	b.WriteString(MessageDelimiter)

	return b.String(), nil
}
