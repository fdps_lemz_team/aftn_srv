package aftn

import "time"

//const sqliteDateTimeFormat = "2006-01-02 15:04:05"
const RdNumFormat = "021504"
const ChannTimeFormat = "1504"

func TimeString() string {
	return time.Now().UTC().Format(RdNumFormat)
}

func chanTimeString() string {
	return time.Now().UTC().Format(ChannTimeFormat)
}
