package aftn

import (
	"log"
)

var (
	testUserTlg = []string{
		`ФФ УУУЩУУУУ
271842 УУУУУУУУ
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSAGE STR2

`,		`FF UUUUUUUU ASDFGHJK
FF ASDFGHJK 
271842 QWERTYUI
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSAGE STR2

`,
		`SS UUUUUUUU ASDFGHJK
271842 QWERTYUIJJJJJ
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSAGE STR2

`,
		`SS UUUUUUUU ASDFGHJK
271842 QWERTYUIJJJJJ
TEST MESSNNNNAGE STR1
TEST MEZCZCSSAGE STR3
TE,,,,ST MESS+:+:AGE STR2

`,
		`ФФ ФЫВАПРОЛ АВПЮВПВП
271842 КККККККК
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSФGE STR2

`,
		`ФФ ФЫВАПРОЛ УУУУУУУУ
271842 ККККККК
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSФGE STR2

`,
		`ФФ ФЫВАПРОЛ УУУУУУУУ
271842 ККККККЮК
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSФGE STR2

`, `FF ФЫВАПРОЛ УУУУУУУУ
271842 КККККККК
TEST MESSAGE STR1
TEST MESSAGE STR3
TEST MESSФGE STR2

`,
	}
)

func TestPrepareUserTlgForSend() {
	ot := NewOutTlg(2000, "\r\r\n")

	for n, txt := range testUserTlg {
		if n != 0 {
			break
		}
		ot.SetText(txt)
		log.Println("n =>", n)
		log.Println("message =>", ot.AddrAndBody())
		log.Println("errors =>", ot.Errors)
	}
	log.Println(MessageDelimiter)
}
