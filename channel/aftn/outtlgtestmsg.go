package aftn

import "strings"

func (t OutTlg) Test(ownAddrLat string) string {
	var b strings.Builder

	b.WriteString("ZCZC QJH")
	b.WriteString(t.CrLf)
	b.WriteString(ownAddrLat) // Свой главный адресный указатель
	b.WriteString(t.CrLf)
	b.WriteString("RYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRY")
	b.WriteString(t.CrLf)
	b.WriteString("RYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRY")
	b.WriteString(t.CrLf)
	b.WriteString("RYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRYRY")
	b.WriteString(t.CrLf)
	b.WriteString("NNNN")

	return b.String()
}
