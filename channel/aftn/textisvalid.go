package aftn

import "strings"

func TextIsValid(val string) bool {
	val = strings.ToUpper(val)
	for i := 0; i < len(ZCZC); i++ {
		if strings.Contains(val, ZCZC[i]) || strings.Contains(val, NNNN[i]) {
			return false
		}
	}
	return true
}
