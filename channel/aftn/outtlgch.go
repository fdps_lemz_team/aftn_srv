package aftn

import (
	"fdps/aftn/aftn-srv/channel/mtk2"
	"strings"
	"unicode/utf8"
)

func (t OutTlg) Ch(regIdx byte) string {
	var b strings.Builder

	if regIdx == IdxRus {
		b.WriteString("ЦХ")
		b.WriteString(mtk2.RusRegStr)
	} else {
		b.WriteString("CH")
		b.WriteString(mtk2.LatRegStr)
	}

	b.WriteString(t.CrLf)
	b.WriteString("\n\n\n\n\n\n\n")

	return b.String()
}

func (t OutTlg) ChLr(regIdx byte, inpChName string, inpLastChNum string) string {
	if utf8.RuneCountInString(inpChName) != 3 || utf8.RuneCountInString(inpLastChNum) != 3 {
		return ""
	}

	var b strings.Builder

	txt := []string{"ЦХ ЛР ", "CH LR "}
	regIdx = RegIdxFromText(inpChName)
	b.WriteString(txt[regIdx])
	b.WriteString(inpChName) // Обозначение канала на прием
	b.WriteString(mtk2.NumRegStr)
	b.WriteString(string(REG[regIdx]))
	b.WriteString(t.CrLf)
	b.WriteString("\n\n\n\n\n\n\n")

	return b.String()
}
