package aftn

import "strings"

const (
	rusLetters = "ЕАСИУДРЙНФЦКТЗЛВХЫПЯОБГМЬЖ"
	latLetters = "EASIUDRJNFCKTZLWHYPQOBGMXV"
	numLetters = "13-'874Ю,Э:(5+)2Щ6019?Ш./="
)

func IsRusText(val string) bool {
	return strings.ContainsAny(strings.ToUpper(val), rusLetters)
}

func ContainRegisters(val string) (r bool, l bool, n bool) {
	r = strings.ContainsAny(strings.ToUpper(val), rusLetters)
	l = strings.ContainsAny(strings.ToUpper(val), latLetters)
	n = strings.ContainsAny(strings.ToUpper(val), numLetters)

	return r, l, n
}
