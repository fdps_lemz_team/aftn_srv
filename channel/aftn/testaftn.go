package aftn

import (
	"log"
)

func TestQCatDetect() {
	type QCatTestData struct {
		s string
		r string
	}
	testQCat := []QCatTestData{
		{s: "SXXS", r: ""},
		{s: "SS", r: "SS"},
		{s: "DD", r: "DD"},
		{s: "FF", r: "FF"},
		{s: "GG", r: "GG"},
		{s: "KK", r: "KK"},
		{s: "XX", r: "FF"},
		{s: "FDS", r: "SS"},
	}
	for _, d := range testQCat {
		q, _ := QCatDetect(d.s)
		if q != d.r {
			log.Println("Error", q, d.r)
		}
	}
}
