package aftn

import (
	"strings"
	"unicode/utf8"
)

var (
	rusToLat = map[rune]string{
		'А': "A",
		'Б': "B",
		'Ц': "C",
		'Д': "D",
		'Е': "E",
		'Ф': "F",
		'Г': "G",
		'Х': "H",
		'И': "I",
		'Й': "J",
		'К': "K",
		'Л': "L",
		'М': "M",
		'Н': "N",
		'О': "O",
		'П': "P",
		'Щ': "Q",
		'Р': "R",
		'С': "S",
		'Т': "T",
		'У': "U",
		'Ж': "V",
		'В': "W",
		'Ь': "X",
		'Ы': "Y",
		'З': "Z",
		'Я': "Q",
	}

	latToRus = map[rune]string{
		'A': "А",
		'B': "Б",
		'C': "Ц",
		'D': "Д",
		'E': "Е",
		'F': "Ф",
		'G': "Г",
		'H': "Х",
		'I': "И",
		'J': "Й",
		'K': "К",
		'L': "Л",
		'M': "М",
		'N': "Н",
		'O': "О",
		'P': "П",
		'Q': "Щ",
		'R': "Р",
		'S': "С",
		'T': "Т",
		'U': "У",
		'V': "Ж",
		'W': "В",
		'X': "Ь",
		'Y': "Ы",
		'Z': "З",
	}
)

func ToLat(value string) string {
	return replaceLettes(strings.ToUpper(value), rusToLat)
}

func ToRus(value string) string {
	return replaceLettes(strings.ToUpper(value), latToRus)
}

func replaceLettes(value string, abc map[rune]string) string {
	var (
		b strings.Builder
	)
	for i, w := 0, 0; i < len(value); i += w {
		runeValue, width := utf8.DecodeRuneInString(value[i:])
		w = width
		if l, ok := abc[runeValue]; ok {
			b.WriteString(l)
		} else {
			b.WriteRune(runeValue)
		}
	}
	return b.String()
}
