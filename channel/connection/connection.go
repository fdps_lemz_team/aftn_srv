package connection

type Connection interface {
	Busy() bool
	Write(p []byte) (n int, err error)
	Read(p []byte) (n int, err error)
	Open() error
	Close() error
}
