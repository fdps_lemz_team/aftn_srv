package sockmon

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"reflect"
	"strconv"
	"sync"
	"time"

	"fdps/aftn/aftn-srv/channel/aftn"
	"fdps/aftn/aftn-srv/channel/mtk2"
	"fdps/utils"
	"fdps/utils/logger"
)

const (
	ccmStateKey       = "ЦКС. Состояние подключение:"
	ccmLastConnKey    = "ЦКС. Последнее подключение:"
	ccmLastDisconnKey = "ЦКС. Последнее отключение:"
	ccmLastErrKey     = "ЦКС. Последняя ошибка:"

	ccmStateOkValue    = "Подключено."
	ccmStateErrorValue = "Не подключено."

	timeFormat = "2006-01-02 15:04:05"
)

var (
	// ErrBusy канал занят
	ErrBusy = errors.New("busy")
	// ErrClosed нет подключения по TCP
	ErrClosed = errors.New("closed")
	// ErrNoData нет данных для чтения
	ErrNoData = errors.New("no data")

	escByte byte = 0xfd // байт управляющей последовательности
	lamByte byte = 0x01 // байт подтверждения получения сообщения

	acceptByte byte = 0x00 // байт подтверждения успешного подключения абонента
	rejectByte byte = 0x01 // байт разрыва соединения с абонентом

	blockByte   byte = 0xff // байт, означающий блокировку передачи сообщений
	unblockByte byte = 0xfe // байт, означающий восстановление передачи сообщений

	pingMsg []byte = []byte{0x00}
)

// SockMon реализация подключения к КЦС на основе TCP сокета.
// Логика работы:
// Инициализируем настройками (список удаленных IP адресов ЦКС, удаленный порт, адрес абонента, сообщение сессии, интервал пингов)
// Вызываем метод Open. Если подключение хотя бы к одному из серверов ЦКС успешно, метод Open возвращает nil в качестве ошибки.
// Проверяем занятость канала и наличие подключения к ЦКС (Busy и IsOpen соответственно)
//
// У подключенного и не занятого подключения можно запросить данные (Read)
// Метод Read вернет кол-во байт и nil в качестве ошибки, записанных в слайс, переданный как входная переменная.
// В случае либо занятости, либо отсутствия подключения будет метод Read возвращает 0 и ошибку
//
// Метод Write при отстуствии подключения, либо занятости подключения аналогично методу Read возвращает 0 и ошибку
// В случае работоспособного подключения метод Write ждет завершения отправки данных по TCP и возвращает кол-во переданных байт и ошибку (nil в случае успешной отправки)
// После вызова метода Write подключение переходит в состояние 'занят' до тех пор, пока либо не получит подтверждение от сервера,
//
// При вызове метода Lam по сети отправляется сообщение подтверждения (первый байт 0xfd, второй 0x01 )
//
// При получении данных по TCP, они складываютсяв буфер (кроме управляющих сообщений:
//  0xff - блокировать передачу, 0xfe - восстановить передачу )
// При получении управляющих сообщения канал переходит в состояние 'занят'/'не занят'
type SockMon struct {
	mu sync.Mutex

	setts          Settings      // текущие настройки
	SettsChan      chan Settings // канала для приема настроек
	BusyChan       chan bool     // канал для передачи готовности канала к передаче
	LamFromCcmChan chan int64    // канал для сигнализации полчения Lam от ЦКС
	ReceivedChan   chan []byte   // канал принятых данных от ЦКС
	pingInterval   time.Duration // интервал отправки пинга с секундах
	// 3 байта телеграфный идентификатор канала абонента на передачу
	// (заглавными русскими буквами в кодировке CP866)
	IdChanForSendRus []byte
	CcmAddress       []byte // 8 байт – АФТН адрес ЦКС (заглавными русскими буквами в кодировке CP866).

	socket          net.Conn // TCP сокет
	isBusy          bool     // Нельзя посылать в канал (true) Причины 1 канал закрыт, 2 цкс прислал FF, 3 цкс не прислал FD01
	isOpen          bool     // признак работоспособности TCP подключения
	getAcceptReject bool     // получен байт с признаком подтверждения / отказа подключения абонента
	getIdAddr       bool

	cancelWorkChan    chan struct{}
	cancelHandlerChan chan struct{}

	sendDataChan chan []byte // канал для передачи данных для отправки серверу ЦКС

	readDoneChan  chan []byte // канал для передачи кол-ва считанных байт в handleConnection
	readErrChan   chan error  // канал для передачи ошибки чтения в handleConnection
	writeDoneChan chan int    // канал для передачи кол-ва считанных байт в Write
	writeErrChan  chan error  // канал для передачи ошибки чтения в Write

	readBuffer []byte // буфер полученных данных

	// если байт управляющей последовательности был последним в буфере полученных байт,
	// то вырезаем его и при следующем получении вырезаем первый байт, если он равен байту Lam
	needCheckLam bool

	reconnTimer   *time.Timer   // таймер подключения к ЦКС
	reconnInteval time.Duration // таймер подключения к ЦКС

	lastConnTime    string // время последнего успешного подключения к ЦКС
	lastDisconnTime string // время отключения от ЦКС

	lastTlgIdForSend int64 // id последней телеграммы на отправку. При получении Lam от ЦКС id отправляется в worker
}

// NewSockMon конструктор
// OwnAddress - адрес абонента, Tping - интревал пинга, Port - сетевой порт сервера ЦКС, Ips - список адресов серверов ЦКС
//func NewSockMon(OwnAddress string, Tping byte, Port uint16, Ips []string) *SockMon {
func NewSockMon() *SockMon {
	s := &SockMon{
		SettsChan:      make(chan Settings),
		BusyChan:       make(chan bool, 2),
		LamFromCcmChan: make(chan int64, 2),
		ReceivedChan:   make(chan []byte, 10),

		IdChanForSendRus: make([]byte, 0, 3),
		CcmAddress:       make([]byte, 0, 8),

		sendDataChan: make(chan []byte, 10),

		readDoneChan:  make(chan []byte, 10),
		readErrChan:   make(chan error, 1),
		writeDoneChan: make(chan int, 10),
		writeErrChan:  make(chan error, 10),

		isOpen:          false,
		isBusy:          false,
		needCheckLam:    false,
		getAcceptReject: false,
		getIdAddr:       false,

		readBuffer:    make([]byte, 0, 10<<20),
		reconnInteval: time.Duration(time.Second * 10),

		lastConnTime:    "-",
		lastDisconnTime: "-",
	}

	s.reconnTimer = time.NewTimer(s.reconnInteval)
	s.reconnTimer.Stop()

	return s
}

// Work основной цикл работы
func (s *SockMon) Work() error {
	s.setBusy(true)

	logger.SetDebugParam(ccmStateKey, ccmStateErrorValue+"   "+time.Now().Format(timeFormat), logger.StateErrorColor)
	logger.SetDebugParam(ccmLastConnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(ccmLastDisconnKey, "-", logger.StateDefaultColor)
	logger.SetDebugParam(ccmLastErrKey, "-", logger.StateDefaultColor)

	for {
		select {
		// получены новые настройки
		case newSetts := <-s.SettsChan:
			if errCheck := newSetts.Check(); errCheck != nil {
				logger.PrintfErr(errCheck.Error())
			} else {
				if newSetts.Tping != s.setts.Tping {
					s.pingInterval = time.Second * time.Duration(int(newSetts.Tping))
				}

				if newSetts.Port != s.setts.Port || !reflect.DeepEqual(newSetts.Ips, s.setts.Ips) {
					if s.isOpen {
						s.close()
					}
				}
				s.setts = newSetts

				if !s.isOpen {
					s.connectToCmm()
				}
			}

		// необходимо подключиться к ЦС
		case <-s.reconnTimer.C:
			s.reconnTimer.Stop()
			s.connectToCmm()

		}
	}
}

// Busy признак занятости канала
//func (s *SockMon) Busy() bool {
//	s.mu.Lock()
//	defer s.mu.Unlock()
//
//	return s.isBusy
//}

// Close закрыть подключение
func (s *SockMon) close() error {
	//s.mu.Lock()
	//defer s.mu.Unlock()

	s.setBusy(true)
	s.isOpen = false
	s.getAcceptReject = false
	s.getIdAddr = false

	//s.lastDisconnTime = time.Now().Format(timeFormat)
	logger.SetDebugParam(ccmLastDisconnKey, time.Now().Format(timeFormat), logger.StateDefaultColor)

	if isClosed := utils.ChanSafeClose(s.cancelWorkChan); isClosed {
		if err := s.socket.Close(); err != nil {
			logger.PrintfErr("Ошибка при закрытии клиентского TCP подключения к ЦКС. Ошибка: %s.", err.Error())
			return err
		} else {
			logger.PrintfWarn("Закрыто клиентское TCP подключение к ЦКС.")
		}
	}

	s.reconnTimer.Reset(s.reconnInteval)

	return nil
}

// IsOpen признак наличия TCP подключения к серверу ЦКС
func (s *SockMon) IsOpen() bool {
	s.mu.Lock()
	defer s.mu.Unlock()

	return s.isOpen
}

// Lam оправить серверу ЦКС подтверждение получения сообщения. Возвращает ошибку:
// nil в случае успешной передачи
// error("closed") - нет подключения к серверу
// error("busy") - подключение занято ???
// net error - ошибка, возникшая при отправке по сети
func (s *SockMon) Lam() error {
	//s.mu.Lock()
	//defer s.mu.Unlock()

	if !s.isOpen {
		return ErrClosed
		//} else if s.isBusy {
		//	return ErrBusy
	} else {
		if _, lamErr := s.sendDataToCcm([]byte{0x01}); lamErr != nil {
			return lamErr
		}
	}
	return nil
}

// connectToCmm попытка подключения к ЦКС. msg - сообщение абонента после успешного подключения к ЦКС
// попытка производится по всем возможным адресам ЦКС. В случае, если ни один не ответил, возвращаем ошибку, иначе - nil
//func (s *SockMon) Open(msg []byte) error {
func (s *SockMon) connectToCmm() {
	for _, curIp := range s.setts.Ips {
		logger.SetDebugParam(ccmLastConnKey, time.Now().Format(timeFormat), logger.StateDefaultColor)

		var err error
		curAddr := curIp + ":" + strconv.Itoa(int(s.setts.Port))
		if s.socket, err = net.Dial("tcp", curAddr); err != nil {
			errText := fmt.Sprintf("При установке TCP подключения к ЦКС возникла ошибка. Адрес: %s. Ошибка: %s", curAddr, err.Error())
			logger.PrintfErr(errText)
			logger.SetDebugParam(ccmLastErrKey, errText, logger.StateErrorColor)
			continue
		}

		if err = s.socket.(*net.TCPConn).SetKeepAlive(false); err != nil {
			errText := fmt.Sprintf("При установке флага keep_alive TCP соединения возникла ошибка. Адрес: %s. Ошибка: %s", curAddr, err.Error())
			logger.PrintfErr(errText)
			logger.SetDebugParam(ccmLastErrKey, errText, logger.StateErrorColor)
			continue
		}
		//s.setBusy(false)
		logger.PrintfInfo("Установлено TCP подключение к ЦКС. Адрес: %s.", curAddr)
		logger.SetDebugParam(ccmStateKey, ccmStateOkValue+curAddr, logger.StateOkColor)

		s.cancelWorkChan = make(chan struct{}, 1)

		go s.handleConnection()
		s.lastConnTime = time.Now().Format(timeFormat)

		return
	}
	// запускаем таймер подключения
	s.reconnTimer.Reset(s.reconnInteval)
	logger.PrintfErr("Не подключен ни к одному ЦКС.")
}

// Read чтение из буфера полученных данных. Данные считаются соответственно capacity переданного слайса
// возвращает кол-во записанных в слайс p байт
//func (s *SockMon) Read(p []byte) int {
//	s.mu.Lock()
//	defer s.mu.Unlock()
//
//	logger.PrintfDebug("Запрошено %d байт. Размер буфера %d байт.", len(p), len(s.readBuffer))
//
//	rc := copy(p, s.readBuffer)
//	s.readBuffer = s.readBuffer[rc:]
//	return rc
//}

// Write отправка серверу ЦКС сообщения.
// возвращает кол-во переданных байт и ошибку:
// nil - в случае успешной передачи
// error("closed") - нет подключения к серверу
// error("busy") - подключение занято
// net error - ошибка, возникшая при отправке по сети
func (s *SockMon) Write(p []byte, tlgId int64) (n int, err error) {
	s.lastTlgIdForSend = tlgId
	if !s.isOpen {
		logger.PrintfErr("Ошибка отправки данных. Канал закрыт.")
		return 0, ErrClosed
	} else if s.isBusy {
		logger.PrintfErr("Ошибка отправки данных. Канал занят.")
		return 0, ErrBusy
	}
	s.setBusy(true)
	return s.sendDataToCcm(p)
}

func (s *SockMon) sendDataToCcm(p []byte) (n int, err error) {

	s.sendDataChan <- p

	//s.setBusy(true)

	for {
		select {
		// данные отправлены
		case writeCount := <-s.writeDoneChan:
			//s.setBusy(false)
			return writeCount, nil
		// ошибка отправки
		case writeErr := <-s.writeErrChan:
			errText := fmt.Sprintf("Ошибка отправки данных ЦКС. Ошибка: %s.", writeErr.Error())
			logger.PrintfErr(errText)
			logger.SetDebugParam(ccmLastErrKey, errText, logger.StateErrorColor)
			_ = s.close()
			return 0, writeErr
		}
	}
}

// обрабатка TCP подключения
func (s *SockMon) handleConnection() {
	go s.receiveLoop()
	go s.sendLoop()

	// отрправляем адрес абонента
	s.sendStartupMsg()

	// таймер отправки пинга
	pingTimer := time.NewTimer(s.pingInterval)
	pingTimer.Stop()

	// тикер для проверки работоспособности подключения
	liveCheckTicker := time.NewTicker(s.pingInterval * 3)

	// время получения каких-либо данных от ЦКС (для проверки работоспособности канала)
	lastDataCome := time.Now()

	for {
		select {
		// завершение handleConnection
		case <-s.cancelWorkChan:
			logger.PrintfDebug("Close handleConnection routine")
			return

		case readBytes := <-s.readDoneChan:
			lastDataCome = time.Now()

			// пока не получили идентификатор и адрес ЦКС, сразу кладем данные в буфер и
			// анализируем буфер s.readBuffer
			// как считаем адрес и идентификатор, данные в буфер будем класть только после удаления
			// байт служебной последовательности (Lam) и системных команд (Blobk, Unblock)
			if !s.getIdAddr {

				s.readBuffer = append(s.readBuffer, readBytes...)

				// не было получено подтверждение / отказа подключения
				if !s.getAcceptReject {
					if len(s.readBuffer) > 0 {
						if s.readBuffer[0] == acceptByte {
							logger.PrintfInfo("Получено подтверждение подключения абонента от сервера ЦКС.")
							s.getAcceptReject = true
							s.getIdAddr = false

							// удаляем байт accept
							s.readBuffer = s.readBuffer[1:]
						} else if s.readBuffer[0] == rejectByte {
							logger.PrintfWarn("Получен отказ подключения абонента от сервера ЦКС.")
							// удаляем байт reject
							s.readBuffer = s.readBuffer[1:]
							continue
						}
					}
				}

				// получено подтверждение считываем телеграфный идентификатор
				if s.getAcceptReject && !s.getIdAddr && len(s.readBuffer) >= aftn.IdChanLen+aftn.AddressLen {
					s.IdChanForSendRus = s.readBuffer[:aftn.IdChanLen]
					logger.PrintfInfo("Получен телеграфный идентификатор от сервера ЦКС. Идентификатор: %s.", utils.Cp866ToUtf8(s.IdChanForSendRus))
					// удаляем телеграфный идентификатор
					s.readBuffer = s.readBuffer[aftn.IdChanLen:]

					s.CcmAddress = s.readBuffer[:aftn.AddressLen]
					logger.PrintfInfo("Получен АФТН адрес ЦКС от сервера ЦКС. Адрес: %s.", utils.Cp866ToUtf8(s.CcmAddress))
					// удаляем АФТН адрес ЦКС
					s.readBuffer = s.readBuffer[aftn.AddressLen:]

					s.getIdAddr = true
					logger.PrintfInfo("АФТН канала с сервером ЦКС установлен.")
					// теперь метод Read будет отдавать данные
					s.isOpen = true

					// отправка сообщения сессии
					s.sendSessionMsg()

					// запускаем таймер пингов
					pingTimer.Reset(s.pingInterval)

					s.setBusy(false)
				}
			} else {
				// в предыдущем сообщении удалили байт управляющей последовательности,
				// проверяем первый и удаляем его, если он Lam
				if s.needCheckLam {
					if len(readBytes) > 0 {
						if readBytes[0] == lamByte {
							logger.PrintfInfo("Получено подтверждение получения сообщения от ЦКС.")

							s.setBusy(false)
							s.LamFromCcmChan <- s.lastTlgIdForSend

							// удаляем байт Lam
							readBytes = readBytes[1:]
						} else {
							logger.PrintfErr("Получен неизвестный код служебной последовательности. %d", readBytes[0])
						}
						s.needCheckLam = false
					}
				}

				if escInd := bytes.IndexByte(readBytes, escByte); escInd != -1 {
					//logger.PrintfInfo("Получен байт управляющей последовательности.")

					if escInd < len(readBytes)-1 {
						if readBytes[escInd+1] == lamByte {
							//logger.PrintfInfo("Получено подтверждение получения сообщения.")
							//logger.PrintfDebug("LamFromCcmChan %d. BusyChan %d.", len(s.LamFromCcmChan), len(s.BusyChan))
							s.setBusy(false)
							s.LamFromCcmChan <- s.lastTlgIdForSend

							// удаляем байт управляющей последовательности и байт Lam
							readBytes = append(readBytes[:escInd], readBytes[escInd+2:]...)
						} else {
							// байт управляющей последовательности последний в буфере
							// удаляем его и при следующем полчении данных удаляем первый байт, если он обозначает Lam
							readBytes = readBytes[:escInd]
							s.needCheckLam = true
						}
					}
				}

				if blockInd := bytes.IndexByte(readBytes, blockByte); blockInd != -1 {
					logger.PrintfInfo("Получено сообщение о блокировке передачи сообщений.")
					s.setBusy(true)
					// работает, даже если blockInd == len(readBytes) - 1
					readBytes = append(readBytes[:blockInd], readBytes[blockInd+1:]...)
				}

				if unblockInd := bytes.IndexByte(readBytes, unblockByte); unblockInd != -1 {
					logger.PrintfInfo("Получено сообщение о возобновлении передачи сообщений.")
					s.setBusy(false)
					// работает, даже если unblockInd == len(readBytes) - 1
					readBytes = append(readBytes[:unblockInd], readBytes[unblockInd+1:]...)
				}

				s.readBuffer = append(s.readBuffer, readBytes...)
			}
			//logger.PrintfDebug("Размер буфера принятых данных: %d", len(s.readBuffer))
			s.ReceivedChan <- s.readBuffer
			s.readBuffer = s.readBuffer[:0]

		case readErr := <-s.readErrChan:
			errText := fmt.Sprintf("Ошибка чтения данных от ЦКС. Ошибка: %s.", readErr.Error())
			logger.PrintfErr(errText)
			logger.SetDebugParam(ccmLastErrKey, errText, logger.StateErrorColor)
			_ = s.close()
			logger.PrintfDebug("Close handleConnection routine/ Read Error")
			return

		// сработал таймер отправки PING
		case <-pingTimer.C:
			//logger.PrintfDebug("Ping Timer handleConnection routine")
			s.sendPingMsg()
			pingTimer.Reset(s.pingInterval)

		// проверка работоспособности подключения к ЦКС
		case <-liveCheckTicker.C:
			//logger.PrintfDebug("Live Check Timer handleConnection routine")
			if time.Now().Add(s.pingInterval * -3).After(lastDataCome) {
				errText := "За интервал времени, разный 3 * PING не получено данных. Соединение с ЦКС будет закрыто."
				logger.PrintfErr(errText)
				logger.SetDebugParam(ccmLastErrKey, errText, logger.StateErrorColor)
				_ = s.close()
				//logger.PrintfDebug("Close handleConnection routine/ Live Check")
				return
			}
		}
	}
}

// обработчик получения данных
func (s *SockMon) receiveLoop() {
	for {
		select {
		// отмена приема данных
		case <-s.cancelWorkChan:
			logger.PrintfDebug("Close receiveLoop routine")
			return
		// прием данных
		default:
			buffer := make([]byte, 8096)
			if count, err := s.socket.Read(buffer); err != nil {
				s.isOpen = false
				s.readErrChan <- err
				logger.PrintfDebug("Close receiveLoop routine")
				return
			} else {
				//logger.PrintfDebug("Приняты данные. Размер: %d байт. Данные: %v", count, buffer[:count])
				s.readDoneChan <- buffer[:count]
			}
		}
	}
}

// обработчик отправки данных
func (s *SockMon) sendLoop() {
	for {
		select {
		// отмена отправки данных
		case <-s.cancelWorkChan:
			logger.PrintfDebug("Close sendLoop routine")
			return

		// получены данные для отправки
		case curData := <-s.sendDataChan:
			if count, err := s.socket.Write(curData); err != nil {
				s.isOpen = false
				s.writeErrChan <- err
				logger.PrintfDebug("Close sendLoop routine")
				return
			} else {
				logger.PrintfDebug("Отправлены данные ЦКС.  Размер: %d байт. Данные: %v.",
					count, mtk2.Mtk2{}.ToString(curData))
				s.writeDoneChan <- count
			}
		}
	}
}

// отправка сообщения начала cессии
func (s *SockMon) sendSessionMsg() {
	s.sendDataChan <- s.setts.SessionMsgFunc()
	for {
		select {
		// данные отправлены
		case <-s.writeDoneChan:
			logger.PrintfInfo("Отправлено сообщение сессии: %s.", mtk2.Mtk2{}.ToString(s.setts.SessionMsgFunc()))
			return

		// ошибка отправки
		case writeErr := <-s.writeErrChan:
			errText := fmt.Sprintf("Ошибка отправки сообщения сессии. Ошибка: %s", writeErr.Error())
			logger.PrintfErr(errText)
			logger.SetDebugParam(ccmLastErrKey, errText, logger.StateErrorColor)
			_ = s.close()
			return
		}
	}
}

// отправка сообщения после успешного подключения к серверу
func (s *SockMon) sendStartupMsg() {
	startupMsg := s.setts.ownAddressCp866
	startupMsg = append(startupMsg, s.setts.Tping)

	s.sendDataChan <- startupMsg
	for {
		select {
		// данные отправлены
		case <-s.writeDoneChan:
			logger.PrintfInfo("Отправлено стартовое сообщение: %s%d.", s.setts.OwnAddressUtf8, int(s.setts.Tping))
			return

		// ошибка отправки
		case writeErr := <-s.writeErrChan:
			errText := fmt.Sprintf("Ошибка отправки стартового сообщения. Ошибка: %s", writeErr.Error())
			logger.PrintfErr(errText)
			logger.SetDebugParam(ccmLastErrKey, errText, logger.StateErrorColor)
			_ = s.close()
			return
		}
	}
}

// отправка сообщения PING
func (s *SockMon) sendPingMsg() {
	s.sendDataChan <- pingMsg

	for {
		select {
		// данные отправлены
		case <-s.writeDoneChan:
			logger.PrintfInfo("Отправлено сообщение PING.")
			return

		// ошибка отправки
		case writeErr := <-s.writeErrChan:
			errText := fmt.Sprintf("Ошибка отправки сообщения PING. Ошибка: %s.", writeErr.Error())
			logger.PrintfErr(errText)
			logger.SetDebugParam(ccmLastErrKey, errText, logger.StateErrorColor)
			_ = s.close()
			return
		}
	}
}

func (s *SockMon) setBusy(busy bool) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.isBusy = busy
	s.BusyChan <- busy
}
