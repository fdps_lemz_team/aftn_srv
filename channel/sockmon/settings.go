package sockmon

import (
	"fmt"
	"strings"
	"unicode/utf8"

	"fdps/aftn/aftn-srv/channel/aftn"
	"fdps/utils"
)

// Settings настройки подключения к ЦКС
type Settings struct {
	OwnAddressUtf8  string        // собственный 8-символьный АФТН-адрес абонента заглавными русскими буквами (UTF)
	ownAddressCp866 []byte        // собственный 8-символьный АФТН-адрес абонента заглавными русскими буквами (CP866)
	Tping           byte          // двоичное значение частоты передачи в секундах, если 0 то 10 секунд
	Ips             []string      // 2 ip цкс основной и резервный
	Port            uint16        // порт цкс
	SessionMsgFunc  func() []byte // фунция, генерирующая сообщение сессии
	//SessionMsgUtf8  string   // сообщение абонента после успешного подключения к ЦКС (UTF)
	//sessionMsgMtk2  []byte   // сообщение абонента после успешного подключения к ЦКС (Mtk2)
}

// Check проверка корректности настроек
func (s *Settings) Check() error {
	if utf8.RuneCountInString(s.OwnAddressUtf8) != aftn.AddressLen {
		return fmt.Errorf("Не верная длина адреса абонента ЦКС. Адрес: [%s]. Длина адреса: %d. Необходимая длина: %d ",
			s.OwnAddressUtf8,
			utf8.RuneCountInString(s.OwnAddressUtf8),
			aftn.AddressLen)
	}
	s.ownAddressCp866 = utils.Utf8ToCp866([]byte(aftn.ToRus(strings.ToUpper(s.OwnAddressUtf8))))

	//if len(s.SessionMsgUtf8) == 0 {
	//	return fmt.Errorf("Не задано сообщение сессии.")
	//}
	//s.sessionMsgMtk2 = mtk2.Mtk2{}.FromString(s.SessionMsgUtf8)

	if s.Tping == 0x00 {
		s.Tping = 0x0A
	}
	return nil
}
